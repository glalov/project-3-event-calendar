import { Button, Grid, GridItem, Select } from "@chakra-ui/react";
import AnimationComponent from "../Animation/Animation";
import ArrowLeft from "../../assets/ArrowLeft.json";
import ArrowRight from "../../assets/ArrowRight.json";
import { useState, useMemo } from "react";
import { format, getYear } from "date-fns";

export type CalendarHeaderProps = {
  handlePrev: () => void;
  handleNext: () => void;
  handleChangeMonth: (month: number) => void;
  handleChangeYear: (month: number) => void;
  currentDate: Date | null;
  setCurrentDate: React.Dispatch<React.SetStateAction<Date>> | null;
  selectMode: React.ReactNode;
  locale: Locale;
};

/**
 * Renders the header of the calendar component, including navigation buttons, month and year selectors,
 * and a "Today" button. Also receives props to handle changes to the selected month and year, and to
 * display a select mode component.
 *
 * @param handlePrev - Function to handle navigation to the previous month.
 * @param handleNext - Function to handle navigation to the next month.
 * @param handleChangeMonth - Function to handle changes to the selected month.
 * @param handleChangeYear - Function to handle changes to the selected year.
 * @param currentDate - The currently selected date.
 * @param setCurrentDate - Function to set the currently selected date.
 * @param selectMode - The select mode component to display.
 * @param locale - The locale to use for formatting month names.
 */
const CalendarHeader = ({
  handlePrev,
  handleNext,
  handleChangeMonth,
  handleChangeYear,
  currentDate,
  setCurrentDate,
  selectMode,
  locale,
}: CalendarHeaderProps) => {
  const [isAnimation1Playing, setIsAnimation1Playing] = useState(false);
  const [isAnimation2Playing, setIsAnimation2Playing] = useState(false);

  const currentYear = useMemo(
    () => getYear(currentDate || new Date()),
    [currentDate]
  );

  const handleAnimation1Click = () => {
    handlePrev();
    setIsAnimation1Playing(!isAnimation1Playing);
  };

  const handleAnimation2Click = () => {
    handleNext();
    setIsAnimation2Playing(!isAnimation2Playing);
  };

  const allMonths = useMemo(
    () =>
      Array.from({ length: 12 }, (_, index) =>
        format(new Date(currentYear, index, 1), "MMMM", {
          locale,
        })
      ),
    [currentYear, locale]
  );

  const allYears = useMemo(
    () => Array.from({ length: 11 }, (_, i) => currentYear - 5 + i),
    [currentYear]
  );

  const onTodayClick = () => {
    if (setCurrentDate) setCurrentDate(new Date());
  };

  return (
    <Grid
      templateColumns="repeat(5, 1fr)"
      gap={3}
      alignItems="center"
      marginBottom={2}
    >
      <div className="calendar-buttons">
        <button className="small-button" onClick={handleAnimation1Click}>
          <AnimationComponent
            animationData={ArrowLeft}
            isPlaying={isAnimation1Playing}
          />
        </button>

        <button className="small-button" onClick={handleAnimation2Click}>
          {" "}
          <AnimationComponent
            animationData={ArrowRight}
            isPlaying={isAnimation2Playing}
          />
        </button>
      </div>
      <GridItem>
        <Button
          width="100%"
          bg="var(--button_color)"
          _hover={{
            bg: "var(--button_hover)",
          }}
          onClick={onTodayClick}
          isDisabled={currentDate?.toDateString() === new Date().toDateString()}
        >
          Today
        </Button>
      </GridItem>
      <GridItem>
        <Select
          value={currentDate?.getMonth()}
          onChange={(e) => handleChangeMonth(parseInt(e.target.value))}
          className="calendar-month"
        >
          {allMonths.map((month, index) => (
            <option key={month} value={index}>
              {month}
            </option>
          ))}
        </Select>
      </GridItem>
      <GridItem>
        <Select
          value={currentDate?.getFullYear()}
          onChange={(e) => handleChangeYear(parseInt(e.target.value))}
          className="calendar-year"
        >
          {allYears.map((year) => (
            <option key={year} value={year}>
              {year}
            </option>
          ))}
        </Select>
      </GridItem>
      <GridItem>{selectMode}</GridItem>
    </Grid>
  );
};

export default CalendarHeader;
