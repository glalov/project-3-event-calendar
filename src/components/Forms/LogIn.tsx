import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { emailText, loginText, passwordText } from "../../common/formTexts";
import { isValidEmail } from "../../common/inputValidations";
import {
  invalidEmail,
  invalidPassword,
  logInSuccessText,
  passwordEmailMismatch,
} from "../../common/validationMessages";
import FromButton from "./FormButton";
import FormGroup, { FormGroupProps } from "./FormGroup";
import {
  Flex,
  Box,
  Heading,
  Alert,
  AlertIcon,
  Avatar,
  Stack,
  Center,
} from "@chakra-ui/react";
import AuthContext from "../../context/AuthContext";
import { loginUser } from "../../services/auth.service";
import UseSuccessToast from "../../hooks/UseSuccessToast";
import { Link as ReactRouterLink } from "react-router-dom";
import { Link as ChakraLink } from "@chakra-ui/react";

/**
 * A component that renders a login form.
 * @returns JSX.Element
 */
const LogIn = () => {
  const [form, setForm] = useState({
    email: "",
    password: "",
  });
  const [validated, setValidated] = useState(false);
  const [loginError, setLoginError] = useState("");

  const { setUser } = useContext(AuthContext);
  const toast = UseSuccessToast();
  const navigate = useNavigate();

  const onEmailSingUp = (event: any) => {
    event.preventDefault();
    event.stopPropagation();

    setValidated(true);

    if (!areAllValid()) {
      return;
    }

    loginUser(form.email, form.password)
      .then((credential) => {
        setUser((prevState) => {
          return { ...prevState, user: credential.user };
        });
      })
      .then(() => {
        toast(logInSuccessText, "log-in-success");
        navigate("/");
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;

        if (
          errorCode === "auth/user-not-found" ||
          errorCode === "auth/wrong-password"
        ) {
          setLoginError(passwordEmailMismatch);
        } else {
          setLoginError(errorMessage);
        }
      });
  };

  const emailFormProps: FormGroupProps = {
    label: emailText,
    type: "text",
    name: "email",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: invalidEmail,
    classNames: "email",
    isValidFunc: isValidEmail,
  };

  const passwordFormProps: FormGroupProps = {
    label: passwordText,
    type: "password",
    name: "password",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: invalidPassword,
    classNames: "password",
    isValidFunc: () => true, //isValidPassword,
  };

  const areAllValid = () => {
    return isValidEmail(form.email); // && isValidPassword(form.password);
  };

  return (
    <Flex width="full" align="center" justifyContent="center">
      <Box p={2}>
        <Box textAlign="center">
          <Avatar size="xl" />
          <Heading>Welcome</Heading>
        </Box>
        <Box my={4} textAlign="left">
          <form className="log-form" onSubmit={onEmailSingUp}>
            <Stack spacing={4} p="1rem" boxShadow="md" borderWidth="2px">
              {loginError && (
                <Alert status="error">
                  <AlertIcon />
                  {loginError}
                </Alert>
              )}

              <FormGroup {...emailFormProps} />
              <FormGroup {...passwordFormProps} />

              <FromButton
                width="full"
                mt={4}
                type="submit"
                loadingText="Submitting"
                size="lg"
                bg={"blue.400"}
                color={"white"}
                _hover={{
                  bg: "blue.500",
                }}
              >
                {loginText}
              </FromButton>
            </Stack>
          </form>
        </Box>
        <Box>
          <Center>
            New to us?&nbsp;{" "}
            <ChakraLink color="teal.500" as={ReactRouterLink} to="/sign-up">
              Sign Up
            </ChakraLink>
          </Center>
        </Box>
      </Box>
    </Flex>
  );
};

export default LogIn;
