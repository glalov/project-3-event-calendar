# School Topia

## Name
Project 3 - Event Calendar - School Topia

## Description
Event Calendar Manager for education centers. The Event Calendar app is a modern solution for people and teams in need of flexible event scheduling. It allows users to create and schedule events and entire event series, and is rich in features like contacts lists, notifications.

## Project information
- Language and version: **TypeScript**
- Platform and version: **Node 14.0+**
- Core Packages: **TypeScript** **ESLint** **React** **Firebase** **Chakra**

## Installation
- Go to the main folder (where `package.json` is)
- Run `npm install` to install all the dependencies
- Run `npm run dev` to run the vite server
- Open the Local link in the browser

## Screenshots
![Public Page](public/PublicPage.PNG)
![Calendar Page](public/calendar.PNG)
![Week Page](public/weekView.PNG)
![Dictionary Page](public/dictionary.PNG)
![My Events Page](public/myevents.PNG)

## Database Structure
```json

{
  "events": {
    "-NeHGpBGRrDQVUvU-aI5": {
      "description": "We will learn the basics of geometry.",
      "dueDate": "2023-08-29T10:30:00.000Z",
      "image": "https://firebasestorage.googleapis.com/v0/b/project-school-d00c0.appspot.com/o/eventImages%2Fmath.JPG?alt=media&token=15bd25ba-0536-420f-8f0f-fc2b054281db",
      "isPrivate": true,
      "owner": "theteacher",
      "recurringRule": {
        "enabled": true,
        "frequency": "weeks",
        "interval": 1,
        "until": "2023-11-30"
      },
      "roomNumber": "",
      "selectedFriendLists": {
        "-NeHCpDNQW0dkdxNCF_h": true
      },
      "startDate": "2023-08-29T07:00:00.000Z",
      "title": "Math"
    },
    "-NeHHbCFU_p_zs6YV2pH": {
      "description": "",
      "dueDate": "2023-08-31T06:00:00.000Z",
      "image": "https://firebasestorage.googleapis.com/v0/b/project-school-d00c0.appspot.com/o/eventImages%2Fmath.JPG?alt=media&token=5aeed4c0-0993-4599-9e25-3af11e1ce6f9",
      "isPrivate": true,
      "owner": "theteacher",
      "recurringRule": {
        "enabled": true,
        "frequency": "weeks",
        "interval": 1,
        "until": "2024-04-30"
      },
      "roomNumber": "",
      "selectedFriendLists": {
        "-NeHCpDNQW0dkdxNCF_h": true
      },
      "startDate": "2023-08-31T05:00:00.000Z",
      "title": "Math"
    }
  },
  "friendLists": {
    "-NeHCpDNQW0dkdxNCF_h": {
      "members": {
        "caca2007": true,
        "danschleifer": true,
        "president": true,
        "spiro": true
      },
      "name": "10 A Class",
      "owner": "theteacher"
    },
    "-NeHc6oTFa0SK1-otppN": {
      "members": {
        "danschleifer": true,
        "spiro": true
      },
      "name": "VIP friends",
      "owner": "caca2007"
    }
  },
  "users": {
    "albertenstein": {
      "address": "",
      "createdOn": 1694669252923,
      "email": "enstein@abv.bg",
      "firstName": "Albert",
      "firstNameLowerCase": "albert",
      "isBlocked": false,
      "lastName": "Enstein",
      "lastNameLowerCase": "enstein",
      "phoneNumber": "0822222222",
      "role": "Teacher",
      "uid": "Flp22eu693Rb9Zv6oh3OM3Z7GX43",
      "username": "albertenstein"
    },
    "bigboss": {
      "address": "",
      "createdOn": 1694668111779,
      "email": "bigboss@abv.bg",
      "firstName": "Teodor",
      "firstNameLowerCase": "teodor",
      "isBlocked": false,
      "lastName": "Nikolov",
      "lastNameLowerCase": "nikolov",
      "phoneNumber": "0899999999",
      "role": "Principal",
      "uid": "49r0DjytqMU26lN6iaQGNKzFwlE3",
      "username": "bigboss"
    },
    "caca2007": {
      "address": "",
      "createdOn": 1694668849644,
      "email": "caca2007@abv.bg",
      "events": {
        "-NeHO6P97V8xF0Q5F07c": false,
        "-NeHSBk62rz-BcfwjqqK": false,
        "-NeHcDEcF7wc6lcnlG9l": false
      },
      "firstName": "Hristyan",
      "firstNameLowerCase": "hristyan",
      "friendLists": {
        "-NeHc6oTFa0SK1-otppN": true
      },
      "isBlocked": false,
      "lastName": "Georgiev",
      "lastNameLowerCase": "georgiev",
      "pending_notifications": {
        "-NeHbUOI0W4sT3NmhCqp": {
          "addedDate": 1694676940577,
          "isNew": false
        }
      },
      "phoneNumber": "0811111111",
      "profilePicture": "https://firebasestorage.googleapis.com/v0/b/project-school-d00c0.appspot.com/o/profilePictures%2Fcaca2007%2Fball.JPG?alt=media&token=72fc2444-8646-4413-872e-0d835aa8f986",
      "role": "Student",
      "uid": "rAeHepYIKkNJjLtZKB4zpP9hAUN2",
      "username": "caca2007"
    }
  }
}

```

## Authors
Team 05
- Kiril Kvitkovski	
- Hristiyan Georgiev	
- Georgi Lalov