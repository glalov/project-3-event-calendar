import { Grid, GridItem, Center, Text, VStack, Box } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { useMemo, useCallback, useContext } from "react";
import {
  addDays,
  eachDayOfInterval,
  endOfMonth,
  endOfWeek,
  format,
  getMonth,
  getWeek,
  getYear,
  startOfMonth,
  startOfWeek,
  subDays,
} from "date-fns";
import AuthContext from "../../context/AuthContext";
import CreateEventView from "../../views/CreateEventView";
import "./MonthMode.css";
import { checkEventExistsOnDate } from "../../common/utils";
import TooltipHover from "./TooltipHover";
import { EventsPerMonth } from "../../services/events.service";

const MonthMode = ({
  todayDate,
  currentDate,
  locale,
  weekStartsOn,
  onDateClick,
  onWeekClick,
}: {
  todayDate: Date;
  currentDate: Date;
  locale: Locale;
  weekStartsOn: 0 | 1 | 2 | 3 | 4 | 5 | 6 | undefined;
  onDateClick: (date: Date) => void;
  onWeekClick: (date: Date) => void;
}) => {
  const todayDay = useMemo(() => todayDate.getDate(), [todayDate]);
  const todayMonth = useMemo(() => todayDate.getMonth(), [todayDate]);
  const todayYear = useMemo(() => todayDate.getFullYear(), [todayDate]);
  const { userData, openModal } = useContext(AuthContext);
  const navigate = useNavigate();

  const weekStart = useMemo(
    () => startOfWeek(new Date(), { locale, weekStartsOn }),
    [locale, weekStartsOn]
  );

  const daysOfTheWeek = useMemo(
    () =>
      new Array(7).fill(null).map((_, index) => {
        const day = addDays(weekStart, index);
        return format(day, "eee", { locale, weekStartsOn });
      }),
    [weekStart, locale, weekStartsOn]
  );

  const firstDayOfCurrentMonth = useMemo(
    () => startOfMonth(currentDate),
    [currentDate]
  );

  const lastDayOfCurrentMonth = useMemo(
    () => endOfMonth(currentDate),
    [currentDate]
  );

  const lastWeekOfPrevMonth = useMemo(
    () =>
      eachDayOfInterval({
        start: startOfWeek(subDays(firstDayOfCurrentMonth, 1), {
          weekStartsOn,
        }),
        end: subDays(firstDayOfCurrentMonth, 1),
      }),
    [firstDayOfCurrentMonth, weekStartsOn]
  );

  const firstWeekOfNextMonth = useMemo(
    () =>
      eachDayOfInterval({
        start: addDays(lastDayOfCurrentMonth, 1),
        end: endOfWeek(addDays(lastDayOfCurrentMonth, 1), { weekStartsOn }),
      }),
    [lastDayOfCurrentMonth, weekStartsOn]
  );

  const daysInMonth = useMemo(
    () =>
      eachDayOfInterval({
        start: firstDayOfCurrentMonth,
        end: lastDayOfCurrentMonth,
      }),
    [firstDayOfCurrentMonth, lastDayOfCurrentMonth]
  );

  const days = useMemo(
    () => lastWeekOfPrevMonth.concat(daysInMonth, firstWeekOfNextMonth),
    [lastWeekOfPrevMonth, daysInMonth, firstWeekOfNextMonth]
  );

  const weeksOfTheMonth = useMemo(
    () =>
      Array.from({ length: days.length / 7 }, (_, index) =>
        days.slice(index * 7, (index + 1) * 7)
      ),
    [days]
  );

  const isEventOnDate = useMemo(() => {
    return (event: EventsPerMonth, day: Date) =>
      checkEventExistsOnDate(event, day);
  }, []);

  const isDayInCurrentMonth = useCallback(
    (day: number | Date) => {
      return (
        getMonth(day) === getMonth(currentDate) &&
        getYear(day) === getYear(currentDate)
      );
    },
    [currentDate]
  );

  const isTodayDay = useCallback(
    (day: Date) => {
      return (
        day.getDate() === todayDay &&
        day.getMonth() === todayMonth &&
        day.getFullYear() === todayYear
      );
    },
    [currentDate, todayDate]
  );

  const handleDateClick = (day: Date) => {
    openModal(
      <CreateEventView ownerUsername={userData?.username} startDate={day} />
    );
  };

  return (
    <>
      <Grid templateColumns="4% repeat(7, 1fr)">
        <GridItem border="1px" borderColor="lightgrey" key="weekNumber">
          <Center>#</Center>
        </GridItem>

        {daysOfTheWeek.map((day) => (
          <GridItem border="1px" borderColor="lightgrey" key={day}>
            <Center>
              <Text as="b">{day}</Text>
            </Center>
          </GridItem>
        ))}
      </Grid>
      {weeksOfTheMonth.map((week, index) => (
        <Grid key={index} templateColumns="4% repeat(7, 1fr)">
          <GridItem border="1px" borderColor="lightgrey" p={1}>
            <Center className="week-link" onClick={() => onWeekClick(week[0])}>
              {getWeek(week[0], { weekStartsOn })}
            </Center>
          </GridItem>

          {week.map((day) => (
            <div
              onClick={() => handleDateClick(day)}
              key={day.toString()}
              className={"event"}
            >
              <GridItem
                border="1px"
                borderColor="lightgrey"
                bg={isTodayDay(day) ? "yellow.200" : ""}
                opacity={isDayInCurrentMonth(day) ? 1 : 0.5}
                key={day.toString()}
                p={1}
                display="flex"
                flexDirection="column"
                _hover={{ bg: "#80ffe2" }}
              >
                <VStack alignItems="end">
                  <Text
                    onClick={(e) => {
                      e.stopPropagation();
                      onDateClick(day);
                    }}
                    className="day-link"
                  >
                    {format(day, "d")}
                  </Text>

                  <Box
                    display="flex"
                    flexDirection="column"
                    flex="1"
                    overflowY="auto"
                    minHeight="100px"
                  >
                    {userData?.events
                      ?.filter((event) => {
                        //return checkEventExistsOnDate(event, day);
                        return isEventOnDate(event, day);
                      })
                      .slice(0, 3) //take only the first 3 events per day
                      .map((event) => {
                        // Check if the event spans multiple days
                        const eventStartDate = new Date(event.startDate);
                        const eventDueDate = new Date(event.dueDate);
                        const isMultiDayEvent =
                          format(eventStartDate, "yyyy-MM-dd") !==
                          format(eventDueDate, "yyyy-MM-dd");

                        return (
                          <div
                            key={event.id}
                            onClick={(e) => {
                              e.stopPropagation();
                              navigate(`/event/${event.id}`);
                            }}
                            className="pointer-cursor"
                          >
                            <TooltipHover event={event}>
                              <Box
                                bg={
                                  isMultiDayEvent
                                    ? "var(--event_long)"
                                    : "var(--button_color)"
                                }
                                color="white"
                                p={1}
                                borderRadius="md"
                                fontSize="sm"
                                marginBottom="2px"
                                whiteSpace="nowrap"
                                overflow="hidden"
                                textOverflow="ellipsis"
                                maxW={{
                                  // the max-width for different screen sizes
                                  base: "100%",
                                  sm: "50px",
                                  md: "120px",
                                  lg: "200px",
                                }}
                              >
                                {`${format(
                                  new Date(event.startDate),
                                  "H:mm"
                                )} ${event.title}`}
                              </Box>
                            </TooltipHover>
                          </div>
                        );
                      })}
                  </Box>
                </VStack>
              </GridItem>
            </div>
          ))}
        </Grid>
      ))}
    </>
  );
};

export default MonthMode;
