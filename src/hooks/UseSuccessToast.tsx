import { useToast } from "@chakra-ui/react";

/**
 * Custom hook that returns a function to display a success toast notification using Chakra UI.
 * @returns A function that takes a description and an id as arguments and displays a success toast notification.
 */
const UseSuccessToast = () => {
  const toast = useToast();

  return (description: string, id: string) => {
    return toast({
      description,
      status: "success",
      duration: 5000,
      isClosable: true,
      position: "top",
      id,
    });
  };
};

export default UseSuccessToast;
