/**
 * Component for displaying a 404 error message.
 * @returns JSX.Element
 */
const NotFoundView = () => {
  return (
    <div>
      <h1>404</h1>
    </div>
  );
};

export default NotFoundView;
