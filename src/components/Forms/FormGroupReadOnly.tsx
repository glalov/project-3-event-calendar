import { FormControl, FormLabel, Input } from "@chakra-ui/react";

export type FormGroupReadOnlyProps = {
  value?: string;
  label: string;
  classNames?: string;
};

/**
 * Renders a read-only form group with a label and input field.
 * @param value - The value to display in the input field.
 * @param label - The label to display for the input field.
 * @param classNames - Additional CSS classes to apply to the form control.
 * @returns A read-only form group component.
 */
const FormGroupReadOnly = ({
  value,
  label,
  classNames,
}: FormGroupReadOnlyProps) => {
  return (
    <FormControl className={classNames || ""}>
      <FormLabel>{label}</FormLabel>
      <Input value={value || ""} isReadOnly={true} />
    </FormControl>
  );
};

export default FormGroupReadOnly;
