import { Button } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

/**
 * AuthMenu component displays two buttons for login and sign up.
 * When clicked, the buttons redirect the user to the corresponding pages.
 */
const AuthMenu = () => {
  const navigate = useNavigate();

  const onLoginRedirect = () => {
    navigate("/log-in");
  };

  const onSignUpRedirect = () => {
    navigate("/sign-up");
  };

  return (
    <>
      <Button onClick={onLoginRedirect}>Log In</Button>
      <Button onClick={onSignUpRedirect}>Sign Up</Button>
    </>
  );
};

export default AuthMenu;
