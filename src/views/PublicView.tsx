import SearchBar from "../components/ui/SearchBar";
import { useCallback, useEffect, useState } from "react";
import { EventsPerMonth, getAllEvents } from "../services/events.service";
import EventListPerMonth from "../components/Calendar/EventListPerMonth";
import { Center } from "@chakra-ui/react";

/**
 * PublicView component displays a list of public events and a search bar to filter them.
 * @returns A React component that displays a list of public events and a search bar to filter them.
 */
const PublicView = () => {
  const [events, setEvents] = useState<EventsPerMonth[]>([]);

  /**
   * Fetches all public events and sets the state with them.
   */
  const fetchPublicEvents = async () => {
    const fetchedEvents = await getAllEvents();
    const publicEvents = fetchedEvents.filter((ev) => ev.isPrivate === false);
    setEvents(publicEvents);
  };

  useEffect(() => {
    fetchPublicEvents();
  }, []);

  /**
   * Filters the events based on the search term and sets the state with the filtered events.
   * If the search term is empty, it fetches all public events and sets the state with them.
   * @param searchTerm The search term to filter the events by.
   * @param selectVal The value of the select input (not used in this function).
   */
  const searchPublicEvents = useCallback(
    (searchTerm: string, selectVal: string) => {
      if (searchTerm) {
        const filteredEvents = events.filter(
          (ev) =>
            ev.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
            ev.description.toLowerCase().includes(searchTerm.toLowerCase())
        );
        setEvents(filteredEvents);
      } else {
        fetchPublicEvents();
      }
    },
    [events]
  );

  return (
    <div>
      <div>
        <SearchBar
          onSearch={searchPublicEvents}
          searchPlaceholder="Search for events"
          buttonText="Find Events"
        />
      </div>

      {events && <EventListPerMonth events={events} />}
      {events.length === 0 && <Center>No events found</Center>}
    </div>
  );
};

export default PublicView;
