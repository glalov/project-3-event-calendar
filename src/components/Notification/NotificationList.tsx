import React, { useEffect, useState, useContext } from "react";
import { EventsPerMonth, getEventByID } from "../../services/events.service";
import { useNavigate } from "react-router-dom";
import AuthContext from "../../context/AuthContext";
import { updateNotificationIsNew } from "../../services/notification.service";
import { Box, Text, Flex, List, ListItem } from "@chakra-ui/react";
import { formatDate, formatTime } from "../../common/timeFormat";

interface Notification {
  id: string;
  isNew: boolean;
  addedDate: Date;
}

interface NotificationListProps {
  notifications: Notification[];
  isNewMap: Record<string, boolean>;
  setIsNewMap: React.Dispatch<React.SetStateAction<Record<string, boolean>>>;
}

/**
 * Renders a list of notifications with their associated events.
 * @param notifications - An array of notifications to display.
 * @param isNewMap - A map of notification IDs to boolean values indicating whether they are new.
 * @param setIsNewMap - A function to update the isNewMap state.
 * @returns A React component that displays a list of notifications.
 */
const NotificationList: React.FC<NotificationListProps> = ({
  notifications,
  isNewMap,
  setIsNewMap,
}) => {
  const [events, setEvents] = useState<EventsPerMonth[]>([]);
  const navigate = useNavigate();
  const { userData } = useContext(AuthContext);

  const sortedNotifications = [...notifications].sort(
    (a, b) => Number(b.addedDate) - Number(a.addedDate)
  );

  useEffect(() => {
    const initialIsNewMap: Record<string, boolean> = {};
    sortedNotifications.forEach((notification) => {
      initialIsNewMap[notification.id] = notification.isNew;
    });
    setIsNewMap(initialIsNewMap);

    Promise.all(
      sortedNotifications.map((notification) => getEventByID(notification.id))
    )
      .then((eventData) => setEvents(eventData))
      .catch((error) => console.log("Error fetching events"));
  }, [notifications]);

  const onNotificationClick = async (eventId: string) => {
    await updateNotificationIsNew(userData?.username, eventId, false);
    navigate("/event/" + eventId);
  };

  return (
    <Box>
      <Text fontSize="xl" fontWeight="bold" mb={4}>
        Your new notifications
      </Text>
      <List>
        {events.map((event) =>
          event ? (
            <ListItem
              key={event.id}
              onClick={() => onNotificationClick(event.id)}
              cursor="pointer"
              borderBottom="1px solid #ccc"
              py={3}
            >
              <Flex alignItems="center">
                <Box>
                  <Text fontSize="lg" fontWeight="semibold">
                    {event?.title}
                  </Text>
                  <Text fontSize="sm">
                    {formatDate(event.startDate)} @{" "}
                    {formatTime(event.startDate)}
                  </Text>
                </Box>
                {isNewMap[event.id] && (
                  <Box
                    ml={2}
                    bgColor="red"
                    color="white"
                    borderRadius="md"
                    p={1}
                  >
                    NEW
                  </Box>
                )}
              </Flex>
            </ListItem>
          ) : null
        )}
      </List>
    </Box>
  );
};

export default NotificationList;
