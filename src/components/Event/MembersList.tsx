import React, { useEffect } from "react";
import {
  Box,
  Heading,
  Input,
  Flex,
  List,
  ListItem,
  IconButton,
  Badge,
} from "@chakra-ui/react";
import { AddIcon, MinusIcon } from "@chakra-ui/icons";
import {
  getUsersByHandle,
  getUsersByFirstName,
  getUsersByLastName,
} from "../../services/users.service";

interface User {
  username: string;
  firstName: string;
  lastName: string;
}
interface MembersListProps {
  selectedMembers: User[];
  setSelectedMembers: React.Dispatch<React.SetStateAction<User[]>>;
  membersInList: User[];
  setMembersInList: React.Dispatch<React.SetStateAction<User[]>>;
  searchResults: User[];
  setSearchResults: React.Dispatch<React.SetStateAction<User[]>>;
  newMemberName: string;
  setNewMemberName: React.Dispatch<React.SetStateAction<string>>;
  ownerUsername: string;
  invitedMembersTitle: string;
}

/**
 * A component that displays a list of members and allows the user to search for and add new members.
 * @param selectedMembers - An array of users that have been selected to attend the event.
 * @param setSelectedMembers - A function that sets the selectedMembers array.
 * @param membersInList - An array of users that are currently in the members list.
 * @param setMembersInList - A function that sets the membersInList array.
 * @param searchResults - An array of users that match the search query.
 * @param setSearchResults - A function that sets the searchResults array.
 * @param newMemberName - The name of the new member being added.
 * @param setNewMemberName - A function that sets the newMemberName.
 * @param invitedMembersTitle - The title of the invited members list.
 */
const MembersList: React.FC<MembersListProps> = ({
  selectedMembers,
  setSelectedMembers,
  membersInList,
  setMembersInList,
  searchResults,
  setSearchResults,
  newMemberName,
  setNewMemberName,
  invitedMembersTitle = "Invited Members",
}) => {
  const handleNewMemberChange = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const input = event.target.value;
    setNewMemberName(input);

    if (input) {
      try {
        const queryValue = input;

        const handleSnapshot = await getUsersByHandle(queryValue);
        const firstNameResults = await getUsersByFirstName(queryValue);
        const lastNameResults = await getUsersByLastName(queryValue);
        const handleData = handleSnapshot.val();
        const handleResults = handleData ? Object.values(handleData) : [];

        const results = [
          ...handleResults,
          ...firstNameResults,
          ...lastNameResults,
        ] as User[];

        setSearchResults(results);
      } catch (error) {
        console.error("Error fetching users:", error);
        setSearchResults([]);
      }
    } else {
      setSearchResults([]);
    }
  };

  const handleRemoveFromMembersList = (member: User) => {
    setSelectedMembers(
      selectedMembers.filter((user) => user.username !== member.username)
    );
    setMembersInList(
      membersInList.filter((user) => user.username !== member.username)
    );

    // Check if the removed user is already in the search results
    const userInSearchResults = searchResults.find(
      (result) => result.username === member.username
    );

    // If the user is not in the search results, add them back
    if (!userInSearchResults) {
      setSearchResults([...searchResults, member]);
    }
  };

  const handleMoveToMembersList = (username: string) => {
    const userToAdd = searchResults.find(
      (result) => result.username === username
    );

    if (userToAdd) {
      // Check if the user is already in the Members in List
      if (!membersInList.some((user) => user.username === userToAdd.username)) {
        setSelectedMembers(
          selectedMembers.filter((user) => user.username !== username)
        );

        setMembersInList([...membersInList, userToAdd]);

        setSearchResults(
          searchResults.filter((result) => result.username !== username)
        );
      }
    }
  };

  useEffect(() => {
    if (!newMemberName) {
      setSearchResults([]);
    }
  }, [newMemberName]);

  return (
    <Flex>
      <Box flex={1} mr={4}>
        <Heading as="h4" size="md" mb={2}>
          {invitedMembersTitle}
        </Heading>
        <List spacing={2}>
          {membersInList.map((member) => (
            <ListItem key={member.username}>
              {member.firstName} {member.lastName}
              <Badge
                m={2}
                key={member.username}
                variant="subtle"
                colorScheme="green"
              >
                {member.username}
              </Badge>
              <IconButton
                ml={2}
                icon={<MinusIcon />}
                onClick={() => handleRemoveFromMembersList(member)}
                aria-label={""}
              />
            </ListItem>
          ))}
        </List>
      </Box>

      <Box flex={1}>
        <Heading as="h4" size="md" mb={2}>
          Add Members
        </Heading>
        <Flex>
          <Input
            flex={1}
            size="lg"
            value={newMemberName}
            onChange={handleNewMemberChange}
            placeholder="Search members..."
          />
        </Flex>
        <List spacing={2} mt={4}>
          {searchResults.map((member) => (
            <ListItem key={member.username}>
              {member.firstName} {member.lastName}
              <Badge
                m={2}
                key={member.username}
                variant="subtle"
                colorScheme="green"
              >
                {member.username}
              </Badge>
              <IconButton
                aria-label={""}
                ml={2}
                icon={<AddIcon />}
                onClick={() => handleMoveToMembersList(member.username)}
              />
            </ListItem>
          ))}
        </List>
      </Box>
    </Flex>
  );
};

export default MembersList;
