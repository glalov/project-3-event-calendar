import { formatDate, formatTime } from "./timeFormat";

/**
 * Formats start and due dates into a readable format for display.
 * @param startDate - The start date of the event.
 * @param dueDate - The due date of the event.
 * @returns An object containing the formatted start and due dates.
 */
export const displayBothDates = (startDate: string, dueDate: string) => {
  const startFormattedDate = formatDate(startDate);
  const startFormattedTime = formatTime(startDate);
  const dueFormattedDate = formatDate(dueDate);
  const dueFormattedTime = formatTime(dueDate);

  const isSameDate =
    startFormattedDate === dueFormattedDate &&
    startFormattedTime !== dueFormattedTime;

  const formattedStartDate = isSameDate
    ? `${startFormattedDate} @ ${startFormattedTime} - ${dueFormattedTime}`
    : `${startFormattedDate} @ ${startFormattedTime}`;

  const formattedDueDate = isSameDate
    ? ""
    : ` until ${dueFormattedDate} @ ${dueFormattedTime}`;

  return {
    formattedStartDate,
    formattedDueDate,
  };
};
