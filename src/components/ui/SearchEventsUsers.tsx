import { useCallback, useContext, useEffect, useState } from "react";
import AuthContext from "../../context/AuthContext";
import { EventsPerMonth, getAllEvents } from "../../services/events.service";
import { Role, getAllUsers } from "../../services/users.service";
import SearchBar from "./SearchBar";
import EventListPerMonth from "../Calendar/EventListPerMonth";
import { Center } from "@chakra-ui/react";
import UserList from "./UserList";

/**
 * Component that allows searching for events and users.
 * Displays a search bar with a dropdown to select the search mode (event or user).
 * Displays a list of events or users based on the search mode and search term.
 * Limits the number of displayed elements to 50.
 */
const SearchEventsUsers = () => {
  const elementsLimit = 50;

  const { userData } = useContext(AuthContext);

  const [events, setEvents] = useState<EventsPerMonth[]>([]);
  const [users, setUsers] = useState<any[]>([]);
  const [searchValue, setSearchValue] = useState("");

  enum SearchMode {
    Event = "event",
    User = "user",
  }
  const [searchMode, setSearchMode] = useState<SearchMode>(SearchMode.Event);

  /**
   * Fetches all events from the server and filters them based on the user's role and search term.
   * Sets the events state to the fetched and filtered events.
   */
  const fetchEvents = async () => {
    let fetchedEvents = await getAllEvents();

    // If not admin filter private events
    if (userData?.role !== Role.Principal) {
      const ownEventIds = userData?.events.map((ev) => ev.id);
      fetchedEvents = fetchedEvents.filter(
        (ev) => ev.isPrivate === false || ownEventIds?.includes(ev.id)
      );
    }

    if (searchValue === "") {
      setEvents(fetchedEvents.slice(0, elementsLimit));
    } else {
      const filteredEvents = fetchedEvents.filter(
        (ev) =>
          ev.title.toLowerCase().includes(searchValue.toLowerCase()) ||
          ev.description.toLowerCase().includes(searchValue.toLowerCase())
      );
      setEvents(filteredEvents.slice(0, elementsLimit));
    }
  };

  /**
   * Fetches all users from the server and filters them based on the current user and search term.
   * Sets the users state to the fetched and filtered users.
   */
  const fetchUsers = async () => {
    let fetchedUsers = await getAllUsers();
    fetchedUsers = fetchedUsers.filter(
      (user) => user.username !== userData?.username
    );

    if (searchValue === "") {
      setUsers(fetchedUsers.slice(0, elementsLimit));
    } else {
      const filteredUsers = fetchedUsers.filter(
        (user) =>
          user.username.toLowerCase().includes(searchValue.toLowerCase()) ||
          user.email.toLowerCase().includes(searchValue.toLowerCase()) ||
          user.firstName.toLowerCase().includes(searchValue.toLowerCase()) ||
          user.lastName.toLowerCase().includes(searchValue.toLowerCase()) ||
          user.phoneNumber.toLowerCase().includes(searchValue.toLowerCase())
      );
      setUsers(filteredUsers.slice(0, elementsLimit));
    }
  };

  useEffect(() => {
    fetchEvents();
    fetchUsers();
  }, []);

  useEffect(() => {
    if (searchMode === SearchMode.Event) {
      fetchEvents();
    } else if (searchMode === SearchMode.User) {
      fetchUsers();
    }
  }, [searchValue, searchMode]);

  /**
   * Callback function that sets the search term and search mode states.
   * Triggers a fetch of events or users based on the selected search mode.
   * @param searchTerm - The search term entered by the user.
   * @param selectVal - The selected search mode (event or user).
   */
  const onSearch = useCallback((searchTerm: string, selectVal: string) => {
    setSearchValue(searchTerm);
    setSearchMode(selectVal as SearchMode);
  }, []);

  return (
    <div>
      <div>
        <SearchBar
          onSearch={onSearch}
          searchPlaceholder="Search..."
          buttonText="Find"
          selectDefaultValue={searchMode}
        >
          <option value="event">Event</option>
          <option value="user">User</option>
        </SearchBar>
      </div>

      {searchMode === SearchMode.Event && events.length > 0 && (
        <EventListPerMonth events={events} />
      )}
      {searchMode === SearchMode.Event && events.length === 0 && (
        <Center>No events found</Center>
      )}
      {searchMode === SearchMode.User && users.length > 0 && (
        <UserList
          key={users.map((user) => user.username).join()}
          users={users}
        />
      )}
      {searchMode === SearchMode.User && users.length === 0 && (
        <Center>No users found</Center>
      )}
    </div>
  );
};

export default SearchEventsUsers;
