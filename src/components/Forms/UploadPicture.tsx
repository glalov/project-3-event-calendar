import { useRef, useState } from "react";
import {
  Alert,
  Button,
  FormControl,
  Icon,
  Input,
  HStack,
} from "@chakra-ui/react";
import { FiFile } from "react-icons/fi";

/**
 * A component that allows the user to upload a profile picture.
 * @param {Object} props - The component props.
 * @param {string} props.username - The username of the user whose profile picture is being uploaded.
 * @returns {JSX.Element} - The rendered component.
 */
const UploadPicture = ({
  handleUpload,
}: {
  handleUpload: (event: any, selectedFile: any, setInvalidMsg: any) => void;
}) => {
  const [selectedFile, setSelectedFile] = useState(null);
  const [invalidMsg, setInvalidMsg] = useState("");
  const [selectedFilename, setSelectedFilename] = useState("");

  const inputRef = useRef<HTMLInputElement | null>(null);

  const handleChooseClick = () => inputRef.current?.click();

  const handleFileChange = (event: any) => {
    setSelectedFile(event.target.files[0]);
    setSelectedFilename(event.target.files[0]?.name || "");
    setInvalidMsg("");
  };

  const handleUploadClick = (event: any) => {
    handleUpload(event, selectedFile, setInvalidMsg);

    setSelectedFilename("");
  };

  return (
    <form onSubmit={(event) => handleUploadClick(event)}>
      <FormControl>
        <Input
          hidden
          type="file"
          accept="image/*"
          onChange={handleFileChange}
          ref={(e) => {
            inputRef.current = e;
          }}
        />
        <HStack spacing={4}>
          <Button onClick={handleChooseClick} leftIcon={<Icon as={FiFile} />}>
            Choose Photo
          </Button>
          <Button type="submit">Upload</Button>
        </HStack>
        {selectedFilename && (
          <Alert status="success">You've selected: {selectedFilename}</Alert>
        )}
        {invalidMsg && <Alert status="error">{invalidMsg}</Alert>}
      </FormControl>
    </form>
  );
};

export default UploadPicture;
