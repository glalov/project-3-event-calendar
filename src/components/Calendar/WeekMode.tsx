import { Grid, GridItem } from "@chakra-ui/react";
import DayMode from "./DayMode";
import { eachDayOfInterval, endOfWeek, startOfWeek } from "date-fns";
import { useMemo } from "react";

/**
 * Renders a week mode calendar view.
 * @param todayDate - The current date.
 * @param currentDate - The date of the current week.
 * @param locale - The locale to use for formatting.
 * @param weekStartsOn - The day on which the week starts.
 * @param showWorkWeek - Whether to show only the work week (Monday to Friday).
 * @param onDateClick - The function to call when a date is clicked.
 * @returns The week mode calendar view.
 */
const WeekMode = ({
  todayDate,
  currentDate,
  locale,
  weekStartsOn,
  showWorkWeek = false,
  onDateClick,
}: {
  todayDate: Date;
  currentDate: Date;
  locale: Locale;
  weekStartsOn: 0 | 1 | 2 | 3 | 4 | 5 | 6 | undefined;
  showWorkWeek?: boolean;
  onDateClick: (date: Date) => void;
}) => {
  const weekDays = useMemo(
    () =>
      eachDayOfInterval({
        start: startOfWeek(currentDate, { locale, weekStartsOn }),
        end: endOfWeek(currentDate, { locale, weekStartsOn }),
      }),
    [locale, weekStartsOn, currentDate]
  );

  // Remove Saturday and Sunday from weekDays
  if (showWorkWeek) {
    if (weekStartsOn === 0) {
      weekDays.splice(6, 1);
      weekDays.splice(0, 1);
    }
    if (weekStartsOn === 1) weekDays.splice(5, 2);
  }

  return (
    <Grid templateColumns={`repeat(${weekDays.length}, 1fr)`}>
      {weekDays.map((day, index) => (
        <GridItem key={index}>
          <DayMode
            todayDate={todayDate}
            currentDate={day}
            locale={locale}
            weekStartsOn={weekStartsOn}
            showHoursLegend={index === 0}
            showShortNames={true}
            onDateClick={onDateClick}
          />
        </GridItem>
      ))}
    </Grid>
  );
};

export default WeekMode;
