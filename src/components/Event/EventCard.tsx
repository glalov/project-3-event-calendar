import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./EventCard.css";

interface EventCardProps {
  eventId: string;
  eventData: any;
  username: string | undefined;
}

/**
 * EventCard component displays an event card with event details.
 * @param {string} eventId - The id of the event.
 * @param {object} eventData - The data of the event.
 * @param {string} username - The username of the event organizer.
 * @returns {JSX.Element} - A React component that displays an event card.
 */
const EventCard: React.FC<EventCardProps> = ({
  eventId,
  eventData,
  username,
}) => {
  const [isHovered, setIsHovered] = useState(false);
  const navigate = useNavigate();
  const startDate = new Date(eventData.startDate);
  const dayOfMonth = startDate.getDate();
  const endDate = new Date(eventData.dueDate);
  const formattedStartDate =
    startDate.toLocaleString("en-US", {
      weekday: "short",
      month: "short",
      day: "numeric",
      hour: "numeric",
      minute: "2-digit",
    }) +
    " - " +
    endDate.toLocaleTimeString([], { hour: "2-digit", minute: "2-digit" });
  const formattedMonth = startDate.toLocaleString("default", {
    month: "short",
  });

  /**
   * Handles the click event on the event card.
   * Navigates to the event details page with the event data.
   */
  const handleCardClick = () => {
    navigate(`/event/${eventData?.id}`, {
      state: {
        eventId: eventId,
        eventData: eventData,
        username: username,
      },
    });
  };

  return (
    <div
      className={`event-card ${
        isHovered ? "hover:bg-purple-400 hover:scale-105 hover:rounded-xl" : ""
      }`}
      onClick={handleCardClick}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <div className="date-container">
        <div className="date">{dayOfMonth}</div>
        <div className="month">{formattedMonth}</div>
      </div>
      <div className="details-container">
        <div className="title">{eventData.title}</div>
        <div className="location">{eventData.location}</div>
        <div className="event-info">
          <div>
            <i className="far fa-clock"></i> {formattedStartDate}
          </div>
          <div>Organizer: {username}</div>
        </div>
      </div>
    </div>
  );
};

export default EventCard;
