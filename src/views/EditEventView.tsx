import React, { useContext, useEffect, useState } from "react";
import {
  Box,
  Flex,
  Heading,
  Button,
  Checkbox,
  Input,
  FormControl,
  FormLabel,
  Textarea,
  Select,
  FormErrorMessage,
} from "@chakra-ui/react";
import MembersList from "../components/Event/MembersList";
import { invitedMembersTitleEvent } from "../common/constants";
import FriendListModule from "../components/Event/FriendListModule";
import {
  getEventByID,
  updateEventInDatabase,
} from "../services/events.service";
import { getUserByHandle } from "../services/users.service";
import UseSuccessToast from "../hooks/UseSuccessToast";
import {
  eventEditSuccess,
  imageUploadSuccess,
  invalidDate,
  invalidDescription,
  invalidRecurrence,
  invalidStartDueDates,
  invalidTitle,
} from "../common/validationMessages";
import UploadPicture from "../components/Forms/UploadPicture";
import uploadImage from "../services/storage.service";
import { User } from "../common/interfaces";
import { EventData, RecurringRule } from "../common/interfaces";
import { format } from "date-fns";
import { getFriendListById } from "../services/friends-list.service";
import AuthContext from "../context/AuthContext";
import {
  areValidStartDueDates,
  isRecurrenceIntervalValid,
  isValidDate,
  isValidDescription,
  isValidTitle,
} from "../common/inputValidations";

interface EditEventViewProps {
  ownerUsername: string | undefined;
  eventId: string;
  updateEventData: (updatedEvent: EventData) => void;
}

interface FriendList {
  members: { [key: string]: boolean };
  id: string;
  name: string;
}

/**
 * The edit event view allows the user to edit an event.
 */
const EditEventView: React.FC<EditEventViewProps> = ({
  ownerUsername,
  eventId,
  updateEventData,
}) => {
  const [showDescription, setShowDescription] = useState(false);
  const [recurring, setRecurring] = useState(false);

  const cleanEvent = {
    title: "",
    description: "",
    isPrivate: true,
    roomNumber: "",
    startDate: "",
    dueDate: "",
    startHours: "",
    dueHours: "",
    recurringRule: {
      enabled: false,
      frequency: "",
      interval: 0,
      until: "",
    },
    image: "",
    owner: ownerUsername,
    selectedMembers: {},
    selectedFriendLists: {},
  };

  const [form, setForm] = useState(cleanEvent);
  const toast = UseSuccessToast();
  const [eventImage, setEventImage] = useState("");
  const { closeModal } = useContext(AuthContext);

  const [validated, setValidated] = useState(false);
  const [invalidDueDateText, setInvalidDueDateText] = useState("");

  // State variables for MembersList
  const [newMemberName, setNewMemberName] = useState("");
  const [selectedMembers, setSelectedMembers] = useState<User[]>([]);
  const [membersInList, setMembersInList] = useState<User[]>([]);
  const [searchResults, setSearchResults] = useState<User[]>([]);

  // State variable for FriendListModule
  const [selectedFriendLists, setSelectedFriendLists] = useState<FriendList[]>(
    []
  );
  //   const [friendLists, setFriendLists] = useState<FriendList[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const event = await getEventByID(eventId);
      if (event) {
        const startDate = format(new Date(event.startDate), "yyyy-MM-dd");
        const startTime = format(new Date(event.startDate), "HH:mm");
        const dueDate = format(new Date(event.dueDate), "yyyy-MM-dd");
        const dueTime = format(new Date(event.dueDate), "HH:mm");

        setRecurring(event.recurringRule?.enabled || false);

        if (event.selectedMembers) {
          const handles = Object.keys(event.selectedMembers);
          const fetchUser = async (handle: string) => {
            const userData = await getUserByHandle(handle);
            return userData;
          };

          Promise.all(handles.map((handle) => fetchUser(handle)))
            .then((users) => {
              return users.map((user) => {
                const userData = user.val();
                return {
                  username: userData.username,
                  firstName: userData.firstName,
                  lastName: userData.lastName,
                };
              });
            })
            .then((data) => setMembersInList(data))
            .catch((error) => {
              console.error("Error fetching user data:", error);
            });
        }

        if (event.selectedFriendLists) {
          const listIDs = Object.keys(event.selectedFriendLists);
          const fetchList = async (listId: string) => {
            const userData = await getFriendListById(listId);
            return userData;
          };

          Promise.all(listIDs.map((listId) => fetchList(listId)))
            .then((lists) => {
              const friendListsData = lists
                .filter((list) => list.val())
                .map((list, index) => {
                  const listData = list.val();
                  return {
                    name: listData.name,
                    id: listIDs[index],
                  } as FriendList;
                });

              setSelectedFriendLists(friendListsData);
            })
            .catch((error) => {
              console.error("Error fetching user data:", error);
            });
        }

        setEventImage(event.image);
        setForm({
          ...event,
          description: event.description,
          isPrivate: event.isPrivate,
          roomNumber: event.roomNumber,
          recurringRule: {
            ...event.recurringRule,
          },
          startDate: startDate,
          startHours: startTime,
          dueDate: dueDate,
          dueHours: dueTime,
        });
      }
    };

    fetchData();
  }, [eventId]);

  const updateForm = (
    prop: keyof typeof form,
    value: string | boolean | RecurringRule
  ) => {
    if (form) {
      setForm({
        ...form,
        [prop]: value,
      });
    }
  };

  const handleUpload = async (
    event: React.MouseEvent<HTMLButtonElement>,
    selectedFile: File | null,
    setInvalidMsg: (msg: string) => void
  ) => {
    event.preventDefault();
    if (!selectedFile) {
      setInvalidMsg("Please select an image to upload.");
      return;
    }

    try {
      const imageUrl = await uploadImage(selectedFile);
      setEventImage(imageUrl || "");
      setInvalidMsg("");
      toast(imageUploadSuccess, "img-success");
    } catch (error) {
      console.error("Error uploading image:", error);
      setInvalidMsg("Error uploading image. Please try again.");
    }
  };

  const handleSaveEdit = async (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.preventDefault();
    event.stopPropagation();

    setValidated(true);

    if (!areAllValid()) {
      if (!isValidDate(form.dueDate)) {
        setInvalidDueDateText(invalidDate);
      } else if (
        !areValidStartDueDates(
          form.startDate,
          form.startHours,
          form.dueDate,
          form.dueHours
        )
      ) {
        setInvalidDueDateText(invalidStartDueDates);
      }

      return;
    }

    const selectedMemberUsernames = membersInList.reduce((acc, member) => {
      acc[member.username] = true;
      return acc;
    }, {} as { [key: string]: boolean });

    const selectedFriendListIds = selectedFriendLists.reduce((acc, list) => {
      acc[list.id] = true;
      return acc;
    }, {} as { [key: string]: boolean });

    const startDateWithHours = new Date(
      form.startDate + " " + form.startHours
    ).toISOString();
    const dueDateWithHours = new Date(
      form.dueDate + " " + form.dueHours
    ).toISOString();

    let recurringRule: RecurringRule | null = null;
    if (recurring) {
      recurringRule = {
        enabled: true,
        frequency: form.recurringRule?.frequency || "days",
        interval: form.recurringRule?.interval || 1,
        until: form.recurringRule?.until || "",
      };
    }

    const eventData: EventData = {
      title: form.title,
      description: form.description,
      isPrivate: form.isPrivate,
      roomNumber: form.roomNumber,
      owner: ownerUsername || "",
      recurringRule,
      image: eventImage,
      startDate: startDateWithHours,
      dueDate: dueDateWithHours,
      selectedMembers: selectedMemberUsernames,
      selectedFriendLists: selectedFriendListIds,
    };

    try {
      await updateEventInDatabase(eventId, eventData);
      toast(eventEditSuccess, "event-success");
      updateEventData(eventData);
      closeModal();
    } catch (error) {
      console.error("Error updating event:", error);
    }
  };

  const areAllValid = () => {
    return (
      isValidTitle(form?.title) &&
      isValidDescription(form.description) &&
      isValidDate(form.startDate) &&
      isValidDate(form.dueDate) &&
      areValidStartDueDates(
        form.startDate,
        form.startHours,
        form.dueDate,
        form.dueHours
      ) &&
      (!recurring || isRecurrenceIntervalValid(form.recurringRule?.interval)) &&
      (!recurring ||
        (isValidDate(form.recurringRule?.until) &&
          areValidStartDueDates(
            form.dueDate,
            form.dueHours,
            form.recurringRule?.until,
            "11:59 PM"
          )))
    );
  };

  return (
    <Box p={4}>
      <Heading as="h1" mb={4}>
        Edit Event
      </Heading>

      <Box p={4}>
        <UploadPicture handleUpload={handleUpload} />
      </Box>

      <form>
        <FormControl
          isRequired={true}
          isInvalid={!isValidTitle(form.title) && validated}
        >
          <Flex alignItems="center" mb={4}>
            <FormLabel flex="0 0 120px">Event Title</FormLabel>
            <Input
              flex="1"
              placeholder="Event Title"
              ml={4}
              value={form?.title || ""}
              onChange={(e) => updateForm("title", e.target.value)}
            />
          </Flex>
          <FormErrorMessage>{invalidTitle}</FormErrorMessage>
        </FormControl>

        <FormControl
          mt={4}
          isRequired={true}
          isInvalid={!isValidDescription(form.description) && validated}
        >
          <FormLabel>
            <Checkbox
              onChange={() => setShowDescription(!showDescription)}
              isChecked={showDescription}
            >
              Include Description
            </Checkbox>
          </FormLabel>
          {showDescription && (
            <Textarea
              onChange={(e) => updateForm("description", e.target.value)}
              value={form?.description || ""}
              mt={2}
              placeholder="Event Description"
            />
          )}
          <FormErrorMessage>{invalidDescription}</FormErrorMessage>
        </FormControl>

        <Flex mt={4}>
          <FormControl>
            <Checkbox
              onChange={(e) => updateForm("isPrivate", !form?.isPrivate)}
              isChecked={!form?.isPrivate}
            >
              This event is Public
            </Checkbox>
          </FormControl>
          <FormControl>
            <Flex alignItems="center">
              <FormLabel>Location:</FormLabel>
              <Input
                placeholder="Room number"
                ml={2}
                value={form?.roomNumber || ""}
                onChange={(e) => updateForm("roomNumber", e.target.value)}
              />
            </Flex>
          </FormControl>
        </Flex>

        <Flex mt={4}>
          <FormControl
            isRequired={true}
            isInvalid={!isValidDate(form.startDate) && validated}
          >
            <FormLabel>Start Date</FormLabel>
            <Input
              type="date"
              value={form?.startDate || ""}
              onChange={(e) => updateForm("startDate", e.target.value)}
            />
            <FormErrorMessage>{invalidDate}</FormErrorMessage>
          </FormControl>
          <FormControl isRequired={true} ml={4}>
            <FormLabel>Start Hours</FormLabel>
            <Input
              type="time"
              value={form?.startHours || ""}
              onChange={(e) => updateForm("startHours", e.target.value)}
            />
          </FormControl>
        </Flex>

        <Flex mt={4}>
          <FormControl
            isRequired={true}
            isInvalid={
              (!isValidDate(form.dueDate) ||
                !areValidStartDueDates(
                  form.startDate,
                  form.startHours,
                  form.dueDate,
                  form.dueHours
                )) &&
              validated
            }
          >
            <FormLabel>Due Date</FormLabel>
            <Input
              type="date"
              value={form?.dueDate || ""}
              onChange={(e) => updateForm("dueDate", e.target.value)}
            />
            <FormErrorMessage>{invalidDueDateText}</FormErrorMessage>
          </FormControl>
          <FormControl ml={4}>
            <FormLabel>Due Hours</FormLabel>
            <Input
              type="time"
              value={form?.dueHours || ""}
              onChange={(e) => updateForm("dueHours", e.target.value)}
            />
          </FormControl>
        </Flex>

        <Flex mt={4}>
          <Checkbox
            onChange={() => setRecurring(!recurring)}
            isChecked={recurring}
          >
            Recurring
          </Checkbox>
        </Flex>
        {recurring && (
          <Flex mt={2} ml={4} alignItems="center">
            <FormControl
              isRequired={true}
              isInvalid={
                !isRecurrenceIntervalValid(form.recurringRule?.interval) &&
                validated
              }
            >
              <Flex>
                <FormLabel>Every:</FormLabel>
                <Input
                  type="number"
                  value={form?.recurringRule?.interval || ""}
                  onChange={(e) =>
                    updateForm("recurringRule", {
                      ...form?.recurringRule,
                      interval: parseInt(e.target.value),
                    })
                  }
                  w="50px"
                  ml={2}
                />
                <Select
                  ml={2}
                  value={form?.recurringRule?.frequency || "days"}
                  onChange={(e) =>
                    updateForm("recurringRule", {
                      ...form?.recurringRule,
                      frequency: e.target.value,
                    })
                  }
                  w="100px"
                >
                  <option value="days">day(s)</option>
                  <option value="weeks">week(s)</option>
                  <option value="months">month(s)</option>
                  <option value="years">year(s)</option>
                </Select>
              </Flex>
              <FormErrorMessage>{invalidRecurrence}</FormErrorMessage>
            </FormControl>
            <FormControl
              isRequired={true}
              isInvalid={
                (!isValidDate(form.recurringRule?.until) ||
                  !areValidStartDueDates(
                    form.dueDate,
                    form.dueHours,
                    form.recurringRule?.until,
                    "11:59 PM"
                  )) &&
                validated
              }
            >
              <Flex>
                <FormLabel flex="0 0 140px" ml={2}>
                  Last Event Date:
                </FormLabel>
                <Input
                  type="date"
                  value={form?.recurringRule?.until || ""}
                  onChange={(e) =>
                    updateForm("recurringRule", {
                      ...form?.recurringRule,
                      until: e.target.value,
                    })
                  }
                  w="150px"
                />
              </Flex>
            </FormControl>
          </Flex>
        )}
        <MembersList
          selectedMembers={selectedMembers}
          setSelectedMembers={setSelectedMembers}
          membersInList={membersInList}
          setMembersInList={setMembersInList}
          searchResults={searchResults}
          setSearchResults={setSearchResults}
          newMemberName={newMemberName}
          setNewMemberName={setNewMemberName}
          ownerUsername={ownerUsername || ""}
          invitedMembersTitle={invitedMembersTitleEvent}
        />

        <FriendListModule
          username={ownerUsername || ""}
          selectedFriendLists={selectedFriendLists}
          setSelectedFriendLists={setSelectedFriendLists}
          // friendLists={friendLists}
          // setFriendLists={setFriendLists}
        />

        <Button onClick={handleSaveEdit} type="submit">
          Save Edit
        </Button>
      </form>
    </Box>
  );
};

export default EditEventView;
