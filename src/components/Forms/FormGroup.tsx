import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
} from "@chakra-ui/react";

export type FormGroupProps = {
  label: string;
  type: string;
  name: string;
  form: { [key: string]: any };
  setForm: React.Dispatch<React.SetStateAction<any>>;
  validated: boolean;
  invalidMsg: string;
  classNames: string;
  isValidFunc?: (key: string) => boolean;
  isValid?: boolean;
  isRequired?: boolean;
};

/**
 * FormGroup component that renders a form control with a label, input, and error message.
 * @param label - The label for the form control.
 * @param type - The type of input for the form control.
 * @param name - The name of the input for the form control.
 * @param form - The form object containing the input values.
 * @param setForm - The function to update the form object.
 * @param validated - A boolean indicating whether the input has been validated.
 * @param invalidMsg - The error message to display if the input is invalid.
 * @param classNames - The class names to apply to the form control.
 * @param isValidFunc - An optional function to validate the input value.
 * @param isValid - An optional boolean indicating whether the input is valid.
 * @param isRequired - An optional boolean indicating whether the input is required.
 * @returns A form control with a label, input, and error message.
 */
const FormGroup = ({
  label,
  type,
  name,
  form,
  setForm,
  validated,
  invalidMsg,
  classNames,
  isValidFunc,
  isValid,
  isRequired,
}: FormGroupProps) => {
  const updateForm = (e: { target: { name: any; value: any } }) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const isInputValid = isValidFunc ? isValidFunc(form[name]) : isValid;

  return (
    <FormControl
      className={classNames}
      isInvalid={!isInputValid && validated}
      isRequired={isRequired !== undefined ? isRequired : true}
    >
      <FormLabel>{label}</FormLabel>
      <Input
        type={type}
        name={name}
        placeholder={label}
        value={form[name]}
        onChange={updateForm}
        isInvalid={!isInputValid && validated}
        isRequired={isRequired !== undefined ? isRequired : true}
      />
      <FormErrorMessage>{invalidMsg}</FormErrorMessage>
    </FormControl>
  );
};

export default FormGroup;
