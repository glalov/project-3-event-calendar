import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  Select,
} from "@chakra-ui/react";

export type FormSelectGroupProps = {
  label: string;
  name: string;
  form: { [key: string]: any };
  setForm: React.Dispatch<React.SetStateAction<any>>;
  validated: boolean;
  invalidMsg: string;
  classNames: string;
  isValidFunc?: (key: string) => boolean;
  isValid?: boolean;
  isRequired?: boolean;
  children: React.ReactNode;
};

/**
 * A form select group component that renders a label, a select input, and an error message.
 * @param label - The label for the select input.
 * @param name - The name of the select input.
 * @param form - The form object that contains the select input's value.
 * @param setForm - The function that updates the form object.
 * @param validated - A boolean that indicates whether the input has been validated.
 * @param invalidMsg - The error message to display if the input is invalid.
 * @param classNames - Additional CSS classes to apply to the form control.
 * @param isValidFunc - An optional function that determines whether the input is valid.
 * @param isValid - An optional boolean that indicates whether the input is valid.
 * @param isRequired - An optional boolean that indicates whether the input is required.
 * @param children - The options to render in the select input.
 * @returns A form select group component.
 */
const FormSelectGroup = ({
  label,
  name,
  form,
  setForm,
  validated,
  invalidMsg,
  classNames,
  isValidFunc,
  isValid,
  isRequired,
  children,
}: FormSelectGroupProps) => {
  const updateForm = (e: { target: { name: any; value: any } }) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const isInputValid = isValidFunc ? isValidFunc(form[name]) : isValid;

  return (
    <FormControl
      className={classNames}
      isInvalid={!isInputValid && validated}
      isRequired={isRequired !== undefined ? isRequired : true}
    >
      <FormLabel>{label}</FormLabel>
      <Select
        name={name}
        placeholder="Please select an option"
        value={form[name]}
        onChange={updateForm}
        isInvalid={!isInputValid && validated}
        isRequired={isRequired !== undefined ? isRequired : true}
      >
        {children}
      </Select>
      <FormErrorMessage>{invalidMsg}</FormErrorMessage>
    </FormControl>
  );
};

export default FormSelectGroup;
