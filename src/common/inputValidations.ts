// This module exports a set of functions that validate user input for various fields in the application.

import {
  descriptionMaxLength,
  nameMaxLength,
  nameMinLength,
  passwordMaxLength,
  passwordMinLength,
  phoneNumberLength,
  titleMaxLength,
  titleMinLength,
  usernameMaxLength,
  usernameMinLength,
} from "./constants";

/**
 * Checks if an email is valid.
 * @param {string} email - The email to validate.
 * @returns {boolean} - True if the email is valid, false otherwise.
 */
export const isValidEmail = (email: string) => {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailRegex.test(email);
};

/**
 * Checks if a password is valid.
 * @param {string} password - The password to validate.
 * @returns {boolean} - True if the password is valid, false otherwise.
 */
export const isValidPassword = (password: string) => {
  return (
    password.length >= passwordMinLength &&
    password.length <= passwordMaxLength &&
    /\d/.test(password) &&
    /[!@#$%^&*]/.test(password)
  );
};

/**
 * Checks if a password re-entered by the user matches the original password.
 * @param {string} password - The original password.
 * @param {string} passwordReEnter - The password re-entered by the user.
 * @returns {boolean} - True if the passwords match and are not empty, false otherwise.
 */
export const isValidPasswordReEnter = (
  password: string,
  passwordReEnter: string
) => {
  return password === passwordReEnter && passwordReEnter !== "";
};

/**
 * Checks if a username is valid.
 * @param {string} username - The username to validate.
 * @returns {boolean} - True if the username is valid, false otherwise.
 */
export const isValidUsername = (username: string) => {
  const usernameRegex = /^[a-zA-Z0-9]+$/;
  if (
    username.length >= usernameMinLength &&
    username.length <= usernameMaxLength &&
    usernameRegex.test(username)
  ) {
    return true;
  } else {
    return false;
  }
};

/**
 * Checks if a name is valid.
 * @param {string} name - The name to validate.
 * @returns {boolean} - True if the name is valid, false otherwise.
 */
export const isValidName = (name: string) => {
  return (
    name.length >= nameMinLength &&
    name.length <= nameMaxLength &&
    /^[a-zA-Z]+$/.test(name)
  );
};

/**
 * Checks if a phone number is valid (optional).
 * @param {string} phone - The phone number to validate.
 * @returns {boolean} - True if the phone number is valid or empty, false otherwise.
 */
export const isValidPhoneOptional = (phone: string) => {
  const phoneRegex = /^[0-9]+$/;
  return phoneRegex.test(phone) || phone === "";
};

/**
 * Checks if a phone number is valid.
 * @param {string} phone - The phone number to validate.
 * @returns {boolean} - True if the phone number is valid or empty, false otherwise.
 */
export const isValidPhone = (phone: string) => {
  const phoneRegex = /^[0-9]+$/;
  return phoneRegex.test(phone) && phone.length === phoneNumberLength;
};

/**
 * Checks if a select field is valid.
 * @param {string} select - The select field to validate.
 * @returns {boolean} - True if the select field is valid, false otherwise.
 */
export const isValidSelect = (select: string) => {
  return !!select && select !== "-1";
};

/**
 * Checks if a title is valid.
 * @param title - The title to validate.
 * @returns - True if the title is valid, false otherwise.
 */
export const isValidTitle = (title: string) => {
  return title.length >= titleMinLength && title.length <= titleMaxLength;
};

/**
 * Checks if a description is valid.
 * @param description - The description to validate.
 * @returns - True if the description is valid, false otherwise.
 */
export const isValidDescription = (description: string) => {
  return description.length <= descriptionMaxLength;
};

/**
 * Checks if a date is valid.
 * @param date - The date to validate.
 * @returns - True if the date is valid, false otherwise.
 */
export const isValidDate = (date: string) => {
  return !isNaN(Date.parse(date));
};

/**
 * Checks if start date is before due date.
 * @param startDate - The start date to validate.
 * @param startHour - The start hour to validate.
 * @param dueDate - The due date to validate.
 * @param dueHour - The due hour to validate.
 * @returns - True if the start date is before the due date, false otherwise.
 */
export const areValidStartDueDates = (
  startDate: string,
  startHour: string,
  dueDate: string,
  dueHour: string
) => {
  const startDateTime = new Date(startDate + " " + startHour);
  const dueDateTime = new Date(dueDate + " " + dueHour);
  return startDateTime < dueDateTime;
};

/**
 * Checks if a recurrence interval is valid.
 * @param interval - The recurrence interval to validate.
 * @returns - True if the recurrence interval is valid, false otherwise.
 */
export const isRecurrenceIntervalValid = (interval: number) => {
  return interval;
};
