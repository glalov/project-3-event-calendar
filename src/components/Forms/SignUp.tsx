import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  addressText,
  emailText,
  firstNameText,
  lastNameText,
  passwordText,
  phoneNumberText,
  reEnterPassText,
  roleText,
  signUpText,
  usernameText,
} from "../../common/formTexts";
import {
  isValidEmail,
  isValidName,
  isValidPassword,
  isValidPasswordReEnter,
  isValidPhone,
  isValidSelect,
  isValidUsername,
} from "../../common/inputValidations";
import {
  emailExistsMsg,
  invalidEmail,
  invalidName,
  invalidPassword,
  invalidPhone,
  invalidUsername,
  phoneExistsMsg,
  reEnterPasswordMismatch,
  signUpSuccessText,
  usernameExistsMsg,
} from "../../common/validationMessages";
import FromButton from "./FormButton";
import FormGroup, { FormGroupProps } from "./FormGroup";
import {
  Flex,
  Box,
  Heading,
  Alert,
  AlertIcon,
  Stack,
  Center,
  HStack,
  Text,
} from "@chakra-ui/react";
import AuthContext from "../../context/AuthContext";
import {
  createUserHandle,
  getUserByHandle,
  getUserByPhone,
} from "../../services/users.service";
import { registerUser, registerUserAsAdmin } from "../../services/auth.service";
import UseSuccessToast from "../../hooks/UseSuccessToast";
import { Link as ReactRouterLink } from "react-router-dom";
import { Link as ChakraLink } from "@chakra-ui/react";
import FormSelectGroup, { FormSelectGroupProps } from "./FormSelectGroup";

export type SignUpProps = {
  isAdmin?: boolean;
};

/**
 * Component for rendering a sign up form with various input fields.
 * @param {boolean} isAdmin - A boolean indicating whether the user is an admin or not.
 * @returns {JSX.Element} - Returns the JSX element for the sign up form.
 */
const SingUp = ({ isAdmin }: SignUpProps) => {
  const [form, setForm] = useState({
    email: "",
    password: "",
    reEnteredPassword: "",
    username: "",
    firstName: "",
    lastName: "",
    phoneNumber: "",
    address: "",
    role: "",
  });
  const [validated, setValidated] = useState(false);
  const [loginError, setLoginError] = useState("");

  const { setUser } = useContext(AuthContext);
  const navigate = useNavigate();

  const toast = UseSuccessToast();

  const onEmailSingUp = (event: any) => {
    event.preventDefault();
    event.stopPropagation();

    setValidated(true);

    if (!areAllValid()) {
      return;
    }

    getUserByHandle(form.username)
      .then((snapshot) => {
        if (snapshot.exists()) {
          throw new Error("usernameExist");
        }

        getUserByPhone(form.phoneNumber)
          .then((snapshot) => {
            if (snapshot.exists()) {
              throw new Error("phoneExist");
            }

            if (!isAdmin) {
              return registerUser(form.email, form.password);
            } else {
              return registerUserAsAdmin(form.email, form.password);
            }
          })
          .then((credential) => {
            return createUserHandle(
              form.username,
              credential.user.uid,
              credential.user.email,
              form.firstName,
              form.lastName,
              form.phoneNumber,
              form.address,
              form.role
            ).then(() => {
              if (!isAdmin) {
                setUser((prevState) => {
                  return {
                    ...prevState,
                    user: credential.user,
                  };
                });
              }
            });
          })
          .then(() => {
            if (!isAdmin) {
              toast(signUpSuccessText, "sign-up-success");
              navigate("/");
            } else {
              //toast("User created successfully", "create-user-success");
              //navigate("/admin");
            }
          })
          .catch((error) => {
            //const errorCode = error.code;
            const errorMessage = error.message;

            if (errorMessage === "phoneExist") {
              setLoginError(phoneExistsMsg);
            } else {
              setLoginError(errorMessage);
            }
          });
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;

        if (errorMessage === "usernameExist") {
          setLoginError(usernameExistsMsg);
        } else if (errorCode === "auth/email-already-in-use") {
          setLoginError(emailExistsMsg);
        } else if (errorCode === "auth/invalid-email") {
          setLoginError(invalidEmail);
        } else {
          setLoginError(errorMessage);
        }
      });
  };

  const emailFormProps: FormGroupProps = {
    label: emailText,
    type: "text",
    name: "email",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: invalidEmail,
    classNames: "email",
    isValidFunc: isValidEmail,
  };

  const passwordFormProps: FormGroupProps = {
    label: passwordText,
    type: "password",
    name: "password",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: invalidPassword,
    classNames: "password",
    isValidFunc: isValidPassword,
  };

  const reEnteredPasswordFormProps: FormGroupProps = {
    label: reEnterPassText,
    type: "password",
    name: "reEnteredPassword",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: reEnterPasswordMismatch,
    classNames: "reEnteredPassword",
    isValid: isValidPasswordReEnter(form.password, form.reEnteredPassword),
  };

  const usernameFormProps: FormGroupProps = {
    label: usernameText,
    type: "text",
    name: "username",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: invalidUsername,
    classNames: "username",
    isValidFunc: isValidUsername,
  };

  const firstNameFormProps: FormGroupProps = {
    label: firstNameText,
    type: "text",
    name: "firstName",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: invalidName,
    classNames: "firstName",
    isValidFunc: isValidName,
  };

  const lastNameFormProps: FormGroupProps = {
    label: lastNameText,
    type: "text",
    name: "lastName",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: invalidName,
    classNames: "lastName",
    isValidFunc: isValidName,
  };

  const telephoneProps: FormGroupProps = {
    label: phoneNumberText,
    type: "text",
    name: "phoneNumber",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: invalidPhone,
    classNames: "phoneNumber",
    isValidFunc: isValidPhone,
  };

  const addressProps: FormGroupProps = {
    label: addressText,
    type: "text",
    name: "address",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: "",
    classNames: "address",
    isValid: true,
    isRequired: false,
  };

  const SelectRoleProps: FormSelectGroupProps = {
    label: "Role",
    name: "role",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: roleText,
    classNames: "role",
    isValidFunc: isValidSelect,
    children: (
      <>
        <option value="Student">Student</option>
        <option value="Teacher">Teacher</option>
      </>
    ),
  };

  const areAllValid = () => {
    return (
      isValidEmail(form.email) &&
      isValidPassword(form.password) &&
      isValidPasswordReEnter(form.password, form.reEnteredPassword) &&
      isValidUsername(form.username) &&
      isValidName(form.firstName) &&
      isValidName(form.lastName) &&
      isValidPhone(form.phoneNumber)
    );
  };

  return (
    <Flex width="full" align="center" justifyContent="center">
      <Box p={2}>
        <Stack align={"center"}>
          <Heading fontSize={"4xl"} textAlign={"center"}>
            Sign up
          </Heading>
          <Text fontSize={"lg"}>to enjoy all of our cool features ✌️</Text>
        </Stack>
        <Box my={4} textAlign="left">
          <Stack spacing={4} p="1rem" boxShadow="md" borderWidth="2px">
            <form className="log-form" onSubmit={onEmailSingUp}>
              {loginError && (
                <Alert status="error">
                  <AlertIcon />
                  {loginError}
                </Alert>
              )}

              <FormGroup {...emailFormProps} />

              <HStack>
                <Box>
                  <FormGroup {...passwordFormProps} />
                </Box>
                <Box>
                  <FormGroup {...reEnteredPasswordFormProps} />
                </Box>
              </HStack>

              <HStack>
                <Box>
                  <FormGroup {...usernameFormProps} />
                </Box>
                <Box>
                  <FormGroup {...telephoneProps} />
                </Box>
              </HStack>

              <HStack>
                <Box>
                  <FormGroup {...firstNameFormProps} />
                </Box>
                <Box>
                  <FormGroup {...lastNameFormProps} />
                </Box>
              </HStack>

              <FormGroup {...addressProps} />

              <FormSelectGroup {...SelectRoleProps} />

              <FromButton
                width="full"
                mt={4}
                type="submit"
                loadingText="Submitting"
                size="lg"
                bg={"blue.400"}
                color={"white"}
                _hover={{
                  bg: "blue.500",
                }}
              >
                {isAdmin ? "Create User" : signUpText}
              </FromButton>
            </form>
          </Stack>
        </Box>

        <Box>
          <Center>
            Already a user?&nbsp;{" "}
            <ChakraLink color="teal.500" as={ReactRouterLink} to="/log-in">
              Log In
            </ChakraLink>
          </Center>
        </Box>
      </Box>
    </Flex>
  );
};

export default SingUp;
