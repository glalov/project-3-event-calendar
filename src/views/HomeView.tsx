import SearchEventsUsers from "../components/ui/SearchEventsUsers";

/**
 * Renders the HomeView component, which displays the SearchEventsUsers component.
 * @returns {JSX.Element} The rendered HomeView component.
 */
const HomeView = () => {
  return <SearchEventsUsers />;
};

export default HomeView;
