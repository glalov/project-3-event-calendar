import {
  Button,
  Center,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightAddon,
  Select,
} from "@chakra-ui/react";
import { Search2Icon } from "@chakra-ui/icons";
import { useState } from "react";

export type SearchBarProps = {
  onSearch: (searchTerm: string, selectVal: string) => void;
  searchPlaceholder: string;
  buttonText: string;
  children?: React.ReactNode;
  selectDefaultValue?: string;
};

/**
 * A search bar component that allows users to enter a search term and select a search category.
 * @param onSearch - A function that is called when the user clicks the search button. It takes two arguments: the search term and the selected category.
 * @param searchPlaceholder - The placeholder text to display in the search input field.
 * @param buttonText - The text to display on the search button.
 * @param children - The options to display in the search category dropdown.
 * @param selectDefaultValue - The default value to display in the search category dropdown.
 */
const SearchBar = ({
  onSearch,
  searchPlaceholder,
  buttonText,
  children,
  selectDefaultValue,
}: SearchBarProps) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [selectVal, setSelectVal] = useState(selectDefaultValue || "");

  return (
    <Center m={5}>
      <InputGroup borderRadius={5} size="sm" height="40px">
        <InputLeftElement
          height="100%"
          pointerEvents="none"
          children={<Search2Icon color="gray.600" />}
        />
        <Input
          type="text"
          height="100%"
          placeholder={searchPlaceholder}
          border="1px solid #949494"
          onChange={(e) => {
            setSearchTerm(e.target.value);
          }}
        />
        {children && (
          <InputRightAddon p={0} border="none" height="100%">
            <Select
              border="1px solid #949494"
              borderLeftRadius={0}
              borderRightRadius={3.3}
              value={selectVal}
              onChange={(e) => {
                setSelectVal(e.target.value);
              }}
            >
              {children}
            </Select>
          </InputRightAddon>
        )}
        <InputRightAddon p={0} border="none" height="100%">
          <Button
            size="sm"
            height="100%"
            borderLeftRadius={0}
            borderRightRadius={3.3}
            border="1px solid #949494"
            onClick={() => onSearch(searchTerm, selectVal)}
          >
            {buttonText}
          </Button>
        </InputRightAddon>
      </InputGroup>
    </Center>
  );
};

export default SearchBar;
