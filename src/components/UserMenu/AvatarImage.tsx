import { Avatar } from "@chakra-ui/react";
import { useContext } from "react";
import AuthContext from "../../context/AuthContext";

export type AvatarImageProps = {
  size?: "sm" | "md" | "lg" | "xl" | "2xl";
};

/**
 * AvatarImage component displays the user's profile picture as an avatar.
 * @param {Object} props - The component props.
 * @param {string} props.size - The size of the avatar.
 * @returns {JSX.Element} - The AvatarImage component.
 */
const AvatarImage = ({ size }: AvatarImageProps): JSX.Element => {
  const { userData } = useContext(AuthContext);
  return (
    <Avatar
      size={size || "sm"}
      name={`${userData?.firstName} ${userData?.lastName}}`}
      src={userData?.profilePicture}
      bg="teal.500"
    />
  );
};

export default AvatarImage;
