export interface User {
  username: string;
  firstName: string;
  lastName: string;
}

export interface RecurringRule {
  enabled: boolean;
  frequency: string;
  interval: number;
  until: string;
}

export interface EventData {
  title: string;
  description: string;
  isPrivate: boolean;
  roomNumber: string;
  startDate: string;
  dueDate: string;
  recurringRule: RecurringRule | null;
  image: string;
  owner: string;
  selectedMembers: Record<string, boolean>;
  selectedFriendLists: Record<string, boolean>;
}
