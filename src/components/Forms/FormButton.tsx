import { Button } from "@chakra-ui/react";

/**
 * A reusable button component for forms.
 * @param props - The props for the button component.
 * @returns A Button component with the given props.
 */
const FromButton = (props: any = {}) => {
  return <Button {...props}>{props.children}</Button>;
};

export default FromButton;
