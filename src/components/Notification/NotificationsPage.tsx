import { useEffect, useState, useContext } from "react";
import NotificationList from "./NotificationList";
import { setupNotificationListener } from "../../services/notification.service";
import AuthContext from "../../context/AuthContext";

const NotificationsPage = () => {
  const [notifications, setNotifications] = useState<any[]>([]);
  const [isNewMap, setIsNewMap] = useState<Record<string, boolean>>({});
  const { userData } = useContext(AuthContext);

  useEffect(() => {
    // Set up the notification listener
    const unsubscribe = setupNotificationListener(
      userData?.username || "",
      (receivedNotifications) => {
        // Update the notifications state when new notifications arrive
        setNotifications(receivedNotifications);
      }
    );

    // Clean up the listener when the component unmounts
    return () => {
      unsubscribe();
    };
  }, [userData]);

  return (
    <div className="calendar-container">
      <NotificationList
        notifications={notifications}
        isNewMap={isNewMap}
        setIsNewMap={setIsNewMap}
      />
    </div>
  );
};

export default NotificationsPage;
