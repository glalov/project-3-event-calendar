import { useState, useEffect, useMemo } from "react";
import "./App.css";
import Calendar, { CalendarMode } from "./components/Calendar/Calendar";
import {
  ChakraProvider,
  ThemeProvider,
  theme,
  ColorModeProvider,
  CSSReset,
} from "@chakra-ui/react";
//import "bootstrap/dist/css/bootstrap.min.css";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from "./config/firebase-config";
import { Role, getUserData } from "./services/users.service";
import AuthContext, { AuthContextDefaultValue } from "./context/AuthContext";
import SingUp from "./components/Forms/SignUp";
import { Routes, Route } from "react-router-dom";
import PublicView from "./views/PublicView";
import HomeView from "./views/HomeView";
import NotFoundView from "./views/NotFoundView";
import LogIn from "./components/Forms/LogIn";
import SidebarWithHeader from "./components/SideBar/SideBar";
import EventView from "./views/EventView";
import {
  fromEventsDocument,
  setEventsListener,
} from "./services/events.service";
import { DataSnapshot } from "firebase/database";
import UnifiedModal from "./hoc/Modal/UnifiedModal";
import { fetchAllFriendLists } from "./services/friends-list.service";
import AdminView from "./views/AdminView";
import ListEvents from "./components/Event/ListEvents";
import NotificationsPage from "./components/Notification/NotificationsPage";
import MyFriendListView from "./views/MyFriendListView";
import EnglishDictionaryView from "./views/EnglishDictionaryView";

/**
 * The main component of the application.
 * It handles the authentication state, sets up the calendar data, and renders the sidebar and the main content.
 */
function App() {
  const [user] = useAuthState(auth);
  const [appState, setAppState] = useState(AuthContextDefaultValue);

  const [mode, setMode] = useState(CalendarMode.Month);
  const [currentDate, setCurrentDate] = useState(new Date());
  const todayDate = useMemo(() => new Date(), []);

  /**
   * An object containing the current calendar mode, the current date, and today's date.
   */
  const calendarData = {
    mode,
    setMode,
    currentDate,
    setCurrentDate,
    todayDate,
  };

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalContent, setModalContent] = useState<React.ReactNode>(null);
  const [modalTitle, setModalTitle] = useState("");

  /**
   * Opens the modal with the specified content and title.
   * @param content The content to be displayed in the modal.
   * @param title The title of the modal.
   */
  const openModal = (content: React.ReactNode, title = "") => {
    setModalContent(content);
    setIsModalOpen(true);
    setModalTitle(title);
  };

  /**
   * Closes the modal.
   */
  const closeModal = () => {
    setIsModalOpen(false);
    setModalContent(null);
  };

  if (appState.user !== user) {
    setAppState((prevState) => {
      return {
        ...prevState,
        user,
      };
    });
  }

  /**
   * Updates the app state with the user data and events.
   * @param allEventsSnapshot The snapshot of all events.
   * @param friendLists The friend lists of the user.
   * @param userData The user data.
   */
  const getAllEventsByUser = (
    allEventsSnapshot: DataSnapshot,
    friendLists: Array<string>,
    userData: any
  ) => {
    const events = fromEventsDocument(allEventsSnapshot).filter((event) => {
      return (
        event.owner === userData.username ||
        (event.selectedMembers &&
          Object.keys(event.selectedMembers).includes(userData.username)) ||
        (event.selectedFriendLists &&
          friendLists.some(
            (friendListId) => event.selectedFriendLists[friendListId]
          ))
      );
    });

    setAppState((prevState) => {
      return {
        ...prevState,
        userData: {
          ...userData,
          events: events,
        },
      };
    });
  };

  useEffect(() => {
    if (user === null || user === undefined) {
      return;
    }

    getUserData(user.uid).then((snapshot) => {
      if (!snapshot.exists()) {
        throw new Error("User data not found");
      }

      const userData = snapshot.val()[Object.keys(snapshot.val())[0]];

      fetchAllFriendLists(userData.username).then((friendLists) => {
        setEventsListener((allEventsSnapshot) => {
          getAllEventsByUser(allEventsSnapshot, friendLists, userData);
        });
      });
    });
  }, [user]);

  return (
    <div className="container">
      <AuthContext.Provider
        value={{
          ...appState,
          setUser: setAppState,
          openModal,
          closeModal,
          calendarData,
        }}
      >
        <ChakraProvider>
          <ThemeProvider theme={theme}>
            <ColorModeProvider>
              <CSSReset />
              <SidebarWithHeader>
                <Routes>
                  <Route path="/event/:eventId" element={<EventView />} />
                  {user === null && <Route path="/" element={<PublicView />} />}
                  {user !== null &&
                    appState.userData?.role !== Role.Principal && (
                      <Route path="/" element={<HomeView />} />
                    )}
                  {user !== null &&
                    appState.userData?.role === Role.Principal && (
                      <Route path="/" element={<AdminView />} />
                    )}
                  {user !== null && (
                    <Route path="/calendar" element={<Calendar />} />
                  )}
                  {user !== null && (
                    <Route
                      path="/notifications"
                      element={<NotificationsPage />}
                    />
                  )}
                  {user == null && (
                    <Route path="/sign-up" element={<SingUp />} />
                  )}
                  {user == null && <Route path="/log-in" element={<LogIn />} />}
                  {user !== null && (
                    <Route path="/myevent" element={<ListEvents />} />
                  )}
                  {user !== null && (
                    <Route
                      path="/myfriendlists"
                      element={<MyFriendListView />}
                    />
                  )}
                  {user !== null && (
                    <Route
                      path="/english-dictionary"
                      element={<EnglishDictionaryView />}
                    />
                  )}

                  <Route path="*" element={<NotFoundView />} />
                </Routes>
                <UnifiedModal
                  isOpen={isModalOpen}
                  onClose={closeModal}
                  content={modalContent}
                  actionButtons={null}
                  title={modalTitle}
                />
              </SidebarWithHeader>
            </ColorModeProvider>
          </ThemeProvider>
        </ChakraProvider>
      </AuthContext.Provider>
    </div>
  );
}

export default App;
