import { format } from "date-fns";
import { Tooltip } from "@chakra-ui/react";

export type TooltipHoverProps = {
  children: React.ReactNode;
  event: {
    id: string;
    title: string;
    startDate: string;
    dueDate: string;
    owner: string;
  };
};

/**
 * TooltipHover component displays a tooltip with event details when hovered over.
 * @param children - The child components to be wrapped by the Tooltip component.
 * @param event - The event object containing the event details to be displayed in the tooltip.
 * @returns A Tooltip component with the event details as the label.
 */
const TooltipHover = ({ children, event }: TooltipHoverProps) => {
  return (
    <Tooltip
      key={event.id}
      label={
        <>
          <div>
            {`${format(new Date(event.startDate), "H:mm")}-${format(
              new Date(event.dueDate),
              "H:mm"
            )}`}
          </div>
          <div>{`${event.title}`}</div>
          <div>{`Organized by: ${event.owner}`}</div>
        </>
      }
      placement="top"
    >
      {children}
    </Tooltip>
  );
};

export default TooltipHover;
