import { useCallback, useContext, useMemo } from "react";
import {
  DateDurationType,
  areIndexRangesOverlapping,
  convertHourToIndex,
  convertHourToIndexExcluding,
  getAllHoursInDay,
  getDateDurationType,
  getEventString,
} from "../../common/utils";
import { Center, Grid, GridItem } from "@chakra-ui/layout";
import { Highlight } from "@chakra-ui/react";
import { addDays, addMonths, addYears, format, parseISO } from "date-fns";
import AuthContext from "../../context/AuthContext";
import { EventsPerMonth } from "../../services/events.service";
import { useNavigate } from "react-router-dom";
import CreateEventView from "../../views/CreateEventView";
import { formatTimeShort } from "../../common/timeFormat";
import TooltipHover from "./TooltipHover";

export type CurrentDayEvents = EventsPerMonth & {
  startHourIndex: number;
  dueHourIndex: number;
  durationType: DateDurationType;
  rowSpan: number;
  inEventCount: number;
};

/**
 * Renders the DayMode component, which displays the events for a single day in a grid format.
 * @returns {JSX.Element} - The rendered DayMode component.
 */
const DayMode = ({
  todayDate,
  currentDate,
  locale,
  weekStartsOn,
  showHoursLegend = true,
  showShortNames = false,
  onDateClick,
}: {
  todayDate: Date;
  currentDate: Date;
  locale: Locale;
  weekStartsOn: 0 | 1 | 2 | 3 | 4 | 5 | 6 | undefined;
  showHoursLegend?: boolean;
  showShortNames?: boolean;
  onDateClick?: (date: Date) => void;
}) => {
  const { userData, openModal } = useContext(AuthContext);
  const navigate = useNavigate();

  const allHours = getAllHoursInDay().map((hour) => {
    const hourInfo = {
      hour,
      events: [] as {
        id: string;
        startDate: string;
        dueDate: string;
        durationType: DateDurationType;
        title: string;
        rowSpan: number;
        inEventCount: number;
        owner: string;
      }[],
      isInEvent: false,
      inRowEventCount: 0,
      inGridEventCount: 0,
    };
    return hourInfo;
  });

  const [currentDayEvents, fullDayEvents] = useMemo(() => {
    return userData?.events
      ? userData.events.reduce(
          ([currentDayEvents, fullDayEvents], event) => {
            const startDate = new Date(event.startDate);
            const dueDate = new Date(event.dueDate);
            let durationType = getDateDurationType(
              currentDate,
              startDate,
              dueDate
            );

            if (event.recurringRule?.enabled) {
              const interval = event.recurringRule.interval;
              const untilDate = new Date(event.recurringRule.until);

              let nextStartDate = startDate;
              let nextDueDate = dueDate;
              while (
                nextStartDate <= currentDate &&
                nextStartDate <= untilDate
              ) {
                switch (event.recurringRule.frequency) {
                  case "days":
                    nextStartDate = addDays(nextStartDate, interval);
                    nextDueDate = addDays(nextDueDate, interval);
                    break;
                  case "weeks":
                    nextStartDate = addDays(nextStartDate, interval * 7);
                    nextDueDate = addDays(nextDueDate, interval * 7);
                    break;
                  case "months":
                    nextStartDate = addMonths(nextStartDate, interval);
                    nextDueDate = addMonths(nextDueDate, interval);
                    break;
                  case "years":
                    nextStartDate = addYears(nextStartDate, interval);
                    nextDueDate = addYears(nextDueDate, interval);
                    break;
                  default:
                    break;
                }

                const nextDurationType = getDateDurationType(
                  currentDate,
                  nextStartDate,
                  nextDueDate
                );

                if (nextDurationType !== DateDurationType.UNSET) {
                  durationType = nextDurationType;
                  break;
                }
              }
            }

            if (durationType === DateDurationType.FULLDAY) {
              fullDayEvents.push(event);
            } else if (durationType !== DateDurationType.UNSET) {
              let startHourIndex = 0;
              let dueHourIndex = allHours.length - 1;

              if (durationType !== DateDurationType.ENDING) {
                startHourIndex = convertHourToIndex(parseISO(event.startDate));
              }
              if (durationType !== DateDurationType.BEGINNING) {
                dueHourIndex = convertHourToIndexExcluding(
                  parseISO(event.dueDate)
                );
              }

              currentDayEvents.push({
                ...event,
                startHourIndex,
                dueHourIndex,
                durationType,
                rowSpan: dueHourIndex - startHourIndex + 1,
                inEventCount: 1,
              });
            }
            return [currentDayEvents, fullDayEvents];
          },
          [[], []] as [CurrentDayEvents[], EventsPerMonth[]]
        )
      : [[], []];
  }, [userData, currentDate, allHours]);

  for (let i = 0; i < currentDayEvents.length - 1; i++) {
    for (let j = i + 1; j < currentDayEvents.length; j++) {
      const firstStartIndex = currentDayEvents[i].startHourIndex;
      const firstEndIndex = currentDayEvents[i].dueHourIndex;
      const secondStartIndex = currentDayEvents[j].startHourIndex;
      const secondEndIndex = currentDayEvents[j].dueHourIndex;
      if (
        areIndexRangesOverlapping(
          firstStartIndex,
          firstEndIndex,
          secondStartIndex,
          secondEndIndex
        )
      ) {
        currentDayEvents[i].inEventCount++;
        currentDayEvents[j].inEventCount++;
      }
    }
  }

  let maxColSpan = 0;

  currentDayEvents.forEach((event) => {
    for (let i = event.startHourIndex; i <= event.dueHourIndex; i++) {
      allHours[i].isInEvent = true;
      allHours[i].inRowEventCount++;
      allHours[i].inGridEventCount = event.inEventCount;

      if (allHours[i].inRowEventCount > maxColSpan) {
        maxColSpan = allHours[i].inRowEventCount;
      }
    }
    allHours[event.startHourIndex].events.push({
      startDate: event.startDate,
      dueDate: event.dueDate,
      durationType: event.durationType,
      id: event.id,
      title: event.title,
      rowSpan: event.rowSpan,
      inEventCount: event.inEventCount,
      owner: event.owner,
    });
  });

  if (fullDayEvents.length > maxColSpan) {
    maxColSpan = fullDayEvents.length;
  }

  // If no events still show empty grid
  if (maxColSpan === 0) {
    maxColSpan = 1;
  }

  //console.log(maxColSpan);

  const currentDayText = useMemo(
    () =>
      `${currentDate.getDate()} - ${format(currentDate, "eeee", {
        locale,
        weekStartsOn,
      })}`,
    [currentDate, locale, weekStartsOn]
  );

  const onEventClick = (eventId: string | undefined) => {
    navigate("/event/" + eventId);
  };

  const onEmptyClick = (hour: Date) => {
    const passedDate = new Date(currentDate);
    passedDate.setHours(hour.getHours(), hour.getMinutes());
    openModal(
      <CreateEventView
        ownerUsername={userData?.username}
        startDate={passedDate}
      />
    );
  };

  const onDayClick = useCallback(
    (day: Date) => {
      if (showShortNames && onDateClick) {
        onDateClick(day);
      }
    },
    [showShortNames, onDateClick]
  );

  const gridItemHeight = 30;
  const minHeight = "30px";
  const shortFontSize = "10px";
  const setGridItemHeight = useCallback(
    (height = gridItemHeight) => {
      if (showShortNames) return height.toString() + "px";
      return "auto";
    },
    [showShortNames]
  );

  return (
    <Grid
      templateColumns={`${
        showHoursLegend ? "40px" : ""
      } repeat(${maxColSpan}, 1fr)`}
    >
      {showHoursLegend && (
        <GridItem border="1px" borderColor="lightgrey">
          &nbsp;
        </GridItem>
      )}

      <GridItem p={2} colSpan={maxColSpan} border="1px" borderColor="lightgrey">
        <Center
          className={showShortNames ? "day-link" : ""}
          onClick={() => onDayClick(currentDate)}
        >
          {todayDate.toDateString() === currentDate.toDateString() && (
            <Highlight
              query={currentDayText}
              children={currentDayText}
              styles={{ px: "2", py: "1", rounded: "full", bg: "#7dd5ba" }}
            />
          )}
          {todayDate.toDateString() !== currentDate.toDateString() && (
            <Highlight
              query={currentDayText}
              children={currentDayText}
              styles={{
                px: "2",
                py: "1",
                rounded: "full",
                bg: "transparent",
              }}
            />
          )}
        </Center>
      </GridItem>

      {showHoursLegend && (
        <GridItem
          border="1px"
          borderColor="lightgrey"
          height={setGridItemHeight()}
          minHeight={minHeight}
          display="flex"
          justifyContent="center"
        >
          <Center fontSize={shortFontSize}>all-day</Center>
        </GridItem>
      )}
      {fullDayEvents.map((event, index) => (
        <GridItem
          height={setGridItemHeight()}
          minHeight={minHeight}
          key={"full-day-event-" + index.toString()}
          border="1px"
          borderColor="#bce6f7"
          bg="var(--button_color)"
          _hover={{
            bg: "var(--button_hover)",
          }}
          colSpan={maxColSpan / fullDayEvents.length}
          className="event-item"
          onClick={() => onEventClick(event.id)}
          overflow="hidden"
        >
          <TooltipHover event={event}>
            <Center fontSize={`${showShortNames ? shortFontSize : "auto"}`}>
              {showShortNames
                ? event.title
                : getEventString(
                    event.title,
                    event.startDate,
                    event.dueDate,
                    DateDurationType.FULLDAY
                  )}
            </Center>
          </TooltipHover>
        </GridItem>
      ))}
      {fullDayEvents.length === 0 && (
        <GridItem
          height={setGridItemHeight()}
          minHeight={minHeight}
          border="1px"
          borderColor="lightgrey"
          colSpan={maxColSpan}
          className="empty-spot-item"
        ></GridItem>
      )}
      {allHours.map((hourInfo, index) => (
        <>
          {showHoursLegend && (
            <GridItem
              height={setGridItemHeight()}
              minHeight={minHeight}
              key={hourInfo.hour.toDateString()}
              border="1px"
              borderColor="lightgrey"
              display="flex"
              justifyContent="center"
            >
              <Center fontSize={shortFontSize}>
                &nbsp;
                {index % 2 === 0 && formatTimeShort(hourInfo.hour.toString())}
                &nbsp;
              </Center>
            </GridItem>
          )}
          {hourInfo.isInEvent &&
            hourInfo.events.map((event, index) => (
              <GridItem
                height={setGridItemHeight(gridItemHeight * event.rowSpan)}
                minHeight={minHeight}
                key={
                  hourInfo.hour.toDateString() + "-event-" + index.toString()
                }
                border="1px"
                borderColor="#bce6f7"
                bg="var(--button_color)"
                _hover={{
                  bg: "var(--button_hover)",
                }}
                rowSpan={event.rowSpan}
                colSpan={event.inEventCount === 1 ? maxColSpan : 1}
                className="event-item"
                onClick={() => onEventClick(event.id)}
                overflow="hidden"
                display="flex"
                justifyContent="center"
              >
                <TooltipHover event={event}>
                  <Center
                    fontSize={`${showShortNames ? shortFontSize : "auto"}`}
                  >
                    {showShortNames
                      ? event.title
                      : getEventString(
                          event.title,
                          event.startDate,
                          event.dueDate,
                          event.durationType
                        )}
                  </Center>
                </TooltipHover>
              </GridItem>
            ))}
          {hourInfo.isInEvent &&
            hourInfo.inRowEventCount > 0 &&
            hourInfo.inGridEventCount != 1 &&
            hourInfo.inRowEventCount < maxColSpan && (
              <GridItem
                height={setGridItemHeight()}
                minHeight={minHeight}
                key={
                  hourInfo.hour.toDateString() +
                  "-empty-" +
                  hourInfo.inRowEventCount.toString()
                }
                border="1px"
                borderColor="lightgrey"
                colSpan={maxColSpan - hourInfo.inRowEventCount}
                className="empty-spot-item"
                onClick={() => onEmptyClick(hourInfo.hour)}
              ></GridItem>
            )}
          {!hourInfo.isInEvent && (
            <GridItem
              height={setGridItemHeight()}
              minHeight={minHeight}
              key={hourInfo.hour.toDateString() + "-empty"}
              border="1px"
              borderColor="lightgrey"
              colSpan={maxColSpan}
              className="empty-spot-item"
              onClick={() => onEmptyClick(hourInfo.hour)}
            ></GridItem>
          )}
        </>
      ))}
    </Grid>
  );
};

export default DayMode;
