import DictionarySearch from "../components/API/DictionarySearch";

/**
 * Renders the English Dictionary view, which contains a search bar for looking up English words.
 * @returns JSX element that displays the English Dictionary view.
 */
const EnglishDictionaryView = () => {
  return (
    <div className="calendar-container" style={{ minHeight: "0px" }}>
      <DictionarySearch></DictionarySearch>
    </div>
  );
};

export default EnglishDictionaryView;
