import { useState } from "react";
import axios from "axios";
import {
  Box,
  Input,
  Button,
  Text,
  VStack,
  HStack,
  Heading,
} from "@chakra-ui/react";
import { englishDictURL } from "../../common/constants";

interface Definition {
  word: string;
  phonetics: { text: string }[];
  meanings: {
    partOfSpeech: string;
    definitions: {
      definition: string;
      example?: string;
      synonyms?: string[];
      antonyms?: string[];
    }[];
  }[];
}

/**
 * A component that allows the user to search for the definition of a word using the English Dictionary API.
 * Displays the word's definition, part of speech, example, synonyms, and antonyms.
 * Uses Chakra UI for styling.
 */
const DictionarySearch = () => {
  const [word, setWord] = useState("");
  const [definitions, setDefinitions] = useState<Definition[]>([]);

  const fetchWordDefinitions = async () => {
    try {
      const response = await axios.get(`${englishDictURL}${word}`);
      setDefinitions(response.data);
    } catch (error) {
      console.error("Error fetching word definitions:", error);
    }
  };

  return (
    <VStack spacing={4}>
      <Heading as="h1" size="xl">
        Dictionary Search
      </Heading>
      <HStack>
        <Input
          placeholder="Enter a word"
          value={word}
          onChange={(e) => setWord(e.target.value)}
        />
        <Button colorScheme="teal" onClick={fetchWordDefinitions}>
          Search
        </Button>
      </HStack>
      {definitions.length > 0 && (
        <Box>
          {definitions.map((entry, entryIndex) => (
            <Box
              key={entryIndex}
              borderWidth="1px"
              p={4}
              m={2}
              borderRadius="md"
            >
              <Text fontSize="2xl" fontWeight="bold">
                {entry.word}
              </Text>

              {entry.phonetics && entry.phonetics.length > 0 && (
                <Text fontSize="lg">Phonetic: {entry.phonetics[0].text}</Text>
              )}
              {entry.meanings.map((meaning, meaningIndex) => (
                <Box key={meaningIndex} mt={4}>
                  <Text fontSize="xl" fontStyle="italic" color="green">
                    {meaning.partOfSpeech}
                  </Text>
                  {meaning.definitions.map((definition, definitionIndex) => (
                    <Box key={definitionIndex} mt={2}>
                      <Text fontSize="lg" padding={"10px 0px"}>
                        Definition: {definition.definition}
                      </Text>
                      {definition.example && (
                        <Text padding={"5px"} textColor={"green"}>
                          <strong>Example:</strong> {definition.example}
                        </Text>
                      )}
                      {definition.synonyms &&
                        definition.synonyms.length > 0 && (
                          <Text>
                            <strong>Synonyms:</strong>{" "}
                            {definition.synonyms.join(", ")}
                          </Text>
                        )}
                      {definition.antonyms &&
                        definition.antonyms.length > 0 && (
                          <Text>
                            <strong>Antonyms:</strong>{" "}
                            {definition.antonyms.join(", ")}
                          </Text>
                        )}
                    </Box>
                  ))}
                </Box>
              ))}
            </Box>
          ))}
        </Box>
      )}
    </VStack>
  );
};

export default DictionarySearch;
