import React, { useState, useEffect } from "react";
import { Box, Heading, Input, Button } from "@chakra-ui/react";
import { updateUserWithFriendList } from "../services/users.service";
import { createFriendListInDatabase } from "../services/friends-list.service";
import UseSuccessToast from "../hooks/UseSuccessToast";
import { friendListSuccess } from "../common/validationMessages";
import MembersList from "../components/Event/MembersList";
import { MembersTitleFriendList } from "../common/constants";
import { User } from "../common/interfaces";

interface CreateFriendListViewProps {
  ownerUsername: string;
}

/**
 * This component allows the user to create a new friend list.
 *
 * @param {CreateFriendListViewProps} ownerUsername - The username of the owner of the friend list.
 * @returns {JSX.Element} - A JSX element representing the create friend list view.
 */
const CreateFriendListView: React.FC<CreateFriendListViewProps> = ({
  ownerUsername,
}) => {
  const [newMemberName, setNewMemberName] = useState("");
  const [selectedMembers, setSelectedMembers] = useState<User[]>([]);
  const [membersInList, setMembersInList] = useState<User[]>([]);
  const [searchResults, setSearchResults] = useState<User[]>([]);
  const [ListName, setListName] = useState("");
  const toast = UseSuccessToast();

  const handleCreateList = async () => {
    if (!ListName) {
      console.log("Please provide a name for the list.");
      return;
    }

    try {
      const listData: { [username: string]: boolean } = {};
      membersInList.forEach((member) => {
        listData[member.username] = true;
      });

      const newListId = await createFriendListInDatabase(
        ListName,
        listData,
        ownerUsername
      );

      // console.log("Friend List created:", newListId);

      await updateUserWithFriendList(ownerUsername, newListId || "");

      setListName("");
      setNewMemberName("");
      setSelectedMembers([]);
      setMembersInList([]);
      setSearchResults([]);

      toast(friendListSuccess, "friend-list-success");
    } catch (error) {
      console.error("Error creating Friend List:", error);
    }
  };

  useEffect(() => {
    if (!newMemberName) {
      setSearchResults([]);
    }
  }, [newMemberName]);

  return (
    <Box p={4}>
      <Heading as="h1" mb={4}>
        Create Friend List
      </Heading>
      <Heading as="h3" size="md" mb={2}>
        List name
      </Heading>
      <Input
        value={ListName}
        onChange={(event) => setListName(event.target.value)}
        placeholder="Enter the name of the list"
        mb={4}
      />

      <MembersList
        selectedMembers={selectedMembers}
        setSelectedMembers={setSelectedMembers}
        membersInList={membersInList}
        setMembersInList={setMembersInList}
        searchResults={searchResults}
        setSearchResults={setSearchResults}
        newMemberName={newMemberName}
        setNewMemberName={setNewMemberName}
        ownerUsername={ownerUsername}
        invitedMembersTitle={MembersTitleFriendList}
      />

      <Button onClick={handleCreateList} my={3} colorScheme="green">
        Create
      </Button>
    </Box>
  );
};

export default CreateFriendListView;
