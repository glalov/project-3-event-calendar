import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Grid,
  GridItem,
  Select,
  SimpleGrid,
} from "@chakra-ui/react";
import {
  Role,
  UserData,
  blockUser,
  setRole,
  unblockUser,
} from "../../services/users.service";
import UseSuccessToast from "../../hooks/UseSuccessToast";
import { useContext, useState } from "react";
import AuthContext from "../../context/AuthContext";

type UserListProps = {
  users: UserData[];
};

/**
 * Renders a list of users with their information and allows a principal user to block/unblock users and change their roles.
 * @param users An array of user data objects.
 */
const UserList = ({ users }: UserListProps) => {
  const { userData } = useContext(AuthContext);
  const [userList, setUserList] = useState<UserData[]>(users);
  const toast = UseSuccessToast();
  const onToggleBlock = (username: string, isBlocked: boolean) => {
    if (isBlocked) {
      unblockUser(username).then(() => {
        toast("User was unblocked", "unblock-user-" + username);
        setUserList((prevUsers) =>
          prevUsers.map((user) => {
            if (user.username === username)
              return { ...user, isBlocked: false };
            return user;
          })
        );
      });
    } else {
      blockUser(username).then(() => {
        toast("User was blocked", "block-user-" + username);
        setUserList((prevUsers) =>
          prevUsers.map((user) => {
            if (user.username === username) return { ...user, isBlocked: true };
            return user;
          })
        );
      });
    }
  };

  const onRoleChange = (username: string, role: Role) => {
    setRole(username, role).then(() => {
      toast("User role was updated", "role-user-" + username);
      setUserList((prevUsers) =>
        prevUsers.map((user) => {
          if (user.username === username) return { ...user, role: role };
          return user;
        })
      );
    });
  };

  return (
    <SimpleGrid
      spacing={4}
      templateColumns="repeat(auto-fill, minmax(300px, 1fr))"
    >
      {userList.map((user, index) => (
        <Card key={index}>
          <CardHeader>{user.username}</CardHeader>
          <CardBody>
            <Grid templateColumns="1fr 1fr">
              <GridItem>First name:</GridItem>
              <GridItem>{user.firstName}</GridItem>
              <GridItem>Last name:</GridItem>
              <GridItem>{user.lastName}</GridItem>
              <GridItem>Email:</GridItem>
              <GridItem>{user.email}</GridItem>
              {user.phoneNumber && <GridItem>Phone number:</GridItem>}
              {user.phoneNumber && <GridItem>{user.phoneNumber}</GridItem>}
              <GridItem>Role:</GridItem>
              {userData?.role === Role.Principal && (
                <GridItem>
                  <Select
                    value={user.role}
                    onChange={(event) =>
                      onRoleChange(user.username, event.target.value as Role)
                    }
                  >
                    <option value="Student">Student</option>
                    <option value="Teacher">Teacher</option>
                    <option value="Principal">Principal</option>
                  </Select>
                </GridItem>
              )}
              {userData?.role !== Role.Principal && (
                <GridItem>{user.role}</GridItem>
              )}
            </Grid>
          </CardBody>
          <CardFooter>
            {userData?.role === Role.Principal && (
              <Button
                onClick={() => onToggleBlock(user.username, user.isBlocked)}
              >
                {user.isBlocked ? "Unblock" : "Block"}
              </Button>
            )}
          </CardFooter>
        </Card>
      ))}
    </SimpleGrid>
  );
};

export default UserList;
