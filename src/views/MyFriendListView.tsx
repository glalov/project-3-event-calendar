import { useContext, useState, useEffect } from "react";
import AuthContext from "../context/AuthContext";
import {
  Box,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  List,
  ListItem,
  ListIcon,
  Badge,
  Center,
  Text,
  Button, // Add Spinner for loading state
} from "@chakra-ui/react";
import {
  deleteFriendList,
  fetchFriendLists,
} from "../services/friends-list.service";
import {
  getUserByHandle,
  removeFriendListFromUser,
} from "../services/users.service";
import { MdCheckCircle, MdEdit, MdDelete } from "react-icons/md"; // Import the MdEdit icon
import EditFriendList from "./EditFriendList";

const MyFriendListView = () => {
  const [myFriendLists, setMyFriendLists] = useState<any[]>([]);
  const [usernames, setUsernames] = useState<any>({});
  const { userData, openModal, closeModal } = useContext(AuthContext);

  useEffect(() => {
    if (userData?.username) {
      fetchFriendLists(userData?.username)
        .then((lists) => {
          setMyFriendLists(lists);
          // Fetch usernames for each member
          const promises = lists.map(async (list) => {
            const memberUsernames = await Promise.all(
              Object.keys(list.members).map(async (member) => {
                const user = await getUserByHandle(member)
                  .then((snapshot) => snapshot.val())
                  .catch((e) => console.log(e));
                const username = `${user.firstName} ${user.lastName}`;
                return { member, username };
              })
            );
            return { list, memberUsernames };
          });

          Promise.all(promises).then((results) => {
            const usernameMap: { [key: string]: string } = {};
            results.forEach((result) => {
              result.memberUsernames.forEach((memberUsername) => {
                usernameMap[memberUsername.member] = memberUsername.username;
              });
            });
            setUsernames(usernameMap);
          });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [myFriendLists]);

  const handleEdit = (event: any, listId: string) => {
    event.stopPropagation();
    openModal(
      <EditFriendList
        ownerUsername={userData?.username || ""}
        listId={listId}
      />
    );
  };

  const handleDelete = (listId: string) => {
    closeModal();
    deleteFriendList(listId)
      .then(() => {
        // Once the friend list is deleted, remove it from the user's data
        removeFriendListFromUser(userData?.username || "", listId);
        // Remove the friend list from the local state
        setMyFriendLists((prevLists) =>
          prevLists.filter((list) => list.id !== listId)
        );
      })
      .catch((error) => {
        console.error("Error deleting friend list:", error);
      });
  };

  const handleDeleteFriendList = (event: any, listId: string) => {
    event.stopPropagation();
    openModal(
      <Box>
        <Center>
          <Text as="b">Are you sure you want to delete this friend list?</Text>
        </Center>
        <Center>
          <Button onClick={() => handleDelete(listId)} m={6} colorScheme="red">
            Delete
          </Button>
          <Button onClick={closeModal} m={6} colorScheme="blue">
            Cancel
          </Button>
        </Center>
      </Box>
    );
  };

  return (
    <div className="calendar-container">
      <Box>
        <Accordion allowToggle>
          {myFriendLists.map((list) => (
            <AccordionItem key={list.id}>
              <h2>
                <AccordionButton>
                  <Box
                    flex="1"
                    textAlign="left"
                    fontWeight="bold"
                    fontSize="xl"
                  >
                    {list.name}
                  </Box>
                  <Box marginRight="30px">
                    <MdEdit
                      onClick={(event: any) => handleEdit(event, list.id)}
                      style={{ cursor: "pointer" }}
                    />
                  </Box>
                  <Box marginRight="30px">
                    <MdDelete
                      onClick={(event: any) =>
                        handleDeleteFriendList(event, list.id)
                      }
                      style={{ cursor: "pointer" }}
                    />
                  </Box>
                  <AccordionIcon />
                </AccordionButton>
              </h2>
              <AccordionPanel>
                <h3 style={{ margin: "5px 0 15px" }}>Invited users:</h3>
                <List spacing={3}>
                  {list.members &&
                    Object.keys(list.members)
                      .sort((a, b) => {
                        return a.localeCompare(b);
                      })
                      .map((member: any) => (
                        <ListItem key={member}>
                          <ListIcon as={MdCheckCircle} color="green.500" />
                          {usernames[member]}
                          <Badge
                            m={2}
                            key={member}
                            variant="subtle"
                            colorScheme="green"
                          >
                            {member}
                          </Badge>
                        </ListItem>
                      ))}
                </List>
              </AccordionPanel>
            </AccordionItem>
          ))}
        </Accordion>
      </Box>
    </div>
  );
};

export default MyFriendListView;
