import { Box, IconButton } from "@chakra-ui/react";
import { css } from "@emotion/react";
import { FaBell } from "react-icons/fa";

/**
 * Renders a notification bell icon with an optional count badge.
 * @param count - The number of notifications to display in the count badge.
 * @returns A React component that displays a notification bell icon with an optional count badge.
 */
const NotificationBell = ({ count }: { count: number }) => {
  return (
    <IconButton
      css={css`
        position: relative !important;
        // background: none;
      `}
      py={"2"}
      colorScheme={"gray"}
      aria-label={"Notifications"}
      //   size={"lg"}
      icon={
        <>
          <FaBell color={"gray.750"} />
          {count > 0 && (
            <Box
              as={"span"}
              color={"white"}
              position={"absolute"}
              top={"6px"}
              right={"4px"}
              fontSize={"0.8rem"}
              bgColor={"red"}
              borderRadius={"lg"}
              zIndex={9999}
              p={"1px"}
            >
              {count}
            </Box>
          )}
        </>
      }
    />
  );
};

export default NotificationBell;
