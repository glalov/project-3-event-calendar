import SearchEventsUsers from "../components/ui/SearchEventsUsers";

/**
 * Renders the AdminView component which displays the search bar for events and users.
 * @returns {JSX.Element} The rendered AdminView component.
 */
const AdminView = () => {
  return <SearchEventsUsers />;
};

export default AdminView;
