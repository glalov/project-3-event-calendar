import React, { useState, useEffect, useContext } from "react";
import { Box, Heading, Input, Button } from "@chakra-ui/react";
import { getUserByHandle } from "../services/users.service";
import {
  fetchFriendListData,
  updateFriendList,
} from "../services/friends-list.service";
import UseSuccessToast from "../hooks/UseSuccessToast";
import { friendListEditSuccess } from "../common/validationMessages";
import MembersList from "../components/Event/MembersList";
import { MembersTitleFriendList } from "../common/constants";
import { User } from "../common/interfaces";
import AuthContext from "../context/AuthContext";

interface EditFriendListViewProps {
  ownerUsername: string;
  listId: string;
}

/**
 * EditFriendList component allows the user to edit a friend list.
 * @param {EditFriendListViewProps} ownerUsername - The username of the owner of the friend list.
 * @param {EditFriendListViewProps} listId - The ID of the friend list to be edited.
 * @returns {JSX.Element} - A JSX element representing the EditFriendList component.
 */
const EditFriendList: React.FC<EditFriendListViewProps> = ({
  ownerUsername,
  listId,
}) => {
  const [newMemberName, setNewMemberName] = useState("");
  const [selectedMembers, setSelectedMembers] = useState<User[]>([]);
  const [membersInList, setMembersInList] = useState<User[]>([]);
  const [searchResults, setSearchResults] = useState<User[]>([]);
  const [ListName, setListName] = useState("");
  const toast = UseSuccessToast();
  const { closeModal } = useContext(AuthContext);

  useEffect(() => {
    if (listId) {
      fetchFriendListData(listId)
        .then((data) => {
          setListName(data.name);
          return Object.keys(data.members);
        })
        .then((handles) => {
          return Promise.all(
            handles.map(async (handle) => {
              const userData = await getUserByHandle(handle);
              return userData;
            })
          );
        })
        .then((snapshots) => snapshots.map((snapshot) => snapshot.val()))
        .then((users) => {
          return users.map((user) => {
            return {
              username: user.username,
              firstName: user.firstName,
              lastName: user.lastName,
            };
          });
        })
        .then((data) => setMembersInList(data))
        .catch((error) => {
          console.error("Error fetching friend list data:", error);
        });
    }
  }, [listId]);

  const handleSaveChangesList = async () => {
    if (!ListName) {
      console.log("Please provide a name for the list.");
      return;
    }

    try {
      const listData: { [username: string]: boolean } = {};
      membersInList.forEach((member) => {
        listData[member.username] = true;
      });

      await updateFriendList(listId, ListName, listData);

      toast(friendListEditSuccess, "friend-list-success");
      closeModal();
    } catch (error) {
      console.error("Error creating Friend List:", error);
    }
  };

  useEffect(() => {
    if (!newMemberName) {
      setSearchResults([]);
    }
  }, [newMemberName]);

  return (
    <Box p={4}>
      <Heading as="h1" mb={4}>
        Create Friend List
      </Heading>
      <Heading as="h3" size="md" mb={2}>
        List name
      </Heading>
      <Input
        value={ListName}
        onChange={(event) => setListName(event.target.value)}
        placeholder="Enter the name of the list"
        mb={4}
      />

      <MembersList
        selectedMembers={selectedMembers}
        setSelectedMembers={setSelectedMembers}
        membersInList={membersInList}
        setMembersInList={setMembersInList}
        searchResults={searchResults}
        setSearchResults={setSearchResults}
        newMemberName={newMemberName}
        setNewMemberName={setNewMemberName}
        ownerUsername={ownerUsername}
        invitedMembersTitle={MembersTitleFriendList}
      />

      <Button onClick={handleSaveChangesList} my={3} colorScheme="green">
        Save Changes
      </Button>
    </Box>
  );
};

export default EditFriendList;
