// Importing constants for validation messages
import {
  descriptionMaxLength,
  nameMaxLength,
  nameMinLength,
  passwordMaxLength,
  passwordMinLength,
  phoneNumberLength,
  titleMaxLength,
  titleMinLength,
  usernameMaxLength,
  usernameMinLength,
} from "./constants";

/**
 * Invalid email validation message.
 * @type {string}
 */
export const invalidEmail = "Please provide a valid email.";

/**
 * Invalid password validation message.
 * @type {string}
 */
export const invalidPassword = `Please provide a valid password. (${passwordMinLength}-${passwordMaxLength} characters long, at least one number and one symbol.)`;

/**
 * Password and email mismatch validation message.
 * @type {string}
 */
export const passwordEmailMismatch =
  "Provided email or password are incorrect.";

/**
 * Login success message.
 * @type {string}
 */
export const logInSuccessText = "You have successfully logged in.";

/**
 * Signup success message.
 * @type {string}
 */
export const signUpSuccessText = "You have successfully signed up.";

/**
 * Logout success message.
 * @type {string}
 */
export const logOutSuccessText = "You have successfully logged out.";

/**
 * Update profile success message.
 * @type {string}
 */
export const updateProfileSuccessText = "Profile updated successfully!";

/**
 * Re-entered password mismatch validation message.
 * @type {string}
 */
export const reEnterPasswordMismatch = "Passwords do not match.";

/**
 * Invalid username validation message.
 * @type {string}
 */
export const invalidUsername = `Please provide a valid username. (${usernameMinLength}-${usernameMaxLength} characters long, only letters and numbers.)`;

/**
 * Invalid name validation message.
 * @type {string}
 */
export const invalidName = `Please provide a valid name. (${nameMinLength}-${nameMaxLength} characters long, only uppercase and lowercase letters.)`;

/**
 * Username already exists validation message.
 * @type {string}
 */
export const usernameExistsMsg = "This username is already taken.";

/**
 * Phone number already exists validation message.
 * @type {string}
 */
export const phoneExistsMsg = "This phone number is already taken.";

/**
 * Email already exists validation message.
 * @type {string}
 */
export const emailExistsMsg = "This email already exist.";

/**
 * Invalid Phone Number validation message.
 * @type {string}
 */
export const invalidPhone = `Please provide a valid phone number. (${phoneNumberLength} digits)`;

/**
 * Friend list success message.
 * @type {string}
 */
export const friendListSuccess =
  "Your friend list has been successfully created.";

/**
 * Friend list edit success message.
 * @type {string}
 */
export const friendListEditSuccess =
  "Your friend list has been successfully updated.";

/**
 * Event success message.
 * @type {string}
 */
export const eventSuccess = "Your event has been successfully created.";

/**
 * Event Edit success message.
 * @type {string}
 */
export const eventEditSuccess = "Your event has been edited successfully.";

/**
 * Image upload success message.
 * @type {string}
 */
export const imageUploadSuccess = "Your image has been successfully uploaded.";

/**
 * Invalid title validation message.
 * @type {string}
 */
export const invalidTitle = `Please provide a valid title. (${titleMinLength}-${titleMaxLength} characters long.)`;

/**
 * Invalid description validation message.
 * @type {string}
 */
export const invalidDescription = `The description cannot be more than ${descriptionMaxLength} characters long.`;

/**
 * Invalid date validation message.
 */
export const invalidDate = `Please provide a valid date.`;

/**
 * Invalid start and due dates validation message.
 */
export const invalidStartDueDates = `The due date cannot be before the start date.`;

/**
 * Invalid recurrence validation message.
 */
export const invalidRecurrence = `Please provide a valid recurrence interval.`;

/**
 * Invalid recurrence last event date validation message.
 */
export const invalidRecDueDate =
  "Please provide a valid recurrence last event date.";
