import AuthContext from "../../context/AuthContext";
import { useContext } from "react";
import ProfileView from "../../views/ProfileView";

/**
 * Renders a button that opens the user's profile view when clicked.
 */
const ProfileButton = () => {
  const { openModal } = useContext(AuthContext);

  /**
   * Opens the user's profile view in a modal.
   */
  const onProfileView = () => {
    openModal(<ProfileView />, "Profile");
  };

  return (
    <div style={{ width: "100%" }} onClick={onProfileView}>
      Profile
    </div>
  );
};

export default ProfileButton;
