import {
  get,
  set,
  ref,
  orderByChild,
  equalTo,
  query,
  update,
  onValue,
  DataSnapshot,
} from "firebase/database";

import { db } from "../config/firebase-config";
import { EventsPerMonth } from "./events.service";

export enum Role {
  Student = "Student",
  Teacher = "Teacher",
  Principal = "Principal",
}

export type UserData = {
  uid: string;
  email: string;
  username: string;
  firstName: string;
  lastName: string;
  role: Role;
  phoneNumber: string;
  address: string;
  isBlocked: boolean;
  createdOn: number;
  profilePicture: string;
  events: EventsPerMonth[];
};

/**
 * Updates the profile picture of a user with the given username.
 * @param {string} username - The username of the user to update.
 * @param {string} downloadURL - The download URL of the new profile picture.
 * @returns {Promise} A promise that resolves when the update is complete.
 */
export const updateProfilePicture = (username: string, downloadURL: string) => {
  return update(ref(db, `users/${username}`), {
    profilePicture: downloadURL,
  });
};

/**
 * Gets the user with the given handle.
 * @param {string} handle - The handle of the user to get.
 * @returns {Promise} A promise that resolves with the user data.
 */
export const getUserByHandle = (handle: string) => {
  return get(ref(db, `users/${handle}`));
};

/**
 * Gets the user with the given email.
 * @param {string} email - The email of the user to get.
 * @returns {Promise} A promise that resolves with the user data.
 */
export const getUserByEmail = (email: string) => {
  return get(query(ref(db, "users"), orderByChild("email"), equalTo(email)));
};

/**
 * Creates a new user with the given data.
 * @param {string} handle - The handle of the new user.
 * @param {string} uid - The UID of the new user.
 * @param {string} email - The email of the new user.
 * @param {string} firstName - The first name of the new user.
 * @param {string} lastName - The last name of the new user.
 * @param {string} phoneNumber - The phone number of the new user.
 * @param {string} role - The role of the new user.
 * @returns {Promise} A promise that resolves when the user is created.
 */
export const createUserHandle = (
  handle: string,
  uid: string,
  email: string | null,
  firstName: string,
  lastName: string,
  phoneNumber: string,
  address: string,
  role: string
) => {
  if (email === null) throw new Error("Email is null");

  return set(ref(db, `users/${handle}`), {
    uid,
    email,
    username: handle,
    firstName,
    lastName,
    role,
    phoneNumber,
    address,
    isBlocked: false,
    createdOn: Date.now(),
    profilePicture: null,
    firstNameLowerCase: firstName.toLowerCase(),
    lastNameLowerCase: lastName.toLowerCase(),
  });
};

/**
 * Gets the user with the given UID.
 * @param {string} uid - The UID of the user to get.
 * @returns {Promise} A promise that resolves with the user data.
 */
export const getUserData = (uid: string) => {
  return get(query(ref(db, "users"), orderByChild("uid"), equalTo(uid)));
};

/**
 * Gets the user with the given phone.
 * @param {string} phoneNumber - The phone of the user to get.
 * @returns {Promise} A promise that resolves with the user data.
 */
export const getUserByPhone = (phoneNumber: string) => {
  return get(
    query(ref(db, "users"), orderByChild("phoneNumber"), equalTo(phoneNumber))
  );
};

/**
 * Updates the general information of a user with the given handle.
 * @param {string} handle - The handle of the user to update.
 * @param {string} firstName - The new first name of the user.
 * @param {string} lastName - The new last name of the user.
 * @returns {Promise} A promise that resolves when the update is complete.
 */
export const updateUserGeneralInfo = (
  handle: string,
  firstName: string,
  lastName: string,
  phoneNumber: string,
  address: string
) => {
  return update(ref(db, `users/${handle}`), {
    firstName,
    firstNameLowerCase: firstName.toLowerCase(),
    lastName,
    lastNameLowerCase: lastName.toLowerCase(),
    phoneNumber,
    address,
  });
};

/**
 * Blocks a user with the given handle.
 * @param {string} handle - The handle of the user to block.
 * @returns {Promise} A promise that resolves when the user is blocked.
 */
export const blockUser = (handle: string) => {
  return update(ref(db, `users/${handle}`), {
    isBlocked: true,
  });
};

/**
 * Unblocks a user with the given handle.
 * @param {string} handle - The handle of the user to unblock.
 * @returns {Promise} A promise that resolves when the user is unblocked.
 */
export const unblockUser = (handle: string) => {
  return update(ref(db, `users/${handle}`), {
    isBlocked: false,
  });
};

export const setRole = (handle: string, role: Role) => {
  return update(ref(db, `users/${handle}`), {
    role,
  });
};

/**
 * Adds a phone number to a user with the given handle.
 * @param {string} handle - The handle of the user to update.
 * @param {string} phoneNumber - The new phone number of the user.
 * @returns {Promise} A promise that resolves when the update is complete.
 */
export const addPhoneToUser = (handle: string, phoneNumber: string) => {
  return update(ref(db, `users/${handle}`), {
    phoneNumber,
  });
};

/**
 * Sets a listener for changes to the "users" collection in the database.
 * @param {function} callback - The function to call when the data changes.
 * @returns {function} A function that can be called to remove the listener.
 */
export const setUsersListener = (callback: (snapshot: DataSnapshot) => any) => {
  const usersRef = ref(db, "users");
  return onValue(usersRef, (snapshot) => {
    callback(snapshot);
  });
};

/**
 * Gets all users in the "users" collection.
 * @returns {Promise} A promise that resolves with an array of all users.
 */
export const getAllUsers = (): Promise<UserData[]> => {
  return get(ref(db, "users")).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }

    return Object.keys(snapshot.val()).map((key) => {
      return { ...snapshot.val()[key] };
    });
  });
};

export const getUsersByFirstName = async (firstName: string) => {
  const users = await getAllUsers();

  const filteredUsers = users.filter(
    (user) => user.firstName && user.firstName.startsWith(firstName)
  );

  return filteredUsers;
};

export const getUsersByLastName = async (lastName: string) => {
  const users = await getAllUsers();

  const filteredUsers = users.filter(
    (user) => user.lastName && user.lastName.startsWith(lastName)
  );

  return filteredUsers;
};

/**
 * Gets users with the given handle (username).
 * @param {string} handle - The handle to search for.
 * @returns {Promise} A promise that resolves with an array of user data.
 */
export const getUsersByHandle = (handle: string) => {
  return get(
    query(ref(db, "users"), orderByChild("username"), equalTo(handle))
  );
};

/**
 * Updates a user's friend list with the given friend list ID.
 * @param {string} username - The username of the user to update.
 * @param {string} friendListId - The ID of the friend list to add to the user's friend lists.
 * @returns {Promise<void>} A promise that resolves when the update is complete.
 * @throws {Error} If there is an error updating the user's friend lists.
 */
export const updateUserWithFriendList = async (
  username: string,
  friendListId: string
) => {
  try {
    await update(ref(db, `users/${username}/friendLists`), {
      [friendListId]: true,
    });
  } catch (error) {
    console.error("Error updating user with Friend List:", error);
    throw error;
  }
};

/**
 * Removes a friend list ID from the user's friendLists in the database.
 * @param {string} username - The username of the user.
 * @param {string} friendListId - The ID of the friend list to remove.
 * @returns {Promise<void>} A promise that resolves when the update is complete.
 * @throws {Error} If there is an error updating the user's friendLists.
 */
export const removeFriendListFromUser = async (
  username: string,
  friendListId: string
) => {
  try {
    // Set the friend list ID to null to remove it from the user's friendLists
    await update(ref(db, `users/${username}/friendLists`), {
      [friendListId]: null,
    });
  } catch (error) {
    console.error("Error removing friend list from user:", error);
    throw error;
  }
};

/**
 * Adds an event ID to the user's list of events.
 * @param {string} username - The username of the user.
 * @param {string} eventID - The ID of the event to add.
 * @returns {Promise} A promise that resolves when the event is added to the user.
 */
export const addEventToUser = async (username: string, eventID: string) => {
  try {
    await update(ref(db, `users/${username}/events`), {
      [eventID]: true,
    });
  } catch (error) {
    console.error("Error adding event to user:", error);
    throw error;
  }
};

/**
 * Removes an event ID from the user's list of events.
 * @param {string} username - The username of the user.
 * @param {string} eventID - The ID of the event to remove.
 * @returns {Promise} A promise that resolves when the event is removed from the user.
 * @throws {Error} If there is an error removing the event from the user's list of events.
 */
export const removeEventFromUser = async (
  username: string,
  eventID: string
) => {
  try {
    await update(ref(db, `users/${username}/events`), {
      [eventID]: false,
    });
  } catch (error) {
    console.error("Error removing event from user:", error);
    throw error;
  }
};
