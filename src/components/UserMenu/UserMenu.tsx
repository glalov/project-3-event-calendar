import {
  Box,
  Flex,
  HStack,
  Menu,
  MenuButton,
  MenuDivider,
  MenuItem,
  MenuList,
  VStack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { FiChevronDown } from "react-icons/fi";
import LogOutButton from "./LogOutButton";
import { useContext, useState, useEffect } from "react";
import AuthContext from "../../context/AuthContext";
import ProfileButton from "./ProfileButton";
import AvatarImage from "./AvatarImage";
import { setupNotificationListener } from "../../services/notification.service";
import NotificationBell from "../Notification/NotificationBell";
import { useNavigate } from "react-router-dom";

const UserMenu = () => {
  const { userData } = useContext(AuthContext);
  const [notifications, setNotifications] = useState<any[]>([]);
  const [newNotificationsCount, setNewNotificationsCount] = useState(0);
  const navigate = useNavigate();

  useEffect(() => {
    // Set up the notification listener
    const unsubscribe = setupNotificationListener(
      userData?.username || "",
      (receivedNotifications) => {
        setNotifications(receivedNotifications);

        const newCount = receivedNotifications.filter(
          (notification) => notification.isNew
        ).length;
        setNewNotificationsCount(newCount);
      }
    );

    return () => {
      unsubscribe();
    };
  }, [userData]);

  return (
    <>
      <span
        onClick={() => {
          navigate("/notifications");
        }}
      >
        <NotificationBell count={newNotificationsCount} />
      </span>

      <Flex alignItems={"center"}>
        <Menu>
          <MenuButton
            py={2}
            transition="all 0.3s"
            _focus={{ boxShadow: "none" }}
          >
            <HStack>
              <AvatarImage />
              <VStack
                display={{ base: "none", md: "flex" }}
                alignItems="flex-start"
                spacing="1px"
                ml="2"
              >
                <Text fontSize="sm">{userData?.username}</Text>
                <Text fontSize="xs" color="gray.600">
                  {userData?.role}
                </Text>
              </VStack>
              <Box display={{ base: "none", md: "flex" }}>
                <FiChevronDown />
              </Box>
            </HStack>
          </MenuButton>
          <MenuList
            bg={useColorModeValue("white", "gray.900")}
            borderColor={useColorModeValue("gray.200", "gray.700")}
          >
            <MenuItem>
              <ProfileButton />
            </MenuItem>
            <MenuDivider />
            <MenuItem>
              <LogOutButton />
            </MenuItem>
          </MenuList>
        </Menu>
      </Flex>
    </>
  );
};

export default UserMenu;
