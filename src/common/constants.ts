/**
 * The Virus Total API key.
 * @type {string}
 */
export const vtAPIkey =
  "38c4de2ddde290bed1b7f683a1b6f858de34d1cf3641462a7e4c57db37c5b6ae";

/**
 * The Virus Total upload endpoint.
 * @type {string}
 */
export const uploadEndpointURL = "https://www.virustotal.com/api/v3/files";

/**
 * The Virus Total upload endpoint.
 * @type {string}
 */
export const vrFileUploadSuccess = "File uploaded successfully.";

/**
 * The English Dictionary endpoint.
 * @type {string}
 */
export const englishDictURL =
  "https://api.dictionaryapi.dev/api/v2/entries/en/";

/**
 * The minimum length allowed for a password.
 * @type {number}
 */
export const passwordMinLength = 8;

/**
 * The maximum length allowed for a password.
 * @type {number}
 */
export const passwordMaxLength = 30;

/**
 * The minimum length allowed for a username.
 * @type {number}
 */
export const usernameMinLength = 3;

/**
 * The maximum length allowed for a username.
 * @type {number}
 */
export const usernameMaxLength = 30;

/**
 * The minimum length allowed for a name.
 * @type {number}
 */
export const nameMinLength = 1;

/**
 * The maximum length allowed for a name.
 * @type {number}
 */
export const nameMaxLength = 30;

/**
 * The length for a phone number.
 * @type {number}
 */
export const phoneNumberLength = 10;

/**
 * The title of the members list component for invited members of the Create Event
 * @type {string}
 */
export const invitedMembersTitleEvent = "Invited Members";

/**
 * The title of the members list component of the Create Friend List
 * @type {string}
 */
export const MembersTitleFriendList = "Members in List";

/**
 * The minimum length allowed for a event title.
 * @type {number}
 */
export const titleMinLength = 3;

/**
 * The maximum length allowed for a event title.
 * @type {number}
 */
export const titleMaxLength = 30;

/**
 * The maximum length allowed for a event description.
 * @type {number}
 */
export const descriptionMaxLength = 500;
