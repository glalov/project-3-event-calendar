import { useContext, useState, useEffect, useMemo } from "react";
import AuthContext from "../context/AuthContext";
import { Alert, AlertIcon, Box, Flex, HStack, Stack } from "@chakra-ui/react";
import FromButton from "../components/Forms/FormButton";
import {
  addressText,
  emailText,
  firstNameText,
  lastNameText,
  phoneNumberText,
  updateProfileText,
  usernameText,
} from "../common/formTexts";
import UseSuccessToast from "../hooks/UseSuccessToast";
import FormGroupReadOnly from "../components/Forms/FormGroupReadOnly";
import FormGroup, { FormGroupProps } from "../components/Forms/FormGroup";
import {
  invalidName,
  invalidPhone,
  phoneExistsMsg,
  updateProfileSuccessText,
} from "../common/validationMessages";
import { isValidName, isValidPhone } from "../common/inputValidations";
import {
  getUserByPhone,
  updateUserGeneralInfo,
} from "../services/users.service";
import { uploadProfilePicture } from "../services/storage.service";
import AvatarImage from "../components/UserMenu/AvatarImage";
import UploadPicture from "../components/Forms/UploadPicture";

/**
 * This component represents the profile view of the user. It displays the user's profile information, including their username, email, role, created date, first name, last name, phone number, and address. The user can update their profile information by filling out the form and clicking the "Update Profile" button. They can also upload a profile picture by clicking the "Upload Picture" button and selecting a file. If there are any errors during the update or upload process, an error message will be displayed. The component uses various form input components, including FormGroup, FormGroupReadOnly, and UploadPicture, to display and handle user input.
 */
const ProfileView: React.FC = () => {
  const { userData, setUser } = useContext(AuthContext);

  const [form, setForm] = useState({
    address: "",
    firstName: "",
    lastName: "",
    phoneNumber: "",
  });
  const [validated, setValidated] = useState(false);
  const [profileUpdateError, setProfileUpdateError] = useState("");
  const toast = UseSuccessToast();

  useEffect(() => {
    if (userData) {
      setForm((prevForm) => {
        return {
          ...prevForm,
          address: userData.address,
          firstName: userData.firstName,
          lastName: userData.lastName,
          phoneNumber: userData.phoneNumber,
        };
      });
    }
  }, [userData]);

  const onProfileUpdate = (event: any) => {
    event.preventDefault();
    event.stopPropagation();

    setValidated(true);

    if (!areAllValid()) {
      return;
    }

    getUserByPhone(form.phoneNumber)
      .then((snapshot) => {
        if (snapshot.exists() && form.phoneNumber !== userData?.phoneNumber) {
          throw new Error("phoneExist");
        }

        return updateUserGeneralInfo(
          userData?.username || "",
          form.firstName,
          form.lastName,
          form.phoneNumber,
          form.address
        ).then(() => {
          setUser((prevState) => {
            return {
              ...prevState,
              userData: {
                ...prevState?.userData,
                firstName: form.firstName,
                lastName: form.lastName,
                phoneNumber: form.phoneNumber,
                address: form.address,
              },
            };
          });
          toast(updateProfileSuccessText, "update-success");
          setProfileUpdateError("");
        });
      })
      .catch((error) => {
        //const errorCode = error.code;
        const errorMessage = error.message;

        if (errorMessage === "phoneExist") {
          setProfileUpdateError(phoneExistsMsg);
        } else {
          setProfileUpdateError(errorMessage);
        }
      });
  };

  const handleUpload = (event: any, selectedFile: any, setInvalidMsg: any) => {
    event.preventDefault();
    event.stopPropagation();

    if (selectedFile) {
      uploadProfilePicture(userData?.username || "", selectedFile)
        .then((downloadURL) => {
          setUser((prevUserData) => {
            return {
              ...prevUserData,
              userData: {
                ...prevUserData?.userData,
                profilePicture: downloadURL,
              },
            };
          });
          setInvalidMsg("");
          toast(
            "Profile picture uploaded and updated successfully",
            "profile-pic-success"
          );
        })
        .catch((error) => {
          setInvalidMsg(error.message);
        });
    } else {
      setInvalidMsg("Please select a file to upload");
    }
  };

  const firstNameFormProps: FormGroupProps = {
    label: firstNameText,
    type: "text",
    name: "firstName",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: invalidName,
    classNames: "firstName",
    isValidFunc: isValidName,
    isRequired: false,
  };

  const lastNameFormProps: FormGroupProps = {
    label: lastNameText,
    type: "text",
    name: "lastName",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: invalidName,
    classNames: "lastName",
    isValidFunc: isValidName,
    isRequired: false,
  };

  const telephoneProps: FormGroupProps = {
    label: phoneNumberText,
    type: "text",
    name: "phoneNumber",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: invalidPhone,
    classNames: "phoneNumber",
    isValidFunc: isValidPhone,
    isRequired: false,
  };

  const addressProps: FormGroupProps = {
    label: addressText,
    type: "text",
    name: "address",
    form: form,
    setForm: setForm,
    validated: validated,
    invalidMsg: "",
    classNames: "address",
    isValid: true,
    isRequired: false,
  };

  const areAllValid = () => {
    return (
      isValidName(form.firstName) &&
      isValidName(form.lastName) &&
      isValidPhone(form.phoneNumber)
    );
  };

  const createdDate = useMemo(() => {
    if (userData?.createdOn) {
      const date = new Date(userData.createdOn);
      return date.toLocaleDateString();
    }
    return "";
  }, [userData?.createdOn]);

  return (
    <Flex width="full" align="center" justifyContent="center">
      <Box my={4} textAlign="left">
        <Stack spacing={4} p="1rem">
          <HStack>
            <Box m={2}>
              <AvatarImage size="xl" />
            </Box>
            <Box m={2}>
              <UploadPicture handleUpload={handleUpload} />
            </Box>
          </HStack>
        </Stack>
        <form className="log-form" onSubmit={onProfileUpdate}>
          <Stack spacing={4} p="1rem">
            {profileUpdateError && (
              <Alert status="error">
                <AlertIcon />
                {profileUpdateError}
              </Alert>
            )}

            <HStack>
              <Box m={2}>
                <FormGroupReadOnly
                  value={userData?.username}
                  label={usernameText}
                />
              </Box>
              <Box m={2}>
                <FormGroupReadOnly value={userData?.email} label={emailText} />
              </Box>
            </HStack>

            <HStack>
              <Box m={2}>
                <FormGroupReadOnly value={userData?.role} label="Role" />
              </Box>
              <Box m={2}>
                <FormGroupReadOnly value={createdDate} label="Created On" />
              </Box>
            </HStack>

            <HStack>
              <Box m={2}>
                <FormGroup {...firstNameFormProps} />
              </Box>
              <Box m={2}>
                <FormGroup {...lastNameFormProps} />
              </Box>
            </HStack>

            <HStack>
              <Box m={2}>
                <FormGroup {...telephoneProps} />
              </Box>
              <Box m={2}>
                <FormGroup {...addressProps} />
              </Box>
            </HStack>

            <FromButton
              width="full"
              mt={4}
              type="submit"
              loadingText="Submitting"
              size="lg"
              bg={"blue.400"}
              color={"white"}
              _hover={{
                bg: "blue.500",
              }}
            >
              {updateProfileText}
            </FromButton>
          </Stack>
        </form>
      </Box>
    </Flex>
  );
};

export default ProfileView;
