import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getStorage } from "firebase/storage";

// Your web app's Firebase configuration
export const firebaseConfig = {
  apiKey: "AIzaSyAErfASSd6mhQ9TjByTt8b_nSaKwPqRX_s",
  authDomain: "project-school-d00c0.firebaseapp.com",
  projectId: "project-school-d00c0",
  storageBucket: "project-school-d00c0.appspot.com",
  messagingSenderId: "163733379967",
  appId: "1:163733379967:web:5fa165ed0841fce981184a",
  databaseURL:
    "https://project-school-d00c0-default-rtdb.europe-west1.firebasedatabase.app/",
};

// Initialize the Firebase app with the configuration object
export const app = initializeApp(firebaseConfig);

// Get the Firebase authentication handler
export const auth = getAuth(app);

// Get the Firebase Realtime Database handler
export const db = getDatabase(app);

// Get the Firebase storage handler
export const storage = getStorage(app);
