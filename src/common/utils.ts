import { get, ref } from "firebase/database";
import { addDays, addMonths, addYears, format } from "date-fns";
import { db } from "../config/firebase-config";
import { EventData } from "./interfaces";
import { EventsPerMonth } from "../services/events.service";

/**
 * Returns an array of all hours in a day, represented as Date objects.
 * @returns An array of Date objects representing all hours in a day.
 */
export const getAllHoursInDay = (): Date[] => {
  return new Array(24).fill(0).reduce((acc, _, index) => {
    return acc.concat([
      new Date(0, 0, 0, index, 0),
      new Date(0, 0, 0, index, 30),
    ]);
  }, []);
};

/**
 * Contains utility functions for the event calendar application.
 */
export const toHourMinFormat = (date: Date) => {
  const hours = date.getHours();
  const minutes = date.getMinutes();

  const hoursStr = hours % 12 ? (hours % 12).toString() : "12";
  const minutesStr =
    minutes < 10 ? "0" + minutes.toString() : minutes.toString();

  const amPm = hours >= 12 ? "PM" : "AM";

  const timeString = hoursStr + ":" + minutesStr + " " + amPm;

  return timeString;
};

/**
 * Converts a given date to an index representing the hour and minute in a day.
 * @param date The date to convert to an index.
 * @returns The index representing the hour and minute in a day.
 */
export const convertHourToIndex = (date: Date) => {
  const hours = date.getHours();
  const minutes = date.getMinutes();

  const index = hours * 2 + (minutes >= 30 ? 1 : 0);

  return index;
};

/**
 * Converts a given date to an index representing the hour and minute in a day, excluding the half hour mark.
 * @param date The date to convert to an index.
 * @returns The index representing the hour and minute in a day, excluding the half hour mark.
 */
export const convertHourToIndexExcluding = (date: Date) => {
  const hours = date.getHours();
  const minutes = date.getMinutes();

  const index = hours * 2 + (minutes > 30 ? 1 : 0);

  if (minutes === 0) return index - 1;

  return index;
};

/**
 * Determines if a given index is within a specified range.
 * @param index The index to check.
 * @param min The minimum value of the range.
 * @param max The maximum value of the range.
 * @returns A boolean indicating whether the index is within the specified range.
 */
export const isIndexInRange = (index: number, min: number, max: number) => {
  if (min === -1 || max === -1) return false;
  return index >= min && index <= max;
};

/**
 * Determines if two index ranges are overlapping.
 * @param firstStartIndex The starting index of the first range.
 * @param firstEndIndex The ending index of the first range.
 * @param secondStartIndex The starting index of the second range.
 * @param secondEndIndex The ending index of the second range.
 * @returns A boolean indicating whether the two index ranges are overlapping.
 */
export const areIndexRangesOverlapping = (
  firstStartIndex: number,
  firstEndIndex: number,
  secondStartIndex: number,
  secondEndIndex: number
) => {
  return (
    isIndexInRange(firstStartIndex, secondStartIndex, secondEndIndex) ||
    isIndexInRange(firstEndIndex, secondStartIndex, secondEndIndex)
  );
};

export enum DateDurationType {
  UNSET,
  BEGINNING,
  TODAY,
  ENDING,
  FULLDAY,
}

/**
 * Determines the duration type of an event based on its start date, end date, and today's date.
 * @param today The current date.
 * @param startDate The start date of the event.
 * @param dueDate The end date of the event.
 * @returns The duration type of the event.
 */
export const getDateDurationType = (
  today: Date,
  startDate: Date,
  dueDate: Date
) => {
  const todayStr = today.toDateString();
  const startDateStr = startDate.toDateString();
  const dueDateStr = dueDate.toDateString();

  if (startDateStr === todayStr && dueDateStr === todayStr) {
    return DateDurationType.TODAY;
  } else if (startDateStr === todayStr) {
    return DateDurationType.BEGINNING;
  } else if (dueDateStr === todayStr) {
    return DateDurationType.ENDING;
  } else if (startDate < today && dueDate > today) {
    return DateDurationType.FULLDAY;
  } else {
    return DateDurationType.UNSET;
  }
};

/**
 * Returns a string representation of an event, including its title, start date, end date, and duration type.
 * @param title The title of the event.
 * @param startDate The start date of the event, in string format.
 * @param dueDate The end date of the event, in string format.
 * @param durationType The duration type of the event.
 * @returns A string representation of the event.
 */
export const getEventString = (
  title: string,
  startDate: string,
  dueDate: string,
  durationType: DateDurationType
) => {
  const startDateStr = toHourMinFormat(new Date(startDate));
  const dueDateStr = toHourMinFormat(new Date(dueDate));

  if (durationType === DateDurationType.BEGINNING) {
    return `${title} (${startDateStr} - ${new Date(dueDate).toDateString()})`;
  } else if (durationType === DateDurationType.ENDING) {
    return `${title} (${new Date(startDate).toDateString()} - ${dueDateStr})`;
  } else if (durationType === DateDurationType.FULLDAY) {
    return `${title} (${new Date(startDate).toDateString()} - ${new Date(
      dueDate
    ).toDateString()})`;
  } else if (durationType === DateDurationType.TODAY) {
    if (startDate === dueDate) {
      return `${title} (${startDateStr})`;
    }
    return `${title} (${startDateStr} - ${dueDateStr})`;
  } else {
    return `${title}`;
  }
};

/**
 * Checks if an event occurs on a given date, taking into account recurring events.
 * @param event The event to check.
 * @param day The date to check if the event occurs on.
 * @returns True if the event occurs on the given date, false otherwise.
 */
export const checkEventExistsOnDate = (event: EventsPerMonth, day: Date) => {
  const eventStartDate = new Date(event.startDate);
  const eventDueDate = new Date(event.dueDate);
  // Check if the event is recurring
  if (event.recurringRule?.enabled) {
    const untilDate = event.recurringRule.until;

    if (!untilDate || new Date(untilDate) >= day) {
      const interval = event.recurringRule.interval;
      const frequency = event.recurringRule.frequency;

      // Calculate the next occurrence date
      let nextOccurrence = new Date(event.startDate);
      while (nextOccurrence <= day) {
        switch (frequency) {
          case "days":
            nextOccurrence = addDays(nextOccurrence, interval);
            break;
          case "weeks":
            nextOccurrence = addDays(nextOccurrence, interval * 7);
            break;
          case "months":
            nextOccurrence = addMonths(nextOccurrence, interval);
            break;
          case "years":
            nextOccurrence = addYears(nextOccurrence, interval);
            break;
          default:
            break;
        }
      }

      // Check if the next occurrence is on the current day
      if (format(nextOccurrence, "yyyy-MM-dd") === format(day, "yyyy-MM-dd")) {
        return true;
      }
    }
  }
  eventStartDate.setHours(0, 0, 0, 0);
  eventDueDate.setHours(23, 59, 59, 999);
  // Check if the non-recurring event is on the current day
  return day >= eventStartDate && day <= eventDueDate;
};

/**
 * Retrieves all events created by a user with the given username and userId.
 * @param username The username of the user whose events to retrieve.
 * @param userId The ID of the user whose events to retrieve.
 * @returns An array of events created by the user, or an empty array if there are no events or an error occurs.
 */
export const getEventsCreatedByUser = async (
  username: string,
  userId: number
) => {
  if (userId) {
    try {
      const userCreatedEventsRef = ref(db, `users/${username}/createdEvents`);
      const userCreatedEventsSnapshot = await get(userCreatedEventsRef);

      if (userCreatedEventsSnapshot.exists()) {
        const eventIds = Object.keys(userCreatedEventsSnapshot.val());
        const events = [];

        for (const eventId of eventIds) {
          const eventRef = ref(db, `events/${eventId}`);
          const eventSnapshot = await get(eventRef);
          if (eventSnapshot.exists()) {
            const eventData = { id: eventId, ...eventSnapshot.val() };
            events.push(eventData);
          }
        }
        return events;
      } else {
        return [];
      }
    } catch (error) {
      console.error("Error getting events:", error);
      return [];
    }
  }
};
