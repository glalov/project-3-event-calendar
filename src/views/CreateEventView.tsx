import React, { useState } from "react";
import {
  Box,
  Flex,
  Heading,
  Button,
  Checkbox,
  Input,
  FormControl,
  FormLabel,
  Textarea,
  Select,
  FormErrorMessage,
} from "@chakra-ui/react";
import MembersList from "../components/Event/MembersList";
import { invitedMembersTitleEvent } from "../common/constants";
import FriendListModule from "../components/Event/FriendListModule";
import { addEventToDatabase } from "../services/events.service";
import { addEventToUser } from "../services/users.service";
import UseSuccessToast from "../hooks/UseSuccessToast";
import {
  eventSuccess,
  imageUploadSuccess,
  invalidDate,
  invalidDescription,
  invalidRecDueDate,
  invalidRecurrence,
  invalidStartDueDates,
  invalidTitle,
} from "../common/validationMessages";
import UploadPicture from "../components/Forms/UploadPicture";
import uploadImage from "../services/storage.service";
import { User } from "../common/interfaces";
import { EventData, RecurringRule } from "../common/interfaces";
import { addMinutes, format } from "date-fns";
import { addNotificationToUsers } from "../services/notification.service";
import {
  areValidStartDueDates,
  isRecurrenceIntervalValid,
  isValidDate,
  isValidDescription,
  isValidTitle,
} from "../common/inputValidations";

interface CreateEventViewProps {
  ownerUsername: string | undefined;
  startDate?: Date;
}

interface FriendList {
  members: { [key: string]: boolean };
  id: string;
  name: string;
}

/**
 * The Create Event View component
 */
const CreateEventView: React.FC<CreateEventViewProps> = ({
  ownerUsername,
  startDate,
}) => {
  const dueMinutesAddition = 30;
  const cleanEvent = {
    title: "",
    description: "",
    isPrivate: true,
    roomNumber: "",
    startDate: startDate ? format(startDate, "yyyy-MM-dd") : "",
    dueDate: startDate
      ? format(addMinutes(startDate, dueMinutesAddition), "yyyy-MM-dd")
      : "",
    startHours: startDate ? format(startDate, "HH:mm") : "12:00",
    dueHours: startDate
      ? format(addMinutes(startDate, dueMinutesAddition), "HH:mm")
      : "13:00",
    recurringRule: {
      enabled: false,
      frequency: "",
      interval: 0,
      until: "",
    },
    image: "",
    owner: ownerUsername,
    selectedMembers: {},
    selectedFriendLists: {},
  };

  const [validated, setValidated] = useState(false);
  const [invalidDueDateText, setInvalidDueDateText] = useState("");

  const [showDescription, setShowDescription] = useState(false);
  const [recurring, setRecurring] = useState(false);
  const [form, setForm] = useState(cleanEvent);
  const toast = UseSuccessToast();
  const [eventImage, setEventImage] = useState("");

  // State variables for MembersList
  const [newMemberName, setNewMemberName] = useState("");
  const [selectedMembers, setSelectedMembers] = useState<User[]>([]);
  const [membersInList, setMembersInList] = useState<User[]>([]);
  const [searchResults, setSearchResults] = useState<User[]>([]);

  // State variable for FriendListModule
  const [selectedFriendLists, setSelectedFriendLists] = useState<FriendList[]>(
    []
  );

  const updateForm =
    (prop: keyof typeof form) =>
    (
      e: React.ChangeEvent<
        HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement
      >
    ) => {
      const value = prop === "isPrivate" ? !form.isPrivate : e.target.value;
      setForm({
        ...form,
        [prop]: value,
      });
    };

  const resetForm = () => {
    setForm(cleanEvent);
    setRecurring(false);
    setSelectedMembers([]);
    setMembersInList([]);
    setSearchResults([]);
    setSelectedFriendLists([]);
    setNewMemberName("");
    setEventImage("");
    setValidated(false);
    setInvalidDueDateText("");
  };

  const handleUpload = async (
    event: any,
    selectedFile: any,
    setInvalidMsg: any
  ) => {
    event.preventDefault();
    if (!selectedFile) {
      setInvalidMsg("Please select an image to upload.");
      return;
    }

    try {
      const imageUrl = await uploadImage(selectedFile);
      setEventImage(imageUrl || "");
      setInvalidMsg("");
      toast(imageUploadSuccess, "img-success");
    } catch (error) {
      console.error("Error uploading image:", error);
      setInvalidMsg("Error uploading image. Please try again.");
    }
  };

  const handleCreateEvent = async (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.preventDefault();
    event.stopPropagation();

    setValidated(true);

    if (!areAllValid()) {
      if (!isValidDate(form.dueDate)) {
        setInvalidDueDateText(invalidDate);
      } else if (
        !areValidStartDueDates(
          form.startDate,
          form.startHours,
          form.dueDate,
          form.dueHours
        )
      ) {
        setInvalidDueDateText(invalidStartDueDates);
      }

      return;
    }

    const selectedMemberUsernames = membersInList.reduce((acc, member) => {
      acc[member.username] = true;
      return acc;
    }, {} as { [key: string]: boolean });

    const selectedFriendListIds = selectedFriendLists.reduce((acc, list) => {
      acc[list.id] = true;
      return acc;
    }, {} as { [key: string]: boolean });

    const startDateWithHours = new Date(
      form.startDate + " " + form.startHours
    ).toISOString();
    const dueDateWithHours = new Date(
      form.dueDate + " " + form.dueHours
    ).toISOString();

    let recurringRule: RecurringRule | null = null;
    if (recurring) {
      recurringRule = {
        enabled: true,
        frequency: form.recurringRule?.frequency || "days",
        interval: form.recurringRule?.interval || 1,
        until: form.recurringRule?.until || "",
      };
    }

    const eventData: EventData = {
      description: form.description,
      isPrivate: form.isPrivate,
      roomNumber: form.roomNumber,
      title: form.title,
      recurringRule,
      selectedMembers: selectedMemberUsernames,
      selectedFriendLists: selectedFriendListIds,
      owner: ownerUsername || "",
      image: eventImage,
      startDate: startDateWithHours,
      dueDate: dueDateWithHours,
    };

    try {
      const eventID = await addEventToDatabase(eventData);

      await addEventToUser(ownerUsername || "", eventID || "");
      const usernamesInFriendLists = selectedFriendLists.reduce(
        (accumulator, item) => {
          const members = item.members;
          const memberUsernames = Object.keys(members);
          return [...accumulator, ...memberUsernames];
        },
        [] as string[]
      );

      const usernamesInInvitedMembers = membersInList.reduce((acc, member) => {
        acc.push(member.username);
        return acc;
      }, [] as string[]);

      const allInvitedUserNamesInEvent = [
        ...usernamesInInvitedMembers,
        ...usernamesInFriendLists,
      ];

      await addNotificationToUsers(eventID || "", allInvitedUserNamesInEvent);
      toast(eventSuccess, "event-success");

      resetForm();
    } catch (error) {
      console.error("Error creating event:", error);
    }
  };

  const areAllValid = () => {
    return (
      isValidTitle(form.title) &&
      isValidDescription(form.description) &&
      isValidDate(form.startDate) &&
      isValidDate(form.dueDate) &&
      areValidStartDueDates(
        form.startDate,
        form.startHours,
        form.dueDate,
        form.dueHours
      ) &&
      (!recurring || isRecurrenceIntervalValid(form.recurringRule?.interval)) &&
      (!recurring ||
        (isValidDate(form.recurringRule?.until) &&
          areValidStartDueDates(
            form.dueDate,
            form.dueHours,
            form.recurringRule?.until,
            "11:59 PM"
          )))
    );
  };

  return (
    <Box p={4}>
      <Heading as="h1" mb={4}>
        Create Event
      </Heading>

      <Box p={4}>
        <UploadPicture handleUpload={handleUpload} />
      </Box>
      <form>
        <FormControl
          isRequired={true}
          isInvalid={!isValidTitle(form.title) && validated}
        >
          <Flex alignItems="center" mb={4}>
            <FormLabel flex="0 0 120px">Event Title</FormLabel>
            <Input
              flex="1"
              placeholder="Event Title"
              ml={4}
              value={form.title}
              onChange={updateForm("title")}
            />
          </Flex>
          <FormErrorMessage>{invalidTitle}</FormErrorMessage>
        </FormControl>

        <FormControl
          mt={4}
          isRequired={true}
          isInvalid={!isValidDescription(form.description) && validated}
        >
          <FormLabel>
            <Checkbox
              onChange={() => setShowDescription(!showDescription)}
              isChecked={showDescription}
            >
              Include Description
            </Checkbox>
          </FormLabel>
          {showDescription && (
            <Textarea
              onChange={updateForm("description")}
              value={form.description}
              mt={2}
              placeholder="Event Description"
            />
          )}
          <FormErrorMessage>{invalidDescription}</FormErrorMessage>
        </FormControl>

        <Flex mt={4}>
          <FormControl>
            <Checkbox
              onChange={(e) => updateForm("isPrivate")(e)}
              isChecked={!form.isPrivate}
            >
              This event is Public
            </Checkbox>
          </FormControl>
          <FormControl>
            <Flex alignItems="center">
              <FormLabel>Location:</FormLabel>
              <Input
                placeholder="Room number"
                ml={2}
                value={form.roomNumber}
                onChange={updateForm("roomNumber")}
              />
            </Flex>
          </FormControl>
        </Flex>

        <Flex mt={4}>
          <FormControl
            isRequired={true}
            isInvalid={!isValidDate(form.startDate) && validated}
          >
            <FormLabel>Start Date</FormLabel>
            <Input
              type="date"
              value={form.startDate}
              onChange={updateForm("startDate")}
            />
            <FormErrorMessage>{invalidDate}</FormErrorMessage>
          </FormControl>
          <FormControl isRequired={true} ml={4}>
            <FormLabel>Start Hours</FormLabel>
            <Input
              type="time"
              value={form.startHours}
              onChange={updateForm("startHours")}
            />
          </FormControl>
        </Flex>

        <Flex mt={4}>
          <FormControl
            isRequired={true}
            isInvalid={
              (!isValidDate(form.dueDate) ||
                !areValidStartDueDates(
                  form.startDate,
                  form.startHours,
                  form.dueDate,
                  form.dueHours
                )) &&
              validated
            }
          >
            <FormLabel>Due Date</FormLabel>
            <Input
              type="date"
              value={form.dueDate}
              onChange={updateForm("dueDate")}
            />
            <FormErrorMessage>{invalidDueDateText}</FormErrorMessage>
          </FormControl>
          <FormControl ml={4} isRequired={true}>
            <FormLabel>Due Hours</FormLabel>
            <Input
              type="time"
              value={form.dueHours}
              onChange={updateForm("dueHours")}
            />
          </FormControl>
        </Flex>

        <Flex mt={4}>
          <Checkbox
            onChange={() => setRecurring(!recurring)}
            isChecked={recurring}
          >
            Recurring
          </Checkbox>
        </Flex>
        {recurring && (
          <Flex mt={2} ml={4} alignItems="center">
            <FormControl
              isRequired={true}
              isInvalid={
                !isRecurrenceIntervalValid(form.recurringRule?.interval) &&
                validated
              }
            >
              <Flex>
                <FormLabel>Every:</FormLabel>
                <Input
                  type="number"
                  value={form.recurringRule?.interval}
                  onChange={(e) =>
                    setForm({
                      ...form,
                      recurringRule: {
                        ...form.recurringRule,
                        interval: parseInt(e.target.value),
                      },
                    })
                  }
                  w="50px"
                  ml={2}
                />
                <Select
                  ml={2}
                  value={form.recurringRule?.frequency}
                  onChange={(e) =>
                    setForm({
                      ...form,
                      recurringRule: {
                        ...form.recurringRule,
                        frequency: e.target.value,
                      },
                    })
                  }
                  w="100px"
                >
                  <option value="days">day(s)</option>
                  <option value="weeks">week(s)</option>
                  <option value="months">month(s)</option>
                  <option value="years">year(s)</option>
                </Select>
              </Flex>
              <FormErrorMessage>{invalidRecurrence}</FormErrorMessage>
            </FormControl>
            <FormControl
              isRequired={true}
              isInvalid={
                (!isValidDate(form.recurringRule?.until) ||
                  !areValidStartDueDates(
                    form.dueDate,
                    form.dueHours,
                    form.recurringRule?.until,
                    "11:59 PM"
                  )) &&
                validated
              }
            >
              <Flex>
                <FormLabel flex="0 0 140px" ml={2}>
                  Last Event Date:
                </FormLabel>
                <Input
                  type="date"
                  value={form.recurringRule?.until}
                  onChange={(e) =>
                    setForm({
                      ...form,
                      recurringRule: {
                        ...form.recurringRule,
                        until: e.target.value,
                      },
                    })
                  }
                  w="150px"
                />
              </Flex>
              <FormErrorMessage>{invalidRecDueDate}</FormErrorMessage>
            </FormControl>
          </Flex>
        )}
        <MembersList
          selectedMembers={selectedMembers}
          setSelectedMembers={setSelectedMembers}
          membersInList={membersInList}
          setMembersInList={setMembersInList}
          searchResults={searchResults}
          setSearchResults={setSearchResults}
          newMemberName={newMemberName}
          setNewMemberName={setNewMemberName}
          ownerUsername={ownerUsername || ""}
          invitedMembersTitle={invitedMembersTitleEvent}
        />

        <FriendListModule
          username={ownerUsername || ""}
          selectedFriendLists={selectedFriendLists}
          setSelectedFriendLists={setSelectedFriendLists}
        />

        <Button
          onClick={handleCreateEvent}
          type="submit"
          my={3}
          colorScheme="green"
        >
          Create Event
        </Button>
      </form>
    </Box>
  );
};

export default CreateEventView;
