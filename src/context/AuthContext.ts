import { User } from "firebase/auth";
import { createContext, Dispatch, SetStateAction, useContext } from "react";
import { UserData } from "../services/users.service";
import { CalendarData } from "../components/Calendar/Calendar";

export type AuthContextType = {
  user: User | null | undefined;
  userData: UserData | null;
  setUser: Dispatch<
    SetStateAction<{ user: User | null | undefined; userData: UserData | null }>
  >;
  openModal: (content: React.ReactNode, title?: string) => void;
  closeModal: () => void;
  calendarData: CalendarData | null;
};

export const AuthContextDefaultValue: AuthContextType = {
  user: null,
  userData: null,
  setUser: () => {
    throw new Error("setUser function must be overridden");
  },
  openModal: () => {
    throw new Error("openModal function must be overridden");
  },
  closeModal: () => {
    throw new Error("closeModal function must be overridden");
  },
  calendarData: null,
};

/**
 * Context object for authentication-related data.
 * @typedef {Object} AuthContext
 * @property {Object} user - The currently logged in user.
 * @property {string} handle - The handle of the currently logged in user.
 * @property {Object} userData - Additional data related to the currently logged in user.
 * @property {Function} setContext - Function to set the context. The real implementation comes from App.jsx.
 */
const AuthContext = createContext(AuthContextDefaultValue);

export const useAuth = () => useContext(AuthContext);

export default AuthContext;
