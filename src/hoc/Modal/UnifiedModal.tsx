import React from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react";
import "./UnifiedModal.css";

interface UnifiedModalProps {
  isOpen: boolean;
  onClose: () => void;
  title: string;
  content: React.ReactNode;
  actionButtons?: React.ReactNode;
}

/**
 * A modal component that displays a title, content and optional action buttons.
 * @param isOpen - A boolean indicating whether the modal is open or not.
 * @param onClose - A function that is called when the modal is closed.
 * @param title - A string representing the title of the modal.
 * @param content - A React node representing the content of the modal.
 * @param actionButtons - An optional React node representing the action buttons of the modal.
 * @returns A React component that displays a modal.
 */
const UnifiedModal: React.FC<UnifiedModalProps> = ({
  isOpen,
  onClose,
  title,
  content,
  actionButtons,
}) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent className="custom-modal-content">
        <ModalHeader>{title}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>{content}</ModalBody>
        {actionButtons && <ModalFooter>{actionButtons}</ModalFooter>}
      </ModalContent>
    </Modal>
  );
};

export default UnifiedModal;
