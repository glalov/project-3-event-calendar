import { Select } from "@chakra-ui/react";
import { useCallback, useContext } from "react";
import DayMode from "./DayMode";
import WeekMode from "./WeekMode";
import MonthMode from "./MonthMode";
import CalendarHeader from "./CalendarHeader";
import "./MonthMode.css";
import {
  addDays,
  addMonths,
  addWeeks,
  setMonth,
  setYear,
  subDays,
  subMonths,
  subWeeks,
} from "date-fns";
import { enUS } from "date-fns/locale";
import AuthContext from "../../context/AuthContext";

export enum CalendarMode {
  Day = "Day",
  Week = "Week",
  WorkWeek = "WorkWeek",
  Month = "Month",
}

export type CalendarData = {
  mode: CalendarMode;
  setMode: React.Dispatch<React.SetStateAction<CalendarMode>>;
  currentDate: Date;
  setCurrentDate: React.Dispatch<React.SetStateAction<Date>>;
  todayDate: Date;
};

/**
 * The `Calendar` component displays a calendar with different modes (day, week, work week, month, year).
 * It receives the current mode, current date, and today's date from the `AuthContext` and renders the appropriate mode.
 * It also allows the user to switch between modes, navigate to different dates, and select a specific date.
 */
const Calendar = () => {
  const { calendarData } = useContext(AuthContext);
  const { mode, setMode, currentDate, setCurrentDate, todayDate } = calendarData
    ? calendarData
    : {
        mode: null,
        setMode: null,
        currentDate: null,
        setCurrentDate: null,
        todayDate: null,
      };

  const locale = enUS;
  const weekStartsOn: 0 | 1 | 2 | 3 | 4 | 5 | 6 | undefined = 1;

  let handleNext = () => {
    null;
  };
  let handlePrev = () => {
    null;
  };

  const handleChangeMonth = useCallback(
    (month: number) => {
      if (setCurrentDate) setCurrentDate(setMonth(currentDate, month));
    },
    [currentDate, setCurrentDate]
  );

  const handleChangeYear = useCallback(
    (year: number) => {
      if (setCurrentDate) setCurrentDate(setYear(currentDate, year));
    },
    [currentDate, setCurrentDate]
  );

  const modeTexts = ["Day", "Week", "Work Week", "Month"];

  const selectMode = (
    <Select
      onChange={(event) => {
        if (setMode)
          setMode(
            CalendarMode[event.target.value as keyof typeof CalendarMode]
          );
      }}
      value={mode as string}
    >
      {Object.values(CalendarMode).map((mode) => (
        <option key={mode} value={mode}>
          {modeTexts[Object.values(CalendarMode).indexOf(mode)]}
        </option>
      ))}
    </Select>
  );

  let calendarMode = null;
  const modeProps = {
    todayDate: todayDate ? todayDate : new Date(),
    currentDate: currentDate ? currentDate : new Date(),
    locale,
    weekStartsOn,
  };
  const handleDateClick = (clickedDate: Date) => {
    if (setCurrentDate) setCurrentDate(clickedDate);
    if (setMode) setMode(CalendarMode.Day);
  };
  const handleWeekClick = (clickedDate: Date) => {
    if (setCurrentDate) setCurrentDate(clickedDate);
    if (setMode) setMode(CalendarMode.Week);
  };

  if (mode === CalendarMode.Day) {
    calendarMode = <DayMode {...modeProps} />;

    handleNext = () => {
      if (setCurrentDate) setCurrentDate(addDays(currentDate, 1));
    };
    handlePrev = () => {
      if (setCurrentDate) setCurrentDate(subDays(currentDate, 1));
    };
  } else if (mode === CalendarMode.Week || mode === CalendarMode.WorkWeek) {
    calendarMode = (
      <WeekMode
        key={mode}
        {...modeProps}
        showWorkWeek={mode === CalendarMode.WorkWeek}
        onDateClick={handleDateClick}
      />
    );

    handleNext = () => {
      if (setCurrentDate) setCurrentDate(addWeeks(currentDate, 1));
    };
    handlePrev = () => {
      if (setCurrentDate) setCurrentDate(subWeeks(currentDate, 1));
    };
  } else if (mode === CalendarMode.Month) {
    calendarMode = (
      <MonthMode
        {...modeProps}
        onDateClick={handleDateClick}
        onWeekClick={handleWeekClick}
      />
    );
    handleNext = () => {
      if (setCurrentDate) setCurrentDate(addMonths(currentDate, 1));
    };
    handlePrev = () => {
      if (setCurrentDate) setCurrentDate(subMonths(currentDate, 1));
    };
  }

  return (
    <div className="calendar-container">
      <CalendarHeader
        handlePrev={handlePrev}
        handleNext={handleNext}
        handleChangeMonth={handleChangeMonth}
        handleChangeYear={handleChangeYear}
        currentDate={currentDate}
        setCurrentDate={setCurrentDate}
        selectMode={selectMode}
        locale={locale}
      />
      {calendarMode}
    </div>
  );
};

export default Calendar;
