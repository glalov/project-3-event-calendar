import React from "react";
import { useAuth } from "../../context/AuthContext";
import EventCard from "./EventCard";
import "./ListEvents.css";

/**
 * Component that displays a list of events owned by the user and events the user is invited to.
 * @returns JSX.Element
 */
const ListEvents: React.FC = () => {
  const { userData } = useAuth();

  const eventsOwnedByUser = userData?.events.filter(
    (event) => event.owner == userData.username
  );
  const eventsInvite = userData?.events.filter(
    (event) => event.owner !== userData.username
  );

  const eventsOwned = () => {
    return (
      eventsOwnedByUser &&
      eventsOwnedByUser.map((event) => (
        <div key={event.id} className="w-full p-4">
          <EventCard
            eventData={event}
            username={userData?.username}
            eventId={event.id}
          />
        </div>
      ))
    );
  };

  const eventsInvites = () => {
    return (
      eventsInvite &&
      eventsInvite.map((event) => (
        <div key={event.id} className="w-full p-4">
          <EventCard
            eventData={event}
            username={event.owner}
            eventId={event.id}
          />
        </div>
      ))
    );
  };

  return (
    <div className="w-full mt-10 flex flex-col items-center ">
      <h1>My Events:</h1>
      <div className="bg-white mx-auto w-10/12 shadow-xl rounded-lg border mt-2 ">
        <div className="grid gap-4 md:grid-cols-2 lg:grid-cols-3 mt-4 ">
          {eventsOwned()}
        </div>
      </div>
      <h1>Events that I'm invited to:</h1>
      <div className="bg-white mx-auto w-10/12 shadow-xl rounded-lg border mt-2 ">
        <div className="grid gap-4 md:grid-cols-2 lg:grid-cols-3 mt-4 ">
          {eventsInvites()}
        </div>
      </div>
    </div>
  );
};

export default ListEvents;
