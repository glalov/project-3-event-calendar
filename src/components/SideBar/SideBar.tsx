"use client";

import {
  IconButton,
  Box,
  CloseButton,
  Flex,
  HStack,
  Icon,
  useColorModeValue,
  Text,
  Drawer,
  DrawerContent,
  useDisclosure,
  BoxProps,
  FlexProps,
  Image,
} from "@chakra-ui/react";
import {
  FiHome,
  FiCalendar,
  FiBell,
  FiList,
  FiMenu,
  FiPenTool,
  FiUsers,
  FiBook,
} from "react-icons/fi";
import { IconType } from "react-icons";
import Logo from "/logo.png";
import ThemeToggler from "../Theme/ThemeToggler";
import { useContext, useState } from "react";
import AuthContext from "../../context/AuthContext";
import UserMenu from "../UserMenu/UserMenu";
import AuthMenu from "./AuthMenu";
import CreateFriendListView from "../../views/CreateFriendListView";
import CreateEventView from "../../views/CreateEventView";
import { useNavigate } from "react-router-dom";

interface LinkItemProps {
  name: string;
  icon: IconType;
  onClick?: () => void;
}

interface NavItemProps extends FlexProps {
  icon: IconType;
  children: React.ReactNode;
  onClick?: () => void;
}

interface MobileProps extends FlexProps {
  onOpen: () => void;
}

interface SidebarProps extends BoxProps {
  onClose: () => void;
}

const SidebarContent = ({ onClose, ...rest }: SidebarProps) => {
  const { user, userData, openModal } = useContext(AuthContext);
  const navigate = useNavigate();

  const LinkItems: Array<LinkItemProps> = [
    { name: "Home", icon: FiHome, onClick: () => navigate("/") },
    {
      name: "Add Friend List",
      icon: FiList,
      onClick: () =>
        openModal(<CreateFriendListView ownerUsername={userData?.username} />),
    },
    {
      name: "Create Event",
      icon: FiPenTool,
      onClick: () =>
        openModal(<CreateEventView ownerUsername={userData?.username} />),
    },
    {
      name: "Calendar",
      icon: FiCalendar,
      onClick: () => navigate("/calendar"),
    },
    { name: "My Events", icon: FiBell, onClick: () => navigate("/myevent") },
    {
      name: "My Friend List",
      icon: FiUsers,
      onClick: () => navigate("/myfriendlists"),
    },
    {
      name: "English Dictionary",
      icon: FiBook,
      onClick: () => navigate("/english-dictionary"),
    },
  ];

  return (
    <Box
      transition="3s ease"
      bg={useColorModeValue("white", "gray.900")}
      borderRight="1px"
      borderRightColor={useColorModeValue("gray.200", "gray.700")}
      w={{ base: "full", md: 60 }}
      pos="fixed"
      h="full"
      {...rest}
    >
      <Flex
        direction="column"
        h={{ base: "auto", md: "230px" }}
        alignItems="center"
        mx="-1"
        justifyContent="space-between"
        py="4"
      >
        <Box
          display={{ base: "none", md: "block" }}
          width={{ base: "100px", md: "auto" }}
        >
          <Image src={Logo} alt="Logo" />
        </Box>
        <CloseButton display={{ base: "flex", md: "none" }} onClick={onClose} />
      </Flex>
      {LinkItems.map((link) => (
        <NavItem key={link.name} icon={link.icon} onClick={link.onClick}>
          {link.name}
        </NavItem>
      ))}
    </Box>
  );
};

const NavItem = ({ icon, children, onClick, ...rest }: NavItemProps) => {
  const { userData } = useContext(AuthContext);
  const isAuthenticated = userData !== null;

  const handleClick = () => {
    if (isAuthenticated) {
      onClick?.();
    }
  };

  return (
    <Box
      as="a"
      onClick={handleClick}
      style={{ textDecoration: "none" }}
      _focus={{ boxShadow: "none" }}
    >
      <Flex
        align="center"
        p="4"
        mx="4"
        borderRadius="lg"
        role="group"
        cursor="pointer"
        _hover={{
          bg: "var(--button_color)",
          color: "white",
        }}
        {...rest}
      >
        {icon && (
          <Icon
            mr="4"
            fontSize="16"
            _groupHover={{
              color: "white",
            }}
            as={icon}
          />
        )}
        {children}
      </Flex>
    </Box>
  );
};

const MobileNav = ({ onOpen, ...rest }: MobileProps) => {
  const { user, userData } = useContext(AuthContext);

  return (
    <Flex
      ml={{ base: 0, md: 60 }}
      px={{ base: 4, md: 4 }}
      height="20"
      alignItems="center"
      bg={useColorModeValue("white", "gray.900")}
      borderBottomWidth="1px"
      borderBottomColor={useColorModeValue("gray.200", "gray.700")}
      justifyContent={{ base: "space-between", md: "flex-end" }}
      {...rest}
    >
      <IconButton
        display={{ base: "flex", md: "none" }}
        onClick={onOpen}
        variant="outline"
        aria-label="open menu"
        icon={<FiMenu />}
      />

      <HStack spacing={{ base: "0", md: "6" }}>
        <ThemeToggler />
        {userData !== null && <UserMenu />}
        {userData === null && <AuthMenu />}
      </HStack>
    </Flex>
  );
};

const SidebarWithHeader = ({ children }: { children: React.ReactNode }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <Box minH="100vh" bg={useColorModeValue("gray.100", "gray.900")}>
      <SidebarContent
        onClose={() => onClose}
        display={{ base: "none", md: "block" }}
      />
      <Drawer
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        returnFocusOnClose={false}
        onOverlayClick={onClose}
        size="full"
      >
        <DrawerContent>
          <SidebarContent onClose={onClose} />
        </DrawerContent>
      </Drawer>
      {/* mobilenav */}
      <MobileNav onOpen={onOpen} />
      <Box ml={{ base: 0, md: 60 }} p="4">
        {children}
      </Box>
    </Box>
  );
};

export default SidebarWithHeader;
