/**
 * Text for the "Re-enter password" input label.
 * @type {string}
 */
export const reEnterPassText = "Re-enter password";

/**
 * Text for the "Username" input label.
 * @type {string}
 */
export const usernameText = "Username";

/**
 * Text for the "Password" input label.
 * @type {string}
 */
export const passwordText = "Password";

/**
 * Text for the "Email address" input label.
 * @type {string}
 */
export const emailText = "Email address";

/**
 * Text for the "Log-In" button.
 * @type {string}
 */
export const loginText = "Log-In";

/**
 * Text for the "Sign up" button.
 * @type {string}
 */
export const signUpText = "Sign up";

/**
 * Text for the "First Name" input label.
 * @type {string}
 */
export const firstNameText = "First Name";

/**
 * Text for the "Last Name" input label.
 * @type {string}
 */
export const lastNameText = "Last Name";

/**
 * Text for the "Phone Number" input label.
 * @type {string}
 */
export const phoneNumberText = "Phone Number";

/**
 * Text for the "Address" input label.
 * @type {string}
 */
export const addressText = "Address";

/**
 * Text for the "Update Profile" button.
 * @type {string}
 */
export const updateProfileText = "Update Profile";

/**
 * Text for the "Role" input label.
 * @type {string}
 */
export const roleText = "Please select your role";
