import { useColorMode, IconButton } from "@chakra-ui/react";
import { SunIcon, MoonIcon } from "@chakra-ui/icons";

/**
 * Component that toggles between light and dark mode.
 * Uses Chakra UI's useColorMode hook and IconButton component.
 * @returns {JSX.Element} The ThemeToggler component.
 */
export default function ThemeToggler() {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <IconButton
      icon={colorMode === "light" ? <MoonIcon /> : <SunIcon />}
      onClick={toggleColorMode}
      variant="ghost"
      aria-label={""}
    />
  );
}
