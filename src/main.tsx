import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import { BrowserRouter } from "react-router-dom";

/**
 * Renders the React application to the DOM root element using ReactDOM.createRoot.
 * Wraps the App component with BrowserRouter and React.StrictMode.
 * @function
 * @name renderApp
 * @returns {void}
 */
ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>
);
