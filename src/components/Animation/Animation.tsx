import React, { useRef, useEffect } from "react";
import Lottie, { AnimationItem } from "lottie-web";

interface AnimationComponentProps {
  animationData: any;
  isPlaying: boolean;
}

/**
 * AnimationComponent is a React functional component that renders a Lottie animation.
 * @param {Object} props - The component props.
 * @param {Object} props.animationData - The animation data object.
 * @param {boolean} props.isPlaying - A boolean indicating whether the animation is playing or not.
 * @returns {JSX.Element} - A JSX element representing the Lottie animation.
 */
const AnimationComponent: React.FC<AnimationComponentProps> = ({
  animationData,
  isPlaying,
}) => {
  const animationContainerRef = useRef<HTMLDivElement>(null);
  const animationRef = useRef<AnimationItem | null>(null);

  useEffect(() => {
    if (animationContainerRef.current) {
      if (!animationRef.current) {
        animationRef.current = Lottie.loadAnimation({
          container: animationContainerRef.current,
          animationData: animationData,
          loop: false,
          autoplay: false,
        });
      }

      if (isPlaying) {
        animationRef.current.play();
      } else {
        animationRef.current.stop();
      }
    }
  }, [animationData, isPlaying]);

  return (
    <div
      ref={animationContainerRef}
      style={{ width: "100%", height: "100%" }}
    />
  );
};

export default AnimationComponent;
