import { db } from "../config/firebase-config";
import {
  update,
  serverTimestamp,
  ref,
  query,
  orderByChild,
  onValue,
  DataSnapshot,
} from "firebase/database";

/**
 * Adds a notification to multiple users.
 * @param {string} eventID - The ID of the event for which the notification is added.
 * @param {string[]} usernames - An array of usernames to add notifications to.
 * @returns {Promise} A promise that resolves when the notifications are added.
 */
export const addNotificationToUsers = async (
  eventID: string,
  usernames: string[]
) => {
  try {
    const notificationData = {
      isNew: true,
      addedDate: serverTimestamp(),
    };

    const updates: { [key: string]: typeof notificationData } = {};

    usernames.forEach((username) => {
      updates[`users/${username}/pending_notifications/${eventID}`] =
        notificationData;
    });

    await update(ref(db), updates);
  } catch (error) {
    console.error("Error adding notification to users:", error);
    throw error;
  }
};

/**
 * Sets up a listener for pending notifications of the current user.
 * @param {string} currentUserUsername - The username of the current user.
 * @param {function} callback - The function to call when notifications are received.
 * @returns {function} A function that can be called to remove the listener.
 */
export const setupNotificationListener = (
  currentUserUsername: string,
  callback: (notifications: any[]) => void
) => {
  const notificationsRef = ref(
    db,
    `users/${currentUserUsername}/pending_notifications`
  );
  const notificationsQuery = query(notificationsRef, orderByChild("addedDate"));

  const onNotificationValue = (snapshot: DataSnapshot) => {
    if (snapshot.exists()) {
      // Convert the snapshot to an array of notifications
      const notifications = Object.keys(snapshot.val()).map(
        (notificationId) => {
          return { id: notificationId, ...snapshot.val()[notificationId] };
        }
      );

      callback(notifications);
    } else {
      callback([]);
    }
  };

  const unsubscribe = onValue(notificationsQuery, onNotificationValue);

  return () => {
    unsubscribe();
  };
};

/**
 * Update the "isNew" status of a specific notification for a user.
 * @param {string} username - The username of the user.
 * @param {string} eventID - The ID of the event associated with the notification.
 * @param {boolean} isNew - The new value for the "isNew" status.
 * @returns {Promise} A promise that resolves when the notification is updated.
 */
export const updateNotificationIsNew = async (
  username: string | undefined,
  eventID: string,
  isNew: boolean
) => {
  try {
    const notificationPath = `users/${username}/pending_notifications/${eventID}/isNew`;

    const updates = {
      [notificationPath]: isNew,
    };

    await update(ref(db), updates);
  } catch (error) {
    console.error("Error updating notification status:", error);
    throw error;
  }
};
