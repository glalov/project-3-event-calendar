import { logoutUser } from "../../services/auth.service";
import { useContext } from "react";
import AuthContext from "../../context/AuthContext";
import { logOutSuccessText } from "../../common/validationMessages";
import UseSuccessToast from "../../hooks/UseSuccessToast";
import { useNavigate } from "react-router-dom";

/**
 * A component that logs out the user and displays a success pop-up message.
 * @returns {JSX.Element} The LogOut component.
 */
const LogOutButton = () => {
  const { setUser } = useContext(AuthContext);
  const toast = UseSuccessToast();
  const navigate = useNavigate();

  /**
   * Logs out the user and displays a success pop-up message.
   */
  const onLogout = () => {
    logoutUser().then(() => {
      setUser((prevState) => {
        return {
          ...prevState,
          user: null,
          userData: null,
        };
      });
      toast(logOutSuccessText, "logout-success");
      navigate("/");
    });
  };

  return (
    <div style={{ width: "100%" }} onClick={onLogout}>
      Log Out
    </div>
  );
};

export default LogOutButton;
