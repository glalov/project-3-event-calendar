import {
  UserCredential,
  createUserWithEmailAndPassword,
  getAuth,
  signInWithEmailAndPassword,
  signOut,
} from "firebase/auth";
import { auth, firebaseConfig } from "../config/firebase-config";
import { initializeApp } from "@firebase/app";

/**
 * Registers a new user with the given email and password.
 * @param {string} email - The email of the user to register.
 * @param {string} password - The password of the user to register.
 * @returns {Promise} A promise that resolves with the user's authentication credentials.
 */
export const registerUser = (
  email: string,
  password: string
): Promise<UserCredential> => {
  return createUserWithEmailAndPassword(auth, email, password);
};

/**
 * Registers a new user with the given email and password as an admin.
 * @param {string} email - The email of the user to register.
 * @param {string} password - The password of the user to register.
 * @returns {Promise} A promise that resolves with the user's authentication credentials.
 */
export const registerUserAsAdmin = (
  email: string,
  password: string
): Promise<UserCredential> => {
  const secondaryApp = initializeApp(firebaseConfig, "Secondary");
  const secondaryAuth = getAuth(secondaryApp);
  return createUserWithEmailAndPassword(secondaryAuth, email, password);
};

/**
 * Logs in a user with the given email and password.
 * @param {string} email - The email of the user to log in.
 * @param {string} password - The password of the user to log in.
 * @returns {Promise} A promise that resolves with the user's authentication credentials.
 */
export const loginUser = (
  email: string,
  password: string
): Promise<UserCredential> => {
  return signInWithEmailAndPassword(auth, email, password);
};

/**
 * Logs out the currently logged in user.
 * @returns {Promise} A promise that resolves when the user is logged out.
 */
export const logoutUser = (): Promise<void> => {
  return signOut(auth);
};
