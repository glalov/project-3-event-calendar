import { get, ref, push, update, remove } from "firebase/database";

import { db } from "../config/firebase-config";

/**
 * Creates a new Friend List in the database.
 * @param {string} listName - The name of the Friend List.
 * @param {object} listData - The data representing the Friend List (members).
 * @returns {Promise} A promise that resolves with the ID of the created Friend List.
 */
export const createFriendListInDatabase = async (
  listName: string,
  listData: object,
  owner: string
) => {
  try {
    // Create a new Friend List entry in the database and get the ID
    const newListRef = await push(ref(db, "friendLists"), {
      name: listName,
      members: listData,
      owner,
    });

    return newListRef.key;
  } catch (error) {
    console.error("Error creating Friend List in database:", error);
  }
};

/**
 * Gets a friend list by its ID.
 * @param {string} listId - The ID of the friend list.
 * @returns {Promise} A promise that resolves with the friend list data.
 */
export const getFriendListById = (listId: string) => {
  return get(ref(db, `friendLists/${listId}`));
};

/**
 * Fetches all friend lists of a user.
 * @param {string} username - The username of the user.
 * @returns {Promise} A promise that resolves with an array of friend lists.
 */
export const fetchFriendLists = async (username: string) => {
  try {
    // Retrieve the user's data to get the list of friend list IDs
    const userDataSnapshot = await get(
      ref(db, `users/${username}/friendLists`)
    );
    const friendListIds = Object.keys(userDataSnapshot.val() || {});

    // Fetch each friend list using the IDs
    const friendListsPromises = friendListIds.map(async (listId) => {
      const friendListDataSnapshot = await getFriendListById(listId);
      return {
        id: listId,
        ...friendListDataSnapshot.val(),
      };
    });

    const friendLists = await Promise.all(friendListsPromises);
    return friendLists;
  } catch (error) {
    console.error("Error fetching friend lists:", error);
    throw error;
  }
};

/**
 * Fetches all friend lists containing the specified username.
 *
 * @param {string} username - The username to search for in friend lists.
 * @return {Promise<string[]>} A promise that resolves to an array of friend list IDs containing the username.
 * @throws {Error} If there is an error fetching the friend lists from the database.
 */
export const fetchAllFriendLists = async (username: string) => {
  try {
    const friendListsRef = ref(db, "friendLists");
    const friendListsSnapshot = await get(friendListsRef);

    if (!friendListsSnapshot.exists()) {
      return [];
    }

    const friendLists: string[] = [];

    friendListsSnapshot.forEach((childSnapshot) => {
      const friendListData = childSnapshot.val();

      // Check if the friend list contains the required username in its members
      if (friendListData.members && friendListData.members[username] === true) {
        friendLists.push(childSnapshot.key);
      }
    });

    return friendLists;
  } catch (error) {
    console.error("Error fetching friend lists:", error);
    return [];
  }
};

/**
 * Fetches the data of a specific friend list by its ID.
 * @param {string} listId - The ID of the friend list.
 * @returns {Promise} A promise that resolves with the friend list data.
 */
export const fetchFriendListData = async (listId: string) => {
  try {
    const friendListDataSnapshot = await getFriendListById(listId);
    return friendListDataSnapshot.val();
  } catch (error) {
    console.error("Error fetching friend list data:", error);
    throw error;
  }
};

/**
 * Updates an existing Friend List in the database.
 * @param {string} listId - The ID of the Friend List to update.
 * @param {string} listName - The updated name of the Friend List.
 * @param {object} listData - The updated data representing the Friend List (members).
 * @returns {Promise} A promise that resolves when the Friend List is successfully updated.
 */
export const updateFriendList = async (
  listId: string,
  listName: string,
  listData: object
) => {
  try {
    const friendListRef = ref(db, `friendLists/${listId}`);

    // Update the Friend List with the new data
    await update(friendListRef, {
      name: listName,
      members: listData,
    });

    console.log("Friend List updated successfully.");
  } catch (error) {
    console.error("Error updating Friend List in the database:", error);
    throw error;
  }
};

/**
 * Deletes a friend list from the database.
 * @param {string} listId - The ID of the friend list to delete.
 * @returns {Promise} A promise that resolves when the friend list is deleted.
 */
export const deleteFriendList = async (listId: string) => {
  try {
    const friendListRef = ref(db, `friendLists/${listId}`);
    await remove(friendListRef);
  } catch (error) {
    console.error("Error deleting friend list:", error);
    throw error;
  }
};
