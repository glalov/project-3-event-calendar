import React, { useState, useEffect } from "react";
import {
  Box,
  Heading,
  Input,
  Flex,
  List,
  ListItem,
  IconButton,
} from "@chakra-ui/react";
import { AddIcon, MinusIcon } from "@chakra-ui/icons";
import { fetchFriendLists } from "../../services/friends-list.service";

interface FriendListModuleProps {
  username: string;
}

interface FriendList {
  members: { [key: string]: boolean };
  id: string;
  name: string;
}

interface SelectedFriendListsProps {
  selectedFriendLists: FriendList[];
  setSelectedFriendLists: React.Dispatch<React.SetStateAction<FriendList[]>>;
  // friendLists: FriendList[];
  // setFriendLists: React.Dispatch<React.SetStateAction<FriendList[]>>;
}

const FriendListModule: React.FC<
  FriendListModuleProps & SelectedFriendListsProps
> = ({
  username,
  selectedFriendLists,
  setSelectedFriendLists,
  // friendLists,
  // setFriendLists,
}) => {
  const [searchQuery, setSearchQuery] = useState<string>("");
  const [friendLists, setFriendLists] = useState<FriendList[]>([]);

  useEffect(() => {
    const fetchFriendListsForUser = async () => {
      try {
        const fetchedFriendLists = await fetchFriendLists(username);
        setFriendLists(fetchedFriendLists);
      } catch (error) {
        console.error("Error fetching friend lists:", error);
      }
    };

    fetchFriendListsForUser();
  }, [username]);

  useEffect(() => {
    const updatedFriendLists = friendLists.filter(
      (list) =>
        !selectedFriendLists.some((selectedList) => selectedList.id === list.id)
    );

    // Check if the new value is different from the current value
    if (JSON.stringify(updatedFriendLists) !== JSON.stringify(friendLists)) {
      setFriendLists(updatedFriendLists);
    }
  }, [selectedFriendLists, friendLists]);

  const handleAddToList = (currentList: FriendList) => {
    setSelectedFriendLists((prevSelected) => [...prevSelected, currentList]);

    setFriendLists((prevFriendLists) =>
      prevFriendLists.filter((list) => list.id !== currentList.id)
    );
  };

  const handleRemoveFromInvitedList = (currentList: FriendList) => {
    setFriendLists((prevList) => [...prevList, currentList]);
    setSelectedFriendLists((prevSelected) =>
      prevSelected.filter((list) => list.id !== currentList.id)
    );
  };

  const filteredFriendLists = friendLists.filter((list) =>
    list.name.toLowerCase().startsWith(searchQuery.toLowerCase())
  );

  return (
    <Flex>
      <Box flex={1} mr={4}>
        <Heading as="h4" size="md" mb={2}>
          Invited Friends List
        </Heading>
        <List spacing={2}>
          {selectedFriendLists.map((list) => (
            <ListItem key={list.id}>
              {list.name}{" "}
              <IconButton
                ml={2}
                icon={<MinusIcon />}
                onClick={() => handleRemoveFromInvitedList(list)}
                aria-label={""}
              />
            </ListItem>
          ))}
        </List>
      </Box>

      <Box flex={1}>
        <Heading as="h4" size="md" mb={2}>
          Friend Lists
        </Heading>
        <Flex>
          <Input
            flex={1}
            size="lg"
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
            placeholder="Search friend lists..."
          />
        </Flex>
        <List spacing={2} mt={4}>
          {filteredFriendLists.map((list) => (
            <ListItem key={list.id}>
              {list.name}{" "}
              {!selectedFriendLists
                .map((friend) => friend.id)
                .includes(list.id) && (
                <IconButton
                  aria-label={""}
                  ml={2}
                  icon={<AddIcon />}
                  onClick={() => handleAddToList(list)}
                />
              )}
            </ListItem>
          ))}
        </List>
      </Box>
    </Flex>
  );
};

export default FriendListModule;
