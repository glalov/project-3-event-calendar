import { useContext, useEffect, useState } from "react";
import {
  Box,
  Heading,
  Text,
  Image,
  Badge,
  Divider,
  Button,
  Center,
} from "@chakra-ui/react";
import { useNavigate, useParams } from "react-router-dom";
import {
  deleteEventFromDatabase,
  getEventByID,
} from "../services/events.service";
import { EventData } from "../common/interfaces";
import { formatDate, formatTime } from "../common/timeFormat";
import { displayBothDates } from "../common/timeFunctions";
import { getFriendListById } from "../services/friends-list.service";
import EditEventView from "./EditEventView";
import AuthContext from "../context/AuthContext";
import UseSuccessToast from "../hooks/UseSuccessToast";
import { Role } from "../services/users.service";

/**
 * This component displays the details of a single event, including its title, description, start and end dates, location, and other details.
 * It also allows the user to edit or delete the event if they have the appropriate permissions.
 */
const EventView = () => {
  const { eventId } = useParams();
  const [event, setEvent] = useState<EventData | null>(null);
  const [selectedFriendLists, setSelectedFriendLists] = useState<string[]>([]);
  const { closeModal, openModal, userData } = useContext(AuthContext);
  const navigate = useNavigate();
  const toast = UseSuccessToast();

  useEffect(() => {
    const fetchEvent = async () => {
      const event = await getEventByID(eventId || "");
      setEvent(event);
    };

    fetchEvent();
  }, [eventId]);

  useEffect(() => {
    // This effect updates selectedFriendLists when event changes
    if (event && event.selectedFriendLists) {
      const fetchFriendLists = async () => {
        const friendListNames = await Promise.all(
          Object.keys(event.selectedFriendLists).map(async (listId) => {
            const friendListSnapshot = await getFriendListById(listId);
            const friendListData = friendListSnapshot.val();
            return friendListData?.name || "";
          })
        );
        setSelectedFriendLists(friendListNames);
      };

      fetchFriendLists();
    }
  }, [event]);

  /**
   * Updates the event data in state with the provided updated event data.
   * @param updatedEvent - The updated event data to set in state.
   */
  const updateEventData = (updatedEvent: EventData) => {
    setEvent(updatedEvent);
  };

  if (!event) {
    return <div>Loading...</div>;
  }

  const { formattedStartDate, formattedDueDate } = displayBothDates(
    event.startDate,
    event.dueDate
  );

  /**
   * Opens the edit event modal with the current event's data.
   * @param eventId - The ID of the event to edit.
   */
  const handleSaveEdit = (eventId: string) => {
    openModal(
      <EditEventView
        ownerUsername={event.owner}
        eventId={eventId}
        updateEventData={updateEventData}
      />
    );
  };

  /**
   * Deletes the current event from the database and navigates back to the previous page.
   * @param eventId - The ID of the event to delete.
   */
  const onDeleteEvent = async (eventId: string) => {
    try {
      await deleteEventFromDatabase(eventId);
      closeModal();
      navigate(-1);
      toast("Event deleted successfully", "event-delete-success-" + eventId);
    } catch (error) {
      console.error(error);
      toast("Error deleting event", "event-delete-error-" + eventId);
    }
  };

  /**
   * Opens the delete event confirmation modal.
   * @param eventId - The ID of the event to delete.
   */
  const handleDeleteEvent = (eventId: string) => {
    openModal(
      <Box>
        <Center>
          <Text as="b">Are you sure you want to delete this post?</Text>
        </Center>
        <Center>
          <Button
            onClick={() => onDeleteEvent(eventId)}
            m={6}
            colorScheme="red"
          >
            Delete
          </Button>
          <Button onClick={closeModal} m={6} colorScheme="blue">
            Cancel
          </Button>
        </Center>
      </Box>
    );
  };

  return (
    <div className="calendar-container">
      <Box p={4}>
        <Heading as="h1" size="xl" mb={2}>
          {event.title}
        </Heading>
        <Badge colorScheme={event.isPrivate ? "red" : "green"} mb={2}>
          {event.isPrivate ? "Private" : "Public"}
        </Badge>
        <Text>
          {formattedStartDate}
          {formattedDueDate && <span>{formattedDueDate}</span>}
        </Text>
        {event.image ? (
          <Image
            src={event.image}
            alt={event.title}
            mb={4}
            maxWidth="1000px"
            maxHeight="700px"
            mt={4}
          />
        ) : null}
        <Text fontSize="md" mb={4}>
          {event.description}
        </Text>
        <Divider my={10} borderColor="gray.700" />
        <Text fontSize="lg" mb={5}>
          Details:
        </Text>
        <Text>
          <strong>Owner:</strong> {event.owner}
        </Text>
        <Text>
          <strong>Location:</strong> {event.roomNumber}
        </Text>
        <Text>
          <strong>Start Date:</strong> {formatDate(event.startDate)} @{" "}
          {formatTime(event.startDate)}
        </Text>
        <Text>
          <strong>Due Date:</strong> {formatDate(event.dueDate)} @{" "}
          {formatTime(event.dueDate)}
        </Text>
        {event.recurringRule && (
          <Box>
            <Text>Recurring:</Text>
            <Text>
              Every {event.recurringRule.interval}{" "}
              {event.recurringRule.frequency} until {event.recurringRule.until}
            </Text>
          </Box>
        )}
        {event.isPrivate && (
          <Box>
            <Text>
              Invited Members:
              {event.selectedMembers &&
                Object.keys(event.selectedMembers).map((username) => (
                  <Badge
                    m={2}
                    key={username}
                    variant="subtle"
                    colorScheme="green"
                  >
                    {username}
                  </Badge>
                ))}
            </Text>
            {selectedFriendLists.length > 0 && (
              <Box>
                <Text>
                  Invited Friend Lists:
                  {selectedFriendLists.map((list) => {
                    return (
                      <Badge m={2} variant="subtle" colorScheme="green">
                        {list}
                      </Badge>
                    );
                  })}
                </Text>
              </Box>
            )}
          </Box>
        )}
      </Box>

      {(userData?.role === Role.Principal ||
        event.owner === userData?.username) && (
        <>
          <Button
            m={2}
            onClick={() => handleSaveEdit(eventId || "")}
            colorScheme="blue"
            mt={4}
          >
            Edit
          </Button>
          <Button
            m={2}
            onClick={() => handleDeleteEvent(eventId || "")}
            colorScheme="red"
            mt={4}
          >
            Delete
          </Button>
        </>
      )}
    </div>
  );
};

export default EventView;
