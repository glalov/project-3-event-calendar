import {
  get,
  ref,
  push,
  set,
  orderByChild,
  equalTo,
  query,
  onValue,
  DataSnapshot,
  remove,
} from "firebase/database";

import { db } from "../config/firebase-config";
import { removeEventFromUser } from "./users.service";
import { EventData, RecurringRule } from "../common/interfaces";

export type EventsPerMonth = {
  title: string;
  id: string;
  owner: string;
  selectedMembers: { [key: string]: boolean };
  selectedFriendLists: { [key: string]: boolean };
  startDate: string;
  dueDate: string;
  description: string;
  isPrivate: boolean;
  image: string;
  recurringRule: RecurringRule | null;
};

/**
 * Converts a Firebase snapshot of events to an array of EventsPerMonth objects.
 * @param snapshot - The Firebase snapshot of events.
 * @returns An array of EventsPerMonth objects.
 */
export const fromEventsDocument = (snapshot: DataSnapshot) => {
  const eventsDocument = snapshot.val() || [];

  return Object.keys(eventsDocument).map((key) => {
    const event = eventsDocument[key];

    return {
      title: event.title,
      id: key,
      owner: event.owner,
      selectedMembers: event.selectedMembers,
      selectedFriendLists: event.selectedFriendLists,
      startDate: event.startDate,
      dueDate: event.dueDate,
      description: event.description,
      isPrivate: event.isPrivate,
      image: event.image,
      recurringRule: event.recurringRule ? event.recurringRule : null,
    } as EventsPerMonth;
  });
};

/**
 * Retrieves all events from the database.
 * @returns An array of EventsPerMonth objects.
 */
export const getAllEvents = async () => {
  try {
    const allEventsSnapshot = await get(ref(db, "events"));

    if (!allEventsSnapshot.exists()) {
      return [];
    }

    return fromEventsDocument(allEventsSnapshot);
  } catch (error) {
    console.error("Error fetching all events from database:", error);
    return [];
  }
};

/**
 * Retrieves all events owned by a specific user from the database.
 * @param username - The username of the owner of the events.
 * @returns An array of EventsPerMonth objects.
 */
export const getEventsByOwner = async (username: string) => {
  try {
    const allEventsSnapshot = await get(
      query(ref(db, "events"), orderByChild("owner"), equalTo(username))
    );

    if (!allEventsSnapshot.exists()) {
      return [];
    }

    return fromEventsDocument(allEventsSnapshot);
  } catch (error) {
    console.error("Error fetching events by user from database:", error);
    return [];
  }
};

/**
 * Adds a new event to the database.
 * @param eventData - The data for the new event.
 * @returns The key of the newly created event.
 */
export const addEventToDatabase = async (eventData: object) => {
  try {
    const newEventRef = await push(ref(db, "events"), eventData);

    return newEventRef.key;
  } catch (error) {
    console.error("Error creating Friend List in database:", error);
  }
};

/**
 * Retrieves an event from the database by its ID.
 * @param eventId - The ID of the event to retrieve.
 * @returns An object representing the event, or null if the event does not exist.
 */
export const getEventByID = async (eventId: string) => {
  try {
    const eventSnapshot = await get(ref(db, `events/${eventId}`));

    if (!eventSnapshot.exists()) {
      return null;
    }

    const eventData = eventSnapshot.val();
    return {
      id: eventId,
      ...eventData,
    };
  } catch (error) {
    console.error("Error fetching event by ID from database:", error);
    return null;
  }
};

/**
 * Sets a listener for changes to the events in the database.
 * @param callback - A function to be called whenever the events in the database change.
 * The function will be passed a Firebase snapshot of the events.
 * @returns A function to stop listening for changes to the events in the database.
 */
export const setEventsListener = async (
  callback: (snapshot: DataSnapshot) => void
) => {
  const eventsRef = ref(db, "events");
  return onValue(eventsRef, (snapshot) => {
    callback(snapshot);
  });
};

/**
 * Updates an event in the database.
 * @param eventId - The ID of the event to update.
 * @param eventData - The updated data for the event.
 * @throws An error if there is a problem updating the event in the database.
 */
export const updateEventInDatabase = async (
  eventId: string,
  eventData: EventData
) => {
  try {
    const eventRef = ref(db, `events/${eventId}`);
    await set(eventRef, eventData);
  } catch (error) {
    console.error("Error updating event in database:", error);
    throw error;
  }
};

/**
 * Deletes an event from the database.
 * @param eventId - The ID of the event to delete.
 * @throws An error if there is a problem deleting the event from the database.
 */
export const deleteEventFromDatabase = async (eventId: string) => {
  try {
    const event = await getEventByID(eventId);
    await removeEventFromUser(event.owner, eventId);
    await remove(ref(db, `events/${eventId}`));
  } catch (error) {
    console.error(`Error deleting event with ID ${eventId}:`, error);
  }
};
