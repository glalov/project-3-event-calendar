import {
  getStorage,
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from "firebase/storage";
import { updateProfilePicture } from "./users.service";

/**
 * Uploads a profile picture for the given user.
 * @param username - The username of the user to upload the profile picture for.
 * @param file - The file object representing the image to upload.
 * @returns A Promise that resolves with the download URL of the uploaded image.
 */
export const uploadProfilePicture = (username: string, file: any) => {
  const storage = getStorage();

  const pictureRef = storageRef(
    storage,
    `profilePictures/${username}/${file.name}`
  );

  return uploadBytes(pictureRef, file)
    .then(() => {
      return getDownloadURL(pictureRef)
        .then((downloadURL) => {
          updateProfilePicture(username, downloadURL);
          return downloadURL;
        })
        .catch((error) => {
          console.error("Error getting download URL:", error);
        });
    })
    .catch((error) => {
      console.error("Error uploading profile picture:", error);
    });
};

/**
 * Uploads an image to Firebase Storage.
 * @param file - The file object representing the image to upload.
 * @returns A Promise that resolves with the download URL of the uploaded image.
 */
const uploadImage = async (file: any) => {
  const storage = getStorage();
  const storagePath = `eventImages/${file.name}`;

  const imageRef = storageRef(storage, storagePath);

  try {
    await uploadBytes(imageRef, file);
    const imageUrl = await getDownloadURL(imageRef);
    return imageUrl;
  } catch (error) {
    console.error("Error uploading image:", error);
  }
};

export default uploadImage;
