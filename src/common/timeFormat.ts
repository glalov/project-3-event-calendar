import { format } from "date-fns";

/**
 * Formats a date string to "MMMM dd" format.
 * @param dateString - The date string to format.
 * @returns The formatted date string.
 */
export const formatDate = (dateString: string) => {
  return format(new Date(dateString), "MMMM dd");
};

/**
 * Formats a date string to "h:mm a" format.
 * @param dateString - The date string to format.
 * @returns The formatted time string.
 */
export const formatTime = (dateString: string) => {
  return format(new Date(dateString), "h:mm a").toLocaleLowerCase();
};

/**
 * Formats a date string to "h a" format.
 * @param dateString - The date string to format.
 * @returns The formatted short time string.
 */
export const formatTimeShort = (dateString: string) => {
  return format(new Date(dateString), "h a").toLocaleLowerCase();
};
