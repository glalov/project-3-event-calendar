import { EventsPerMonth } from "../../services/events.service";
import {
  Grid,
  GridItem,
  Box,
  Link as ChakraLink,
  Divider,
  Heading,
  Flex,
  Text,
  Image,
} from "@chakra-ui/react";
import { Link as ReactRouterLink } from "react-router-dom";

export interface EventsPerMonthProps {
  events: EventsPerMonth[];
}

type SortedEventsPerMonth = {
  startDateAsDate: Date;
  dueDateAsDate: Date;
  startDayOfWeek: string;
  startDay: string;
} & EventsPerMonth;

/**
 * Renders a list of events grouped by month and year.
 * @param {EventsPerMonthProps} events - An array of events to be displayed.
 * @returns {JSX.Element} - A React component that displays the events grouped by month and year.
 */
const EventListPerMonth: React.FC<EventsPerMonthProps> = ({ events }) => {
  // Sort events by start date and add them into map per month and year
  const eventsPerMonth = events.reduce((acc, event) => {
    const startDateAsDate = new Date(event.startDate);
    const dueDateAsDate = new Date(event.dueDate);
    const month = startDateAsDate.getMonth();
    const year = startDateAsDate.getFullYear();
    const monthYearKey = `${month}-${year}`;

    const startDayOfWeek = startDateAsDate.toLocaleString("default", {
      weekday: "short",
    });
    const startDay = startDateAsDate.toLocaleString("default", {
      day: "numeric",
    });

    if (!acc.has(monthYearKey)) {
      acc.set(monthYearKey, []);
    }
    acc.get(monthYearKey).push({
      ...event,
      startDateAsDate,
      dueDateAsDate,
      startDayOfWeek,
      startDay,
    });
    return acc;
  }, new Map());

  return (
    <div>
      {Array.from(eventsPerMonth.entries()).map(([monthYearKey, events]) => {
        const [month, year] = monthYearKey.split("-");
        const keyDate = new Date(year, month);
        const monthText = keyDate.toLocaleString("default", { month: "long" });

        return (
          <Grid key={monthYearKey} m={2}>
            <GridItem m={2}>
              <Flex alignItems="center" gap={6}>
                <Heading as="h4" size="sm" sx={{ whiteSpace: "nowrap" }}>
                  {monthText} {year}
                </Heading>
                <Divider border="1px" />
              </Flex>
            </GridItem>
            <GridItem m={2}>
              {events.map((event: SortedEventsPerMonth) => {
                return (
                  <Grid
                    templateColumns={event.image ? "10% 45% 45%" : "10% 90%"}
                    key={event.id}
                    m={2}
                  >
                    <GridItem rowSpan={3} m={2}>
                      <Box as="div">
                        <Text align={"center"}>{event.startDayOfWeek}</Text>
                      </Box>
                      <Box as="div">
                        <Text align={"center"}>
                          <Text as="b">{event.startDay}</Text>
                        </Text>
                      </Box>
                    </GridItem>
                    <GridItem m={2}>
                      {`${event.startDateAsDate.toLocaleString()} - ${event.dueDateAsDate.toLocaleString()}`}
                    </GridItem>
                    {event.image && (
                      <GridItem rowSpan={3} m={2}>
                        <Image src={event.image} maxWidth="300px" />
                      </GridItem>
                    )}
                    <GridItem m={2}>
                      <ChakraLink
                        as={ReactRouterLink}
                        to={"/event/" + event.id}
                      >
                        <Text as="b">{event.title}</Text>
                      </ChakraLink>
                    </GridItem>
                    <GridItem m={2}>
                      <Text>{event.description}</Text>
                    </GridItem>
                  </Grid>
                );
              })}
            </GridItem>
          </Grid>
        );
      })}
    </div>
  );
};

export default EventListPerMonth;
